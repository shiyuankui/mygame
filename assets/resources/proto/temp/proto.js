/*eslint-disable block-scoped-var, id-length, no-control-regex, no-magic-numbers, no-prototype-builtins, no-redeclare, no-shadow, no-var, sort-vars*/
"use strict";

var $protobuf = protobuf//require("protobufjs/minimal");

// Common aliases
var $Reader = $protobuf.Reader, $Writer = $protobuf.Writer, $util = $protobuf.util;

// Exported root namespace
var $root = $protobuf.roots["default"] || ($protobuf.roots["default"] = {});

$root.fish_login = (function() {

    /**
     * Namespace fish_login.
     * @exports fish_login
     * @namespace
     */
    var fish_login = {};

    fish_login.fish_login_req = (function() {

        /**
         * Properties of a fish_login_req.
         * @memberof fish_login
         * @interface Ifish_login_req
         * @property {string|null} [strTcyUserId] fish_login_req strTcyUserId
         * @property {number|null} [nChannelId] fish_login_req nChannelId
         * @property {number|null} [nOsType] fish_login_req nOsType
         * @property {number|null} [nSex] fish_login_req nSex
         * @property {number|null} [nFromId] fish_login_req nFromId
         * @property {string|null} [strDeviceId] fish_login_req strDeviceId
         * @property {string|null} [strNickName] fish_login_req strNickName
         * @property {string|null} [strVersion] fish_login_req strVersion
         * @property {string|null} [strImei] fish_login_req strImei
         * @property {string|null} [strImsi] fish_login_req strImsi
         */

        /**
         * Constructs a new fish_login_req.
         * @memberof fish_login
         * @classdesc Represents a fish_login_req.
         * @implements Ifish_login_req
         * @constructor
         * @param {fish_login.Ifish_login_req=} [properties] Properties to set
         */
        function fish_login_req(properties) {
            if (properties)
                for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                    if (properties[keys[i]] != null)
                        this[keys[i]] = properties[keys[i]];
        }

        /**
         * fish_login_req strTcyUserId.
         * @member {string} strTcyUserId
         * @memberof fish_login.fish_login_req
         * @instance
         */
        fish_login_req.prototype.strTcyUserId = "";

        /**
         * fish_login_req nChannelId.
         * @member {number} nChannelId
         * @memberof fish_login.fish_login_req
         * @instance
         */
        fish_login_req.prototype.nChannelId = 0;

        /**
         * fish_login_req nOsType.
         * @member {number} nOsType
         * @memberof fish_login.fish_login_req
         * @instance
         */
        fish_login_req.prototype.nOsType = 0;

        /**
         * fish_login_req nSex.
         * @member {number} nSex
         * @memberof fish_login.fish_login_req
         * @instance
         */
        fish_login_req.prototype.nSex = 0;

        /**
         * fish_login_req nFromId.
         * @member {number} nFromId
         * @memberof fish_login.fish_login_req
         * @instance
         */
        fish_login_req.prototype.nFromId = 0;

        /**
         * fish_login_req strDeviceId.
         * @member {string} strDeviceId
         * @memberof fish_login.fish_login_req
         * @instance
         */
        fish_login_req.prototype.strDeviceId = "";

        /**
         * fish_login_req strNickName.
         * @member {string} strNickName
         * @memberof fish_login.fish_login_req
         * @instance
         */
        fish_login_req.prototype.strNickName = "";

        /**
         * fish_login_req strVersion.
         * @member {string} strVersion
         * @memberof fish_login.fish_login_req
         * @instance
         */
        fish_login_req.prototype.strVersion = "";

        /**
         * fish_login_req strImei.
         * @member {string} strImei
         * @memberof fish_login.fish_login_req
         * @instance
         */
        fish_login_req.prototype.strImei = "";

        /**
         * fish_login_req strImsi.
         * @member {string} strImsi
         * @memberof fish_login.fish_login_req
         * @instance
         */
        fish_login_req.prototype.strImsi = "";

        /**
         * Creates a new fish_login_req instance using the specified properties.
         * @function create
         * @memberof fish_login.fish_login_req
         * @static
         * @param {fish_login.Ifish_login_req=} [properties] Properties to set
         * @returns {fish_login.fish_login_req} fish_login_req instance
         */
        fish_login_req.create = function create(properties) {
            return new fish_login_req(properties);
        };

        /**
         * Encodes the specified fish_login_req message. Does not implicitly {@link fish_login.fish_login_req.verify|verify} messages.
         * @function encode
         * @memberof fish_login.fish_login_req
         * @static
         * @param {fish_login.Ifish_login_req} message fish_login_req message or plain object to encode
         * @param {$protobuf.Writer} [writer] Writer to encode to
         * @returns {$protobuf.Writer} Writer
         */
        fish_login_req.encode = function encode(message, writer) {
            if (!writer)
                writer = $Writer.create();
            if (message.strTcyUserId != null && message.hasOwnProperty("strTcyUserId"))
                writer.uint32(/* id 1, wireType 2 =*/10).string(message.strTcyUserId);
            if (message.nChannelId != null && message.hasOwnProperty("nChannelId"))
                writer.uint32(/* id 2, wireType 0 =*/16).int32(message.nChannelId);
            if (message.nOsType != null && message.hasOwnProperty("nOsType"))
                writer.uint32(/* id 3, wireType 0 =*/24).int32(message.nOsType);
            if (message.nSex != null && message.hasOwnProperty("nSex"))
                writer.uint32(/* id 4, wireType 0 =*/32).int32(message.nSex);
            if (message.nFromId != null && message.hasOwnProperty("nFromId"))
                writer.uint32(/* id 5, wireType 0 =*/40).int32(message.nFromId);
            if (message.strDeviceId != null && message.hasOwnProperty("strDeviceId"))
                writer.uint32(/* id 6, wireType 2 =*/50).string(message.strDeviceId);
            if (message.strNickName != null && message.hasOwnProperty("strNickName"))
                writer.uint32(/* id 7, wireType 2 =*/58).string(message.strNickName);
            if (message.strVersion != null && message.hasOwnProperty("strVersion"))
                writer.uint32(/* id 8, wireType 2 =*/66).string(message.strVersion);
            if (message.strImei != null && message.hasOwnProperty("strImei"))
                writer.uint32(/* id 9, wireType 2 =*/74).string(message.strImei);
            if (message.strImsi != null && message.hasOwnProperty("strImsi"))
                writer.uint32(/* id 10, wireType 2 =*/82).string(message.strImsi);
            return writer;
        };

        /**
         * Encodes the specified fish_login_req message, length delimited. Does not implicitly {@link fish_login.fish_login_req.verify|verify} messages.
         * @function encodeDelimited
         * @memberof fish_login.fish_login_req
         * @static
         * @param {fish_login.Ifish_login_req} message fish_login_req message or plain object to encode
         * @param {$protobuf.Writer} [writer] Writer to encode to
         * @returns {$protobuf.Writer} Writer
         */
        fish_login_req.encodeDelimited = function encodeDelimited(message, writer) {
            return this.encode(message, writer).ldelim();
        };

        /**
         * Decodes a fish_login_req message from the specified reader or buffer.
         * @function decode
         * @memberof fish_login.fish_login_req
         * @static
         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
         * @param {number} [length] Message length if known beforehand
         * @returns {fish_login.fish_login_req} fish_login_req
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        fish_login_req.decode = function decode(reader, length) {
            if (!(reader instanceof $Reader))
                reader = $Reader.create(reader);
            var end = length === undefined ? reader.len : reader.pos + length, message = new $root.fish_login.fish_login_req();
            while (reader.pos < end) {
                var tag = reader.uint32();
                switch (tag >>> 3) {
                case 1:
                    message.strTcyUserId = reader.string();
                    break;
                case 2:
                    message.nChannelId = reader.int32();
                    break;
                case 3:
                    message.nOsType = reader.int32();
                    break;
                case 4:
                    message.nSex = reader.int32();
                    break;
                case 5:
                    message.nFromId = reader.int32();
                    break;
                case 6:
                    message.strDeviceId = reader.string();
                    break;
                case 7:
                    message.strNickName = reader.string();
                    break;
                case 8:
                    message.strVersion = reader.string();
                    break;
                case 9:
                    message.strImei = reader.string();
                    break;
                case 10:
                    message.strImsi = reader.string();
                    break;
                default:
                    reader.skipType(tag & 7);
                    break;
                }
            }
            return message;
        };

        /**
         * Decodes a fish_login_req message from the specified reader or buffer, length delimited.
         * @function decodeDelimited
         * @memberof fish_login.fish_login_req
         * @static
         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
         * @returns {fish_login.fish_login_req} fish_login_req
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        fish_login_req.decodeDelimited = function decodeDelimited(reader) {
            if (!(reader instanceof $Reader))
                reader = new $Reader(reader);
            return this.decode(reader, reader.uint32());
        };

        /**
         * Verifies a fish_login_req message.
         * @function verify
         * @memberof fish_login.fish_login_req
         * @static
         * @param {Object.<string,*>} message Plain object to verify
         * @returns {string|null} `null` if valid, otherwise the reason why it is not
         */
        fish_login_req.verify = function verify(message) {
            if (typeof message !== "object" || message === null)
                return "object expected";
            if (message.strTcyUserId != null && message.hasOwnProperty("strTcyUserId"))
                if (!$util.isString(message.strTcyUserId))
                    return "strTcyUserId: string expected";
            if (message.nChannelId != null && message.hasOwnProperty("nChannelId"))
                if (!$util.isInteger(message.nChannelId))
                    return "nChannelId: integer expected";
            if (message.nOsType != null && message.hasOwnProperty("nOsType"))
                if (!$util.isInteger(message.nOsType))
                    return "nOsType: integer expected";
            if (message.nSex != null && message.hasOwnProperty("nSex"))
                if (!$util.isInteger(message.nSex))
                    return "nSex: integer expected";
            if (message.nFromId != null && message.hasOwnProperty("nFromId"))
                if (!$util.isInteger(message.nFromId))
                    return "nFromId: integer expected";
            if (message.strDeviceId != null && message.hasOwnProperty("strDeviceId"))
                if (!$util.isString(message.strDeviceId))
                    return "strDeviceId: string expected";
            if (message.strNickName != null && message.hasOwnProperty("strNickName"))
                if (!$util.isString(message.strNickName))
                    return "strNickName: string expected";
            if (message.strVersion != null && message.hasOwnProperty("strVersion"))
                if (!$util.isString(message.strVersion))
                    return "strVersion: string expected";
            if (message.strImei != null && message.hasOwnProperty("strImei"))
                if (!$util.isString(message.strImei))
                    return "strImei: string expected";
            if (message.strImsi != null && message.hasOwnProperty("strImsi"))
                if (!$util.isString(message.strImsi))
                    return "strImsi: string expected";
            return null;
        };

        /**
         * Creates a fish_login_req message from a plain object. Also converts values to their respective internal types.
         * @function fromObject
         * @memberof fish_login.fish_login_req
         * @static
         * @param {Object.<string,*>} object Plain object
         * @returns {fish_login.fish_login_req} fish_login_req
         */
        fish_login_req.fromObject = function fromObject(object) {
            if (object instanceof $root.fish_login.fish_login_req)
                return object;
            var message = new $root.fish_login.fish_login_req();
            if (object.strTcyUserId != null)
                message.strTcyUserId = String(object.strTcyUserId);
            if (object.nChannelId != null)
                message.nChannelId = object.nChannelId | 0;
            if (object.nOsType != null)
                message.nOsType = object.nOsType | 0;
            if (object.nSex != null)
                message.nSex = object.nSex | 0;
            if (object.nFromId != null)
                message.nFromId = object.nFromId | 0;
            if (object.strDeviceId != null)
                message.strDeviceId = String(object.strDeviceId);
            if (object.strNickName != null)
                message.strNickName = String(object.strNickName);
            if (object.strVersion != null)
                message.strVersion = String(object.strVersion);
            if (object.strImei != null)
                message.strImei = String(object.strImei);
            if (object.strImsi != null)
                message.strImsi = String(object.strImsi);
            return message;
        };

        /**
         * Creates a plain object from a fish_login_req message. Also converts values to other types if specified.
         * @function toObject
         * @memberof fish_login.fish_login_req
         * @static
         * @param {fish_login.fish_login_req} message fish_login_req
         * @param {$protobuf.IConversionOptions} [options] Conversion options
         * @returns {Object.<string,*>} Plain object
         */
        fish_login_req.toObject = function toObject(message, options) {
            if (!options)
                options = {};
            var object = {};
            if (options.defaults) {
                object.strTcyUserId = "";
                object.nChannelId = 0;
                object.nOsType = 0;
                object.nSex = 0;
                object.nFromId = 0;
                object.strDeviceId = "";
                object.strNickName = "";
                object.strVersion = "";
                object.strImei = "";
                object.strImsi = "";
            }
            if (message.strTcyUserId != null && message.hasOwnProperty("strTcyUserId"))
                object.strTcyUserId = message.strTcyUserId;
            if (message.nChannelId != null && message.hasOwnProperty("nChannelId"))
                object.nChannelId = message.nChannelId;
            if (message.nOsType != null && message.hasOwnProperty("nOsType"))
                object.nOsType = message.nOsType;
            if (message.nSex != null && message.hasOwnProperty("nSex"))
                object.nSex = message.nSex;
            if (message.nFromId != null && message.hasOwnProperty("nFromId"))
                object.nFromId = message.nFromId;
            if (message.strDeviceId != null && message.hasOwnProperty("strDeviceId"))
                object.strDeviceId = message.strDeviceId;
            if (message.strNickName != null && message.hasOwnProperty("strNickName"))
                object.strNickName = message.strNickName;
            if (message.strVersion != null && message.hasOwnProperty("strVersion"))
                object.strVersion = message.strVersion;
            if (message.strImei != null && message.hasOwnProperty("strImei"))
                object.strImei = message.strImei;
            if (message.strImsi != null && message.hasOwnProperty("strImsi"))
                object.strImsi = message.strImsi;
            return object;
        };

        /**
         * Converts this fish_login_req to JSON.
         * @function toJSON
         * @memberof fish_login.fish_login_req
         * @instance
         * @returns {Object.<string,*>} JSON object
         */
        fish_login_req.prototype.toJSON = function toJSON() {
            return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
        };

        return fish_login_req;
    })();

    fish_login.fish_login_rsp = (function() {

        /**
         * Properties of a fish_login_rsp.
         * @memberof fish_login
         * @interface Ifish_login_rsp
         * @property {number} nStatus fish_login_rsp nStatus
         * @property {number|null} [nResult] fish_login_rsp nResult
         * @property {string|null} [strMsg] fish_login_rsp strMsg
         * @property {string|null} [strServerTime] fish_login_rsp strServerTime
         * @property {number|null} [nSwitch] fish_login_rsp nSwitch
         */

        /**
         * Constructs a new fish_login_rsp.
         * @memberof fish_login
         * @classdesc Represents a fish_login_rsp.
         * @implements Ifish_login_rsp
         * @constructor
         * @param {fish_login.Ifish_login_rsp=} [properties] Properties to set
         */
        function fish_login_rsp(properties) {
            if (properties)
                for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                    if (properties[keys[i]] != null)
                        this[keys[i]] = properties[keys[i]];
        }

        /**
         * fish_login_rsp nStatus.
         * @member {number} nStatus
         * @memberof fish_login.fish_login_rsp
         * @instance
         */
        fish_login_rsp.prototype.nStatus = 0;

        /**
         * fish_login_rsp nResult.
         * @member {number} nResult
         * @memberof fish_login.fish_login_rsp
         * @instance
         */
        fish_login_rsp.prototype.nResult = 0;

        /**
         * fish_login_rsp strMsg.
         * @member {string} strMsg
         * @memberof fish_login.fish_login_rsp
         * @instance
         */
        fish_login_rsp.prototype.strMsg = "";

        /**
         * fish_login_rsp strServerTime.
         * @member {string} strServerTime
         * @memberof fish_login.fish_login_rsp
         * @instance
         */
        fish_login_rsp.prototype.strServerTime = "";

        /**
         * fish_login_rsp nSwitch.
         * @member {number} nSwitch
         * @memberof fish_login.fish_login_rsp
         * @instance
         */
        fish_login_rsp.prototype.nSwitch = 0;

        /**
         * Creates a new fish_login_rsp instance using the specified properties.
         * @function create
         * @memberof fish_login.fish_login_rsp
         * @static
         * @param {fish_login.Ifish_login_rsp=} [properties] Properties to set
         * @returns {fish_login.fish_login_rsp} fish_login_rsp instance
         */
        fish_login_rsp.create = function create(properties) {
            return new fish_login_rsp(properties);
        };

        /**
         * Encodes the specified fish_login_rsp message. Does not implicitly {@link fish_login.fish_login_rsp.verify|verify} messages.
         * @function encode
         * @memberof fish_login.fish_login_rsp
         * @static
         * @param {fish_login.Ifish_login_rsp} message fish_login_rsp message or plain object to encode
         * @param {$protobuf.Writer} [writer] Writer to encode to
         * @returns {$protobuf.Writer} Writer
         */
        fish_login_rsp.encode = function encode(message, writer) {
            if (!writer)
                writer = $Writer.create();
            writer.uint32(/* id 1, wireType 0 =*/8).int32(message.nStatus);
            if (message.nResult != null && message.hasOwnProperty("nResult"))
                writer.uint32(/* id 2, wireType 0 =*/16).int32(message.nResult);
            if (message.strMsg != null && message.hasOwnProperty("strMsg"))
                writer.uint32(/* id 3, wireType 2 =*/26).string(message.strMsg);
            if (message.strServerTime != null && message.hasOwnProperty("strServerTime"))
                writer.uint32(/* id 4, wireType 2 =*/34).string(message.strServerTime);
            if (message.nSwitch != null && message.hasOwnProperty("nSwitch"))
                writer.uint32(/* id 5, wireType 0 =*/40).int32(message.nSwitch);
            return writer;
        };

        /**
         * Encodes the specified fish_login_rsp message, length delimited. Does not implicitly {@link fish_login.fish_login_rsp.verify|verify} messages.
         * @function encodeDelimited
         * @memberof fish_login.fish_login_rsp
         * @static
         * @param {fish_login.Ifish_login_rsp} message fish_login_rsp message or plain object to encode
         * @param {$protobuf.Writer} [writer] Writer to encode to
         * @returns {$protobuf.Writer} Writer
         */
        fish_login_rsp.encodeDelimited = function encodeDelimited(message, writer) {
            return this.encode(message, writer).ldelim();
        };

        /**
         * Decodes a fish_login_rsp message from the specified reader or buffer.
         * @function decode
         * @memberof fish_login.fish_login_rsp
         * @static
         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
         * @param {number} [length] Message length if known beforehand
         * @returns {fish_login.fish_login_rsp} fish_login_rsp
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        fish_login_rsp.decode = function decode(reader, length) {
            if (!(reader instanceof $Reader))
                reader = $Reader.create(reader);
            var end = length === undefined ? reader.len : reader.pos + length, message = new $root.fish_login.fish_login_rsp();
            while (reader.pos < end) {
                var tag = reader.uint32();
                switch (tag >>> 3) {
                case 1:
                    message.nStatus = reader.int32();
                    break;
                case 2:
                    message.nResult = reader.int32();
                    break;
                case 3:
                    message.strMsg = reader.string();
                    break;
                case 4:
                    message.strServerTime = reader.string();
                    break;
                case 5:
                    message.nSwitch = reader.int32();
                    break;
                default:
                    reader.skipType(tag & 7);
                    break;
                }
            }
            if (!message.hasOwnProperty("nStatus"))
                throw $util.ProtocolError("missing required 'nStatus'", { instance: message });
            return message;
        };

        /**
         * Decodes a fish_login_rsp message from the specified reader or buffer, length delimited.
         * @function decodeDelimited
         * @memberof fish_login.fish_login_rsp
         * @static
         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
         * @returns {fish_login.fish_login_rsp} fish_login_rsp
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        fish_login_rsp.decodeDelimited = function decodeDelimited(reader) {
            if (!(reader instanceof $Reader))
                reader = new $Reader(reader);
            return this.decode(reader, reader.uint32());
        };

        /**
         * Verifies a fish_login_rsp message.
         * @function verify
         * @memberof fish_login.fish_login_rsp
         * @static
         * @param {Object.<string,*>} message Plain object to verify
         * @returns {string|null} `null` if valid, otherwise the reason why it is not
         */
        fish_login_rsp.verify = function verify(message) {
            if (typeof message !== "object" || message === null)
                return "object expected";
            if (!$util.isInteger(message.nStatus))
                return "nStatus: integer expected";
            if (message.nResult != null && message.hasOwnProperty("nResult"))
                if (!$util.isInteger(message.nResult))
                    return "nResult: integer expected";
            if (message.strMsg != null && message.hasOwnProperty("strMsg"))
                if (!$util.isString(message.strMsg))
                    return "strMsg: string expected";
            if (message.strServerTime != null && message.hasOwnProperty("strServerTime"))
                if (!$util.isString(message.strServerTime))
                    return "strServerTime: string expected";
            if (message.nSwitch != null && message.hasOwnProperty("nSwitch"))
                if (!$util.isInteger(message.nSwitch))
                    return "nSwitch: integer expected";
            return null;
        };

        /**
         * Creates a fish_login_rsp message from a plain object. Also converts values to their respective internal types.
         * @function fromObject
         * @memberof fish_login.fish_login_rsp
         * @static
         * @param {Object.<string,*>} object Plain object
         * @returns {fish_login.fish_login_rsp} fish_login_rsp
         */
        fish_login_rsp.fromObject = function fromObject(object) {
            if (object instanceof $root.fish_login.fish_login_rsp)
                return object;
            var message = new $root.fish_login.fish_login_rsp();
            if (object.nStatus != null)
                message.nStatus = object.nStatus | 0;
            if (object.nResult != null)
                message.nResult = object.nResult | 0;
            if (object.strMsg != null)
                message.strMsg = String(object.strMsg);
            if (object.strServerTime != null)
                message.strServerTime = String(object.strServerTime);
            if (object.nSwitch != null)
                message.nSwitch = object.nSwitch | 0;
            return message;
        };

        /**
         * Creates a plain object from a fish_login_rsp message. Also converts values to other types if specified.
         * @function toObject
         * @memberof fish_login.fish_login_rsp
         * @static
         * @param {fish_login.fish_login_rsp} message fish_login_rsp
         * @param {$protobuf.IConversionOptions} [options] Conversion options
         * @returns {Object.<string,*>} Plain object
         */
        fish_login_rsp.toObject = function toObject(message, options) {
            if (!options)
                options = {};
            var object = {};
            if (options.defaults) {
                object.nStatus = 0;
                object.nResult = 0;
                object.strMsg = "";
                object.strServerTime = "";
                object.nSwitch = 0;
            }
            if (message.nStatus != null && message.hasOwnProperty("nStatus"))
                object.nStatus = message.nStatus;
            if (message.nResult != null && message.hasOwnProperty("nResult"))
                object.nResult = message.nResult;
            if (message.strMsg != null && message.hasOwnProperty("strMsg"))
                object.strMsg = message.strMsg;
            if (message.strServerTime != null && message.hasOwnProperty("strServerTime"))
                object.strServerTime = message.strServerTime;
            if (message.nSwitch != null && message.hasOwnProperty("nSwitch"))
                object.nSwitch = message.nSwitch;
            return object;
        };

        /**
         * Converts this fish_login_rsp to JSON.
         * @function toJSON
         * @memberof fish_login.fish_login_rsp
         * @instance
         * @returns {Object.<string,*>} JSON object
         */
        fish_login_rsp.prototype.toJSON = function toJSON() {
            return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
        };

        return fish_login_rsp;
    })();

    return fish_login;
})();

$root.form = (function() {

    /**
     * Namespace form.
     * @exports form
     * @namespace
     */
    var form = {};

    form.submit_form = (function() {

        /**
         * Properties of a submit_form.
         * @memberof form
         * @interface Isubmit_form
         * @property {string} sendStr submit_form sendStr
         */

        /**
         * Constructs a new submit_form.
         * @memberof form
         * @classdesc Represents a submit_form.
         * @implements Isubmit_form
         * @constructor
         * @param {form.Isubmit_form=} [properties] Properties to set
         */
        function submit_form(properties) {
            if (properties)
                for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                    if (properties[keys[i]] != null)
                        this[keys[i]] = properties[keys[i]];
        }

        /**
         * submit_form sendStr.
         * @member {string} sendStr
         * @memberof form.submit_form
         * @instance
         */
        submit_form.prototype.sendStr = "";

        /**
         * Creates a new submit_form instance using the specified properties.
         * @function create
         * @memberof form.submit_form
         * @static
         * @param {form.Isubmit_form=} [properties] Properties to set
         * @returns {form.submit_form} submit_form instance
         */
        submit_form.create = function create(properties) {
            return new submit_form(properties);
        };

        /**
         * Encodes the specified submit_form message. Does not implicitly {@link form.submit_form.verify|verify} messages.
         * @function encode
         * @memberof form.submit_form
         * @static
         * @param {form.Isubmit_form} message submit_form message or plain object to encode
         * @param {$protobuf.Writer} [writer] Writer to encode to
         * @returns {$protobuf.Writer} Writer
         */
        submit_form.encode = function encode(message, writer) {
            if (!writer)
                writer = $Writer.create();
            writer.uint32(/* id 1, wireType 2 =*/10).string(message.sendStr);
            return writer;
        };

        /**
         * Encodes the specified submit_form message, length delimited. Does not implicitly {@link form.submit_form.verify|verify} messages.
         * @function encodeDelimited
         * @memberof form.submit_form
         * @static
         * @param {form.Isubmit_form} message submit_form message or plain object to encode
         * @param {$protobuf.Writer} [writer] Writer to encode to
         * @returns {$protobuf.Writer} Writer
         */
        submit_form.encodeDelimited = function encodeDelimited(message, writer) {
            return this.encode(message, writer).ldelim();
        };

        /**
         * Decodes a submit_form message from the specified reader or buffer.
         * @function decode
         * @memberof form.submit_form
         * @static
         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
         * @param {number} [length] Message length if known beforehand
         * @returns {form.submit_form} submit_form
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        submit_form.decode = function decode(reader, length) {
            if (!(reader instanceof $Reader))
                reader = $Reader.create(reader);
            var end = length === undefined ? reader.len : reader.pos + length, message = new $root.form.submit_form();
            while (reader.pos < end) {
                var tag = reader.uint32();
                switch (tag >>> 3) {
                case 1:
                    message.sendStr = reader.string();
                    break;
                default:
                    reader.skipType(tag & 7);
                    break;
                }
            }
            if (!message.hasOwnProperty("sendStr"))
                throw $util.ProtocolError("missing required 'sendStr'", { instance: message });
            return message;
        };

        /**
         * Decodes a submit_form message from the specified reader or buffer, length delimited.
         * @function decodeDelimited
         * @memberof form.submit_form
         * @static
         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
         * @returns {form.submit_form} submit_form
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        submit_form.decodeDelimited = function decodeDelimited(reader) {
            if (!(reader instanceof $Reader))
                reader = new $Reader(reader);
            return this.decode(reader, reader.uint32());
        };

        /**
         * Verifies a submit_form message.
         * @function verify
         * @memberof form.submit_form
         * @static
         * @param {Object.<string,*>} message Plain object to verify
         * @returns {string|null} `null` if valid, otherwise the reason why it is not
         */
        submit_form.verify = function verify(message) {
            if (typeof message !== "object" || message === null)
                return "object expected";
            if (!$util.isString(message.sendStr))
                return "sendStr: string expected";
            return null;
        };

        /**
         * Creates a submit_form message from a plain object. Also converts values to their respective internal types.
         * @function fromObject
         * @memberof form.submit_form
         * @static
         * @param {Object.<string,*>} object Plain object
         * @returns {form.submit_form} submit_form
         */
        submit_form.fromObject = function fromObject(object) {
            if (object instanceof $root.form.submit_form)
                return object;
            var message = new $root.form.submit_form();
            if (object.sendStr != null)
                message.sendStr = String(object.sendStr);
            return message;
        };

        /**
         * Creates a plain object from a submit_form message. Also converts values to other types if specified.
         * @function toObject
         * @memberof form.submit_form
         * @static
         * @param {form.submit_form} message submit_form
         * @param {$protobuf.IConversionOptions} [options] Conversion options
         * @returns {Object.<string,*>} Plain object
         */
        submit_form.toObject = function toObject(message, options) {
            if (!options)
                options = {};
            var object = {};
            if (options.defaults)
                object.sendStr = "";
            if (message.sendStr != null && message.hasOwnProperty("sendStr"))
                object.sendStr = message.sendStr;
            return object;
        };

        /**
         * Converts this submit_form to JSON.
         * @function toJSON
         * @memberof form.submit_form
         * @instance
         * @returns {Object.<string,*>} JSON object
         */
        submit_form.prototype.toJSON = function toJSON() {
            return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
        };

        return submit_form;
    })();

    form.submit_form_resp = (function() {

        /**
         * Properties of a submit_form_resp.
         * @memberof form
         * @interface Isubmit_form_resp
         * @property {number} status submit_form_resp status
         * @property {number|null} [code] submit_form_resp code
         * @property {string|null} [msg] submit_form_resp msg
         * @property {string} objStr submit_form_resp objStr
         */

        /**
         * Constructs a new submit_form_resp.
         * @memberof form
         * @classdesc Represents a submit_form_resp.
         * @implements Isubmit_form_resp
         * @constructor
         * @param {form.Isubmit_form_resp=} [properties] Properties to set
         */
        function submit_form_resp(properties) {
            if (properties)
                for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                    if (properties[keys[i]] != null)
                        this[keys[i]] = properties[keys[i]];
        }

        /**
         * submit_form_resp status.
         * @member {number} status
         * @memberof form.submit_form_resp
         * @instance
         */
        submit_form_resp.prototype.status = 0;

        /**
         * submit_form_resp code.
         * @member {number} code
         * @memberof form.submit_form_resp
         * @instance
         */
        submit_form_resp.prototype.code = 0;

        /**
         * submit_form_resp msg.
         * @member {string} msg
         * @memberof form.submit_form_resp
         * @instance
         */
        submit_form_resp.prototype.msg = "";

        /**
         * submit_form_resp objStr.
         * @member {string} objStr
         * @memberof form.submit_form_resp
         * @instance
         */
        submit_form_resp.prototype.objStr = "";

        /**
         * Creates a new submit_form_resp instance using the specified properties.
         * @function create
         * @memberof form.submit_form_resp
         * @static
         * @param {form.Isubmit_form_resp=} [properties] Properties to set
         * @returns {form.submit_form_resp} submit_form_resp instance
         */
        submit_form_resp.create = function create(properties) {
            return new submit_form_resp(properties);
        };

        /**
         * Encodes the specified submit_form_resp message. Does not implicitly {@link form.submit_form_resp.verify|verify} messages.
         * @function encode
         * @memberof form.submit_form_resp
         * @static
         * @param {form.Isubmit_form_resp} message submit_form_resp message or plain object to encode
         * @param {$protobuf.Writer} [writer] Writer to encode to
         * @returns {$protobuf.Writer} Writer
         */
        submit_form_resp.encode = function encode(message, writer) {
            if (!writer)
                writer = $Writer.create();
            writer.uint32(/* id 1, wireType 0 =*/8).int32(message.status);
            if (message.code != null && message.hasOwnProperty("code"))
                writer.uint32(/* id 2, wireType 0 =*/16).int32(message.code);
            if (message.msg != null && message.hasOwnProperty("msg"))
                writer.uint32(/* id 3, wireType 2 =*/26).string(message.msg);
            writer.uint32(/* id 4, wireType 2 =*/34).string(message.objStr);
            return writer;
        };

        /**
         * Encodes the specified submit_form_resp message, length delimited. Does not implicitly {@link form.submit_form_resp.verify|verify} messages.
         * @function encodeDelimited
         * @memberof form.submit_form_resp
         * @static
         * @param {form.Isubmit_form_resp} message submit_form_resp message or plain object to encode
         * @param {$protobuf.Writer} [writer] Writer to encode to
         * @returns {$protobuf.Writer} Writer
         */
        submit_form_resp.encodeDelimited = function encodeDelimited(message, writer) {
            return this.encode(message, writer).ldelim();
        };

        /**
         * Decodes a submit_form_resp message from the specified reader or buffer.
         * @function decode
         * @memberof form.submit_form_resp
         * @static
         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
         * @param {number} [length] Message length if known beforehand
         * @returns {form.submit_form_resp} submit_form_resp
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        submit_form_resp.decode = function decode(reader, length) {
            if (!(reader instanceof $Reader))
                reader = $Reader.create(reader);
            var end = length === undefined ? reader.len : reader.pos + length, message = new $root.form.submit_form_resp();
            while (reader.pos < end) {
                var tag = reader.uint32();
                switch (tag >>> 3) {
                case 1:
                    message.status = reader.int32();
                    break;
                case 2:
                    message.code = reader.int32();
                    break;
                case 3:
                    message.msg = reader.string();
                    break;
                case 4:
                    message.objStr = reader.string();
                    break;
                default:
                    reader.skipType(tag & 7);
                    break;
                }
            }
            if (!message.hasOwnProperty("status"))
                throw $util.ProtocolError("missing required 'status'", { instance: message });
            if (!message.hasOwnProperty("objStr"))
                throw $util.ProtocolError("missing required 'objStr'", { instance: message });
            return message;
        };

        /**
         * Decodes a submit_form_resp message from the specified reader or buffer, length delimited.
         * @function decodeDelimited
         * @memberof form.submit_form_resp
         * @static
         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
         * @returns {form.submit_form_resp} submit_form_resp
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        submit_form_resp.decodeDelimited = function decodeDelimited(reader) {
            if (!(reader instanceof $Reader))
                reader = new $Reader(reader);
            return this.decode(reader, reader.uint32());
        };

        /**
         * Verifies a submit_form_resp message.
         * @function verify
         * @memberof form.submit_form_resp
         * @static
         * @param {Object.<string,*>} message Plain object to verify
         * @returns {string|null} `null` if valid, otherwise the reason why it is not
         */
        submit_form_resp.verify = function verify(message) {
            if (typeof message !== "object" || message === null)
                return "object expected";
            if (!$util.isInteger(message.status))
                return "status: integer expected";
            if (message.code != null && message.hasOwnProperty("code"))
                if (!$util.isInteger(message.code))
                    return "code: integer expected";
            if (message.msg != null && message.hasOwnProperty("msg"))
                if (!$util.isString(message.msg))
                    return "msg: string expected";
            if (!$util.isString(message.objStr))
                return "objStr: string expected";
            return null;
        };

        /**
         * Creates a submit_form_resp message from a plain object. Also converts values to their respective internal types.
         * @function fromObject
         * @memberof form.submit_form_resp
         * @static
         * @param {Object.<string,*>} object Plain object
         * @returns {form.submit_form_resp} submit_form_resp
         */
        submit_form_resp.fromObject = function fromObject(object) {
            if (object instanceof $root.form.submit_form_resp)
                return object;
            var message = new $root.form.submit_form_resp();
            if (object.status != null)
                message.status = object.status | 0;
            if (object.code != null)
                message.code = object.code | 0;
            if (object.msg != null)
                message.msg = String(object.msg);
            if (object.objStr != null)
                message.objStr = String(object.objStr);
            return message;
        };

        /**
         * Creates a plain object from a submit_form_resp message. Also converts values to other types if specified.
         * @function toObject
         * @memberof form.submit_form_resp
         * @static
         * @param {form.submit_form_resp} message submit_form_resp
         * @param {$protobuf.IConversionOptions} [options] Conversion options
         * @returns {Object.<string,*>} Plain object
         */
        submit_form_resp.toObject = function toObject(message, options) {
            if (!options)
                options = {};
            var object = {};
            if (options.defaults) {
                object.status = 0;
                object.code = 0;
                object.msg = "";
                object.objStr = "";
            }
            if (message.status != null && message.hasOwnProperty("status"))
                object.status = message.status;
            if (message.code != null && message.hasOwnProperty("code"))
                object.code = message.code;
            if (message.msg != null && message.hasOwnProperty("msg"))
                object.msg = message.msg;
            if (message.objStr != null && message.hasOwnProperty("objStr"))
                object.objStr = message.objStr;
            return object;
        };

        /**
         * Converts this submit_form_resp to JSON.
         * @function toJSON
         * @memberof form.submit_form_resp
         * @instance
         * @returns {Object.<string,*>} JSON object
         */
        submit_form_resp.prototype.toJSON = function toJSON() {
            return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
        };

        return submit_form_resp;
    })();

    form.form_notify = (function() {

        /**
         * Properties of a form_notify.
         * @memberof form
         * @interface Iform_notify
         * @property {string} objStr form_notify objStr
         */

        /**
         * Constructs a new form_notify.
         * @memberof form
         * @classdesc Represents a form_notify.
         * @implements Iform_notify
         * @constructor
         * @param {form.Iform_notify=} [properties] Properties to set
         */
        function form_notify(properties) {
            if (properties)
                for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                    if (properties[keys[i]] != null)
                        this[keys[i]] = properties[keys[i]];
        }

        /**
         * form_notify objStr.
         * @member {string} objStr
         * @memberof form.form_notify
         * @instance
         */
        form_notify.prototype.objStr = "";

        /**
         * Creates a new form_notify instance using the specified properties.
         * @function create
         * @memberof form.form_notify
         * @static
         * @param {form.Iform_notify=} [properties] Properties to set
         * @returns {form.form_notify} form_notify instance
         */
        form_notify.create = function create(properties) {
            return new form_notify(properties);
        };

        /**
         * Encodes the specified form_notify message. Does not implicitly {@link form.form_notify.verify|verify} messages.
         * @function encode
         * @memberof form.form_notify
         * @static
         * @param {form.Iform_notify} message form_notify message or plain object to encode
         * @param {$protobuf.Writer} [writer] Writer to encode to
         * @returns {$protobuf.Writer} Writer
         */
        form_notify.encode = function encode(message, writer) {
            if (!writer)
                writer = $Writer.create();
            writer.uint32(/* id 1, wireType 2 =*/10).string(message.objStr);
            return writer;
        };

        /**
         * Encodes the specified form_notify message, length delimited. Does not implicitly {@link form.form_notify.verify|verify} messages.
         * @function encodeDelimited
         * @memberof form.form_notify
         * @static
         * @param {form.Iform_notify} message form_notify message or plain object to encode
         * @param {$protobuf.Writer} [writer] Writer to encode to
         * @returns {$protobuf.Writer} Writer
         */
        form_notify.encodeDelimited = function encodeDelimited(message, writer) {
            return this.encode(message, writer).ldelim();
        };

        /**
         * Decodes a form_notify message from the specified reader or buffer.
         * @function decode
         * @memberof form.form_notify
         * @static
         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
         * @param {number} [length] Message length if known beforehand
         * @returns {form.form_notify} form_notify
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        form_notify.decode = function decode(reader, length) {
            if (!(reader instanceof $Reader))
                reader = $Reader.create(reader);
            var end = length === undefined ? reader.len : reader.pos + length, message = new $root.form.form_notify();
            while (reader.pos < end) {
                var tag = reader.uint32();
                switch (tag >>> 3) {
                case 1:
                    message.objStr = reader.string();
                    break;
                default:
                    reader.skipType(tag & 7);
                    break;
                }
            }
            if (!message.hasOwnProperty("objStr"))
                throw $util.ProtocolError("missing required 'objStr'", { instance: message });
            return message;
        };

        /**
         * Decodes a form_notify message from the specified reader or buffer, length delimited.
         * @function decodeDelimited
         * @memberof form.form_notify
         * @static
         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
         * @returns {form.form_notify} form_notify
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        form_notify.decodeDelimited = function decodeDelimited(reader) {
            if (!(reader instanceof $Reader))
                reader = new $Reader(reader);
            return this.decode(reader, reader.uint32());
        };

        /**
         * Verifies a form_notify message.
         * @function verify
         * @memberof form.form_notify
         * @static
         * @param {Object.<string,*>} message Plain object to verify
         * @returns {string|null} `null` if valid, otherwise the reason why it is not
         */
        form_notify.verify = function verify(message) {
            if (typeof message !== "object" || message === null)
                return "object expected";
            if (!$util.isString(message.objStr))
                return "objStr: string expected";
            return null;
        };

        /**
         * Creates a form_notify message from a plain object. Also converts values to their respective internal types.
         * @function fromObject
         * @memberof form.form_notify
         * @static
         * @param {Object.<string,*>} object Plain object
         * @returns {form.form_notify} form_notify
         */
        form_notify.fromObject = function fromObject(object) {
            if (object instanceof $root.form.form_notify)
                return object;
            var message = new $root.form.form_notify();
            if (object.objStr != null)
                message.objStr = String(object.objStr);
            return message;
        };

        /**
         * Creates a plain object from a form_notify message. Also converts values to other types if specified.
         * @function toObject
         * @memberof form.form_notify
         * @static
         * @param {form.form_notify} message form_notify
         * @param {$protobuf.IConversionOptions} [options] Conversion options
         * @returns {Object.<string,*>} Plain object
         */
        form_notify.toObject = function toObject(message, options) {
            if (!options)
                options = {};
            var object = {};
            if (options.defaults)
                object.objStr = "";
            if (message.objStr != null && message.hasOwnProperty("objStr"))
                object.objStr = message.objStr;
            return object;
        };

        /**
         * Converts this form_notify to JSON.
         * @function toJSON
         * @memberof form.form_notify
         * @instance
         * @returns {Object.<string,*>} JSON object
         */
        form_notify.prototype.toJSON = function toJSON() {
            return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
        };

        return form_notify;
    })();

    return form;
})();

$root.form2 = (function() {

    /**
     * Namespace form2.
     * @exports form2
     * @namespace
     */
    var form2 = {};

    form2.FormTest2 = (function() {

        /**
         * Properties of a FormTest2.
         * @memberof form2
         * @interface IFormTest2
         * @property {string} func FormTest2 func
         * @property {string} param FormTest2 param
         */

        /**
         * Constructs a new FormTest2.
         * @memberof form2
         * @classdesc Represents a FormTest2.
         * @implements IFormTest2
         * @constructor
         * @param {form2.IFormTest2=} [properties] Properties to set
         */
        function FormTest2(properties) {
            if (properties)
                for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                    if (properties[keys[i]] != null)
                        this[keys[i]] = properties[keys[i]];
        }

        /**
         * FormTest2 func.
         * @member {string} func
         * @memberof form2.FormTest2
         * @instance
         */
        FormTest2.prototype.func = "";

        /**
         * FormTest2 param.
         * @member {string} param
         * @memberof form2.FormTest2
         * @instance
         */
        FormTest2.prototype.param = "";

        /**
         * Creates a new FormTest2 instance using the specified properties.
         * @function create
         * @memberof form2.FormTest2
         * @static
         * @param {form2.IFormTest2=} [properties] Properties to set
         * @returns {form2.FormTest2} FormTest2 instance
         */
        FormTest2.create = function create(properties) {
            return new FormTest2(properties);
        };

        /**
         * Encodes the specified FormTest2 message. Does not implicitly {@link form2.FormTest2.verify|verify} messages.
         * @function encode
         * @memberof form2.FormTest2
         * @static
         * @param {form2.IFormTest2} message FormTest2 message or plain object to encode
         * @param {$protobuf.Writer} [writer] Writer to encode to
         * @returns {$protobuf.Writer} Writer
         */
        FormTest2.encode = function encode(message, writer) {
            if (!writer)
                writer = $Writer.create();
            writer.uint32(/* id 1, wireType 2 =*/10).string(message.func);
            writer.uint32(/* id 2, wireType 2 =*/18).string(message.param);
            return writer;
        };

        /**
         * Encodes the specified FormTest2 message, length delimited. Does not implicitly {@link form2.FormTest2.verify|verify} messages.
         * @function encodeDelimited
         * @memberof form2.FormTest2
         * @static
         * @param {form2.IFormTest2} message FormTest2 message or plain object to encode
         * @param {$protobuf.Writer} [writer] Writer to encode to
         * @returns {$protobuf.Writer} Writer
         */
        FormTest2.encodeDelimited = function encodeDelimited(message, writer) {
            return this.encode(message, writer).ldelim();
        };

        /**
         * Decodes a FormTest2 message from the specified reader or buffer.
         * @function decode
         * @memberof form2.FormTest2
         * @static
         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
         * @param {number} [length] Message length if known beforehand
         * @returns {form2.FormTest2} FormTest2
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        FormTest2.decode = function decode(reader, length) {
            if (!(reader instanceof $Reader))
                reader = $Reader.create(reader);
            var end = length === undefined ? reader.len : reader.pos + length, message = new $root.form2.FormTest2();
            while (reader.pos < end) {
                var tag = reader.uint32();
                switch (tag >>> 3) {
                case 1:
                    message.func = reader.string();
                    break;
                case 2:
                    message.param = reader.string();
                    break;
                default:
                    reader.skipType(tag & 7);
                    break;
                }
            }
            if (!message.hasOwnProperty("func"))
                throw $util.ProtocolError("missing required 'func'", { instance: message });
            if (!message.hasOwnProperty("param"))
                throw $util.ProtocolError("missing required 'param'", { instance: message });
            return message;
        };

        /**
         * Decodes a FormTest2 message from the specified reader or buffer, length delimited.
         * @function decodeDelimited
         * @memberof form2.FormTest2
         * @static
         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
         * @returns {form2.FormTest2} FormTest2
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        FormTest2.decodeDelimited = function decodeDelimited(reader) {
            if (!(reader instanceof $Reader))
                reader = new $Reader(reader);
            return this.decode(reader, reader.uint32());
        };

        /**
         * Verifies a FormTest2 message.
         * @function verify
         * @memberof form2.FormTest2
         * @static
         * @param {Object.<string,*>} message Plain object to verify
         * @returns {string|null} `null` if valid, otherwise the reason why it is not
         */
        FormTest2.verify = function verify(message) {
            if (typeof message !== "object" || message === null)
                return "object expected";
            if (!$util.isString(message.func))
                return "func: string expected";
            if (!$util.isString(message.param))
                return "param: string expected";
            return null;
        };

        /**
         * Creates a FormTest2 message from a plain object. Also converts values to their respective internal types.
         * @function fromObject
         * @memberof form2.FormTest2
         * @static
         * @param {Object.<string,*>} object Plain object
         * @returns {form2.FormTest2} FormTest2
         */
        FormTest2.fromObject = function fromObject(object) {
            if (object instanceof $root.form2.FormTest2)
                return object;
            var message = new $root.form2.FormTest2();
            if (object.func != null)
                message.func = String(object.func);
            if (object.param != null)
                message.param = String(object.param);
            return message;
        };

        /**
         * Creates a plain object from a FormTest2 message. Also converts values to other types if specified.
         * @function toObject
         * @memberof form2.FormTest2
         * @static
         * @param {form2.FormTest2} message FormTest2
         * @param {$protobuf.IConversionOptions} [options] Conversion options
         * @returns {Object.<string,*>} Plain object
         */
        FormTest2.toObject = function toObject(message, options) {
            if (!options)
                options = {};
            var object = {};
            if (options.defaults) {
                object.func = "";
                object.param = "";
            }
            if (message.func != null && message.hasOwnProperty("func"))
                object.func = message.func;
            if (message.param != null && message.hasOwnProperty("param"))
                object.param = message.param;
            return object;
        };

        /**
         * Converts this FormTest2 to JSON.
         * @function toJSON
         * @memberof form2.FormTest2
         * @instance
         * @returns {Object.<string,*>} JSON object
         */
        FormTest2.prototype.toJSON = function toJSON() {
            return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
        };

        return FormTest2;
    })();

    return form2;
})();

module.exports = $root;
