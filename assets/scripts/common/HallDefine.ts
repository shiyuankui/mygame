// 放置一些大厅模块的宏定义

// 炮台模块分页编号
export enum CANNON_MODULE_TABS {
	CANNON_TAB_PROPERTY 	= 1,			// 炮台属性分页
	CANNON_TAB_COMPONENT,					// 炮台组件分页
	CANNON_TAB_SKIN,						// 炮台皮肤分页
}

// 炮台属性分页的内部分页编号
export enum CANNON_PROPERTY_MODULE_TABS {
	CANNON_PROPERTY_TAB_NUM		= 1,		// 炮号分页
	CANNON_PROPERTY_TAB_MULTI,				// 炮倍分页
	CANNON_PROPERTY_TAB_SPEED				// 射速分页
}