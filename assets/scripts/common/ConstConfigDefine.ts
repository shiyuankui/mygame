////////////////////////////////////////////////////////////
///////////////////b'instruction'///////////////////
////////////////////////////////////////////////////////////
export const CFG_TBL__BET : any = {
	tbl_name : 'Bet',    
	id      : 'id',     //炮倍ID:int
	multi   : 'multi',  //炮倍:int
	item_id : 'item_id',//消耗材料类型:int
	number  : 'number', //消耗数量:int
}
////////////////////////////////////////////////////////////
///////////////////b'instruction'///////////////////
////////////////////////////////////////////////////////////
export const CFG_TBL__BULLETEFFECT : any = {
	tbl_name             : 'BulletEffect',        
	id                   : 'id',                  //子弹效果ID:int
	bullet_effect_config : 'bullet_effect_config',//效果说明:string
	deceleration_rate    : 'deceleration_rate',   //减速率（%）:int
	deceleration_time    : 'deceleration_time',   //减速持续时长（ms）:int
	flex                 : 'flex',                //反弹次数:int
	is_dingdian          : 'is_dingdian',         //是否定点:int
	is_jingzhun          : 'is_jingzhun',         //是否精准:int
	is_zhuizong          : 'is_zhuizong',         //是否追踪:int
	bullet_style         : 'bullet_style',        //子弹样式:string
	bullet_priority      : 'bullet_priority',     //子弹样式优先级:int
	net_style            : 'net_style',           //渔网样式:string
	net_priority         : 'net_priority',        //渔网样式优先级:int
}
////////////////////////////////////////////////////////////
///////////////////b'instruction'///////////////////
////////////////////////////////////////////////////////////
export const CFG_TBL__BULLETSPEED : any = {
	tbl_name             : 'BulletSpeed',         
	id                   : 'id',                  //射速ID:int
	bullet_speed         : 'bullet_speed',        //弹道速度:int
	att_time             : 'att_time',            //发射间隔:int
	Item_id              : 'Item_id',             //消耗材料类型:int
	number               : 'number',              //消耗数量:int
}
////////////////////////////////////////////////////////////
///////////////////b'instruction'///////////////////
////////////////////////////////////////////////////////////
export const CFG_TBL__CANNON : any = {
	tbl_name             : 'Cannon',              
	cannon_id            : 'cannon_id',           //炮台ID:int
	cannon_name          : 'cannon_name',         //炮台名称:string
	att_base             : 'att_base',            //基础攻击力:int
	grade                : 'grade',               //炮台等级:string
	power_max            : 'power_max',           //炮号上限:int
	power_min            : 'power_min',           //炮号下限:int
	bet_max              : 'bet_max',             //炮倍上限:int
	bet_min              : 'bet_min',             //炮倍下限:int
	bullet_speed_max     : 'bullet_speed_max',    //射速上限:int
	bullet_speed_min     : 'bullet_speed_min',    //射速下限:int
	att_radius           : 'att_radius',          //攻击面积:int
	hit                  : 'hit',                 //最大同时命中:int
	bullet_effect        : 'bullet_effect',       //可选子弹:string
}
////////////////////////////////////////////////////////////
///////////////////b'instruction'///////////////////
////////////////////////////////////////////////////////////
export const CFG_TBL__CANNONPARTS : any = {
	tbl_name             : 'CannonParts',         
	id                   : 'id',                  //炮件ID:int
	item_id              : 'item_id',             //炮件的道具ID:int
	group_effect_id      : 'group_effect_id',     //组合技能ID(-1无组合技能）:int
}
////////////////////////////////////////////////////////////
///////////////////b'instruction'///////////////////
////////////////////////////////////////////////////////////
export const CFG_TBL__CANNONSKIN : any = {
	tbl_name             : 'CannonSkin',          
	id                   : 'id',                  //皮肤ID:int
	item_id              : 'item_id',             //皮肤的材料ID:int
}
////////////////////////////////////////////////////////////
///////////////////b'instruction'///////////////////
////////////////////////////////////////////////////////////
export const CFG_TBL__EFFECT : any = {
	tbl_name             : 'Effect',              
	id                   : 'id',                  //技能ID:int
	effect_instruction   : 'effect_instruction',  //技能说明:string
	type                 : 'type',                //加成方式（1百分比/0固定值/-1无此值）:int
	value                : 'value',               //加成值(%/固定值):int
	cannon_parts_id      : 'cannon_parts_id',     //需组合的炮件ID(-1无需组合）:string
}
////////////////////////////////////////////////////////////
///////////////////b'instruction'///////////////////
////////////////////////////////////////////////////////////
export const CFG_TBL__FISHPOND : any = {
	tbl_name             : 'FishPond',            
	fishpond_id          : 'fishpond_id',         //鱼池id:int
	fishpond_level       : 'fishpond_level',      //鱼池等级:int
	weight               : 'weight',              //权重:int
}
////////////////////////////////////////////////////////////
///////////////////b'instruction'///////////////////
////////////////////////////////////////////////////////////
export const CFG_TBL__FISHPONDCONFIG : any = {
	tbl_name             : 'FishPondConfig',      
	id                   : 'id',                  //条目id:int
	pond_id              : 'pond_id',             //鱼池id:int
	fish_id              : 'fish_id',             //鱼id:int
	fish_num             : 'fish_num',            //鱼数量:int
}
////////////////////////////////////////////////////////////
///////////////////b'instruction'///////////////////
////////////////////////////////////////////////////////////
export const CFG_TBL__FISHTIDE : any = {
	tbl_name             : 'FishTide',            
	id                   : 'id',                  //条目id:int
	fishtide_id          : 'fishtide_id',         //鱼潮id:int
	fish_id1             : 'fish_id1',            //鱼id1:int
	fish_weight1         : 'fish_weight1',        //权重1:int
	fish_id2             : 'fish_id2',            //鱼id2:int
	fish_weight2         : 'fish_weight2',        //权重2:int
	fish_id3             : 'fish_id3',            //鱼id3:int
	fish_weight3         : 'fish_weight3',        //权重3:int
	fish_id4             : 'fish_id4',            //鱼id4:int
	fish_weight4         : 'fish_weight4',        //权重4:int
	fish_id5             : 'fish_id5',            //鱼id5:int
	fish_weight5         : 'fish_weight5',        //权重5:int
	fish_num             : 'fish_num',            //鱼数量:int
	interval             : 'interval',            //间隔时间:int
	routes               : 'routes',              //路径:string
	time_node            : 'time_node',           //时间节点:int
	Duration             : 'Duration',            //持续时长:int
	accounts_type        : 'accounts_type',       //结算类型:int
}
////////////////////////////////////////////////////////////
///////////////////b'instruction'///////////////////
////////////////////////////////////////////////////////////
export const CFG_TBL__FISHTYPE : any = {
	tbl_name             : 'FishType',            
	id                   : 'id',                  //鱼ID:int
	fishbooksmall_id     : 'fishbooksmall_id',    //鱼icon:string
	tcbyfish_id          : 'tcbyfish_id',         //鱼动画:string
	score                : 'score',               //分值:int
	hp                   : 'hp',                  //血量:int
	name                 : 'name',                //名称:string
	type                 : 'type',                //种类:int
	MoSpeed              : 'MoSpeed',             //移动速度:int
	weight_1             : 'weight_1',            //房间1出现权重:int
	weight_2             : 'weight_2',            //房间2出现权重:int
	weight_3             : 'weight_3',            //房间3出现权重:int
	weight_4             : 'weight_4',            //房间4出现权重:int
	maxapp               : 'maxapp',              //最大同时出现:int
	rand_type            : 'rand_type',           //路径类型:int
	awards               : 'awards',              //掉落奖励:string
	awardbonus           : 'awardbonus',          //赏金抽水比:string
	awarddragon          : 'awarddragon',         //龙晶抽水比:string
	is_award_eff         : 'is_award_eff',        //奖励特效:string
	dead_sound           : 'dead_sound',          //死亡音效:string
	bubble_msg           : 'bubble_msg',          //气泡文字:string
	kill_rate            : 'kill_rate',           //击杀触发特殊事件概率（单位：万分比）:float
	event_id             : 'event_id',            //特殊事件id:string
	Offset_X             : 'Offset_X',            //碰撞起始中心点X:int
	Offset_Y             : 'Offset_Y',            //碰撞起始中心点Y:int
	Size_Weight          : 'Size_Weight',         //碰撞尺寸宽:int
	Size_High            : 'Size_High',           //碰撞尺寸高:int
}
////////////////////////////////////////////////////////////
///////////////////b'instruction'///////////////////
////////////////////////////////////////////////////////////
export const CFG_TBL__ITEM : any = {
	tbl_name             : 'Item',                
	id                   : 'id',                  //序号:int
	name                 : 'name',                //名称:string
	scription            : 'scription',           //内容描述:string
	buy_id               : 'buy_id',              //购买货币(-1RMB):int
	buy_price            : 'buy_price',           //购买价格:int
	sell_id              : 'sell_id',             //出售货币:int
	sell_price           : 'sell_price',          //出售价格:int
	last                 : 'last',                //持续时间（ms）:int
	stack                : 'stack',               //堆叠数:int
	fragment             : 'fragment',            //可合成碎片ID:int
	fragment_number      : 'fragment_number',     //碎片量:int
	effect_id            : 'effect_id',           //技能效果ID:string
	vip_limit            : 'vip_limit',           //最小可用VIP等级:int
}
////////////////////////////////////////////////////////////
///////////////////b'instruction'///////////////////
////////////////////////////////////////////////////////////
export const CFG_TBL__POWER : any = {
	tbl_name             : 'Power',               
	id                   : 'id',                  //炮号ID:int
	multi                : 'multi',               //炮号值:int
	item_id              : 'item_id',             //消耗材料类型:int
	number               : 'number',              //消耗数量:int
}
////////////////////////////////////////////////////////////
//////////////////////b'describe'//////////////////////
////////////////////////////////////////////////////////////
export const CFG_TBL__ROOMEVENT : any = {
	tbl_name             : 'RoomEvent',           
	events_id            : 'events_id',           //事件id:int
	events_describe      : 'events_describe',     //事件描述:string
	events_type          : 'events_type',         //事件类型:string
	time_interval        : 'time_interval',       //投放间隔时间:int
	killevents_time      : 'killevents_time',     //击杀触发时间:int
	priority             : 'priority',            //优先级:int
	mutex                : 'mutex',               //互斥性:int
}
////////////////////////////////////////////////////////////
//////////////////////b'describe'//////////////////////
////////////////////////////////////////////////////////////
export const CFG_TBL__ROOMEVENTCONFIG : any = {
	tbl_name             : 'RoomEventConfig',     
	id                   : 'id',                  //条目id:int
	events_id            : 'events_id',           //事件id:int
	events_describe      : 'events_describe',     //事件描述:string
	fish_id              : 'fish_id',             //鱼id:string
	fish_num             : 'fish_num',            //鱼数量:int
	fish_weight          : 'fish_weight',         //权重:int
}
////////////////////////////////////////////////////////////
//////////////////////b'describe'//////////////////////
////////////////////////////////////////////////////////////
export const CFG_TBL__ROOMTYPE : any = {
	tbl_name             : 'RoomType',            
	id                   : 'id',                  //索引id:int
	max_players          : 'max_players',         //人数上限:int
	min_cannon           : 'min_cannon',          //最低倍数:int
	max_cannon           : 'max_cannon',          //最高倍数:int
	min_money            : 'min_money',           //最低鱼币:int
	max_money            : 'max_money',           //最高鱼币:int
	room_type            : 'room_type',           //房间类型:int
	room_name            : 'room_name',           //房间名称:string
	weight_index         : 'weight_index',        //权重索引:string
	room_events          : 'room_events',         //事件触发:string
	task_list            : 'task_list',           //循环任务列表:string
	room_fishlist        : 'room_fishlist',       //房间鱼签:string
	room_ratemodify      : 'room_ratemodify',     //房间概率修正:string
	fish_interval        : 'fish_interval',       //放鱼触发间隔:int
	fish_pond            : 'fish_pond',           //初始鱼池:int
}
////////////////////////////////////////////////////////////
///////////////////////b'explain'///////////////////////
////////////////////////////////////////////////////////////
export const CFG_TBL__ROUTESAREA : any = {
	tbl_name             : 'RoutesArea',          
	area                 : 'area',                //区域id:int
	X_Min                : 'X_Min',               //X轴最小值:int
	X_Max                : 'X_Max',               //X轴最大值:int
	Y_Min                : 'Y_Min',               //Y轴最小值:int
	Y_Max                : 'Y_Max',               //Y轴最大值:int
}
////////////////////////////////////////////////////////////
///////////////////////b'explain'///////////////////////
////////////////////////////////////////////////////////////
export const CFG_TBL__ROUTESTYPE : any = {
	tbl_name             : 'RoutesType',          
	routes_id            : 'routes_id',           //路径id:int
	routes               : 'routes',              //路径:string
	routes_type          : 'routes_type',         //路径类型:int
}
////////////////////////////////////////////////////////////
///////////////////////b'explain'///////////////////////
////////////////////////////////////////////////////////////
export const CFG_TBL__SPECIALROUTES : any = {
	tbl_name             : 'SpecialRoutes',       
	routes_id            : 'routes_id',           //路径id:int
	routes               : 'routes',              //路径:string
	fish_type            : 'fish_type',           //适用鱼类型:string
}
