// 定义了客户端与服务端通信的消息变量名，主要是提高代码可读性

// 定义了客户端与服务端通信的结构
export const MSG_STRUCT_LIST = {
	// 请求匹配的消息
	FISH_MATCH_ENTER_REQ: {
		roomCfgId: "roomCfgId"			// 房间类型，number
	},
	// 请求同步房间信息
	FISH_BATTLE_SYNC_ROOMINFO_REQ: {
	},
	// 同步房间信息响应
	FISH_BATTLE_SYNC_ROOMINFO_RSP: {
		stRoomInfo: "stRoomInfo",
		veUserList: "veUserList",
		veFishList: "veFishList"
	},
	// 用户登陆消息
	FISH_LOGIN_REQ: {
		strTcyUserId: 	"strTcyUserId",			// 绑定的tcy用户ID
		nChannelId: 	"nChannelId",			// 渠道ID，同城游为1
		nOsType: 		"nOsType",				// 系统类别，1表示安卓，2表示IOS
		nSex:			"nSex",					// 性别，0表示男生，1表示女生
		nFromId:		"nFromId",				// 单包安卓6,ios 8,zip包安卓66,ios 88
		strDeviceId:	"nstrDeviceId",			// 设备标识ID
		strNickName:	"strNickName",			// 昵称
		strVersion:		"strVersion",			// 客户端当前版本号
		strImei:		"strImei",				// 硬件码
		strImsi:		"strImsi"				// 手机码
	},

	// 鱼的路径结构
	FISH__ROUTE: {
		routeType:		"nRouteType",			// 路径类型
		moveTime:		"nRouteTime",				// 移动时间
		routeId:		"nFishRouteId",				// 路径Id
		strRouteVecs:	"strRouteVecs",			// 路径节点
	},
	// 鱼的buff信息
	FISH__BUFF: {
		nBuffId:			"nBuffId",			// 鱼的BuffId
		nBuffValue:			"nBuffValue",		// 鱼的Buff数值
		nBuffRemainTime:	"nBuffRemainTime",	// 鱼的Buff剩余时间
	},
	// 鱼的信息
	FISH__INFO: {
		nFishId:			"nFishId",			// 鱼的fishId
		nFishIndex:			"nFishIndex",		// 鱼的fishIndex
		fishRoutes:			"fishRoutes",		// 鱼的路径
		fishBuffs:			"fishBuffs",		// 鱼的buff集合
	},

	// 炮台属性
	CANNON__PROPERTY: {
		nCannonNum:			"nCannonNum",			// 炮号
		nCannonSkinId:		"nCannonSkinId",		// 炮衣Id
		nCannonMulti:		"nCannonMulti",			// 炮倍
		nCannonSpeed:		"nCannonSpeed",			// 射速
		nCannonLevel:		"nCannonLevel",			// 等级
		cannonProperties:	"cannonProperties"		// 子弹属性
	},
	// 炮台具体属性
	CANNON__PROPERTY__INFO: {
		nPropertyId:		"nPropertyId",			// 属性Id
		strPropertyValue:	"strPropertyValue"		// 属性值
	},
	// 放鱼通知 
	FISH_NOTIFY: {
		veFishList:			"veFishList",			// 鱼的集合
	},
	// 开火通知
	FIRE_BULLET_NOTIFY: {
		strUserId:			"strUserId",			// 发射子弹的玩家Id
		nConsumeMoney:		"nConsumeMoney",		// 代币消耗
		nAngle:				"nAngle",				// 玩家发射的角度
		nTrackFishIndex:	"nTrackFishIndex",		// 跟踪鱼的索引
	},
	// 玩家数据
	USER__INFO: {
		strUserId:			"strUserId",			// 玩家ID
		name:				"name",					// 名字
		headurl:			"headurl",				// 玩家头像
		nTableNo:			"nTableNo",				// 桌子号
		nBet:				"nBet",					// 炮倍
		nMoney:				"nMoney",				// 鱼币
		nExp:				"nExp",					// 经验
		nVip:				"nVip",					// vip等级
		cannonInfo:			"cannonInfo",			// 炮台信息
	},
	// 炮台数据
	CANNON__INFO: {
		nCannonId:			"nCannonId",			// 炮台ID
		veEffectList:		"veEffectList",			// 炮台效果
	},
	// 炮台属性
	CANNON__EFFECT__INFO: {
		nCannonEffectId:		"nCannonEffectId",		// 炮台效果Id
		strEffectProperties:	"strEffectProperties",	// 炮台属性列表
	},
	// 鱼和子弹碰撞请求
	BULLET_AND_FISH_COLLISION_REQ: {
		nBulletIndex:		"nBulletIndex",				// 子弹的索引
		fishIdList:			"fishIdList",				// 鱼的列表
	},

	// 奖励物品列表
	REWARD_ITEM_INFO: {
		nItemId:			"nItemId",					// 物品Id
		nItemCount:			"nItemCount",				// 物品数量
	},

	// 击中鱼的奖励
	FISH_REWARD_INFO: {
		nFishId:			"nFishId",					// 鱼的Id
		rewardInfo:			"rewardInfo",				// 击中鱼的奖励
	},

	// 鱼和子弹碰撞通知
	BULLET_AND_FISH_COLLISION_NOTIFY: {
		strUserId: 			"strUserId",				// 碰撞的鱼的列表
		fishInfo:			"fishInfo",					// 鱼的信息
		nDie:				"nDie",						// 鱼是否死亡
	},

	// 鱼的buff信息
	FISH_BUFF_INFO: {
		nFishId:			"nFishId",					// 鱼的id
		strFishBuff:		"strFishBuff",				// 玩家的buff状态
	},

	// 碰撞buff通知
	BULLET_AND_FISH_COLLISION_BUFF_NOTIFY: {
		strUserId:			"strUserId",				// 发射子弹的用户ID
		fishBuffInfo:		"fishBuffInfo",				// 鱼的属性列表
	}
}

// 定义了需要发送给服务端的通信的结构
export const MSG_DATA_STRUCT_LIST = {
	// 请求匹配的数据
	FISH_MATCH_ENTER_REQ: {
		roomCfgId: 1,					// 匹配的响应，初值为1，number
	},
	// 用户登陆消息
	FISH_LOGIN_REQ: {
		strTcyUserId: 	-1,
		nChannelId: 	1,
		nOsType: 		1,
		nSex:			0,
		nFromId:		6,
		strDeviceId:	1,
		strNickName:	"defaultNickName",
		strVersion:		"strVersion",
		strImei:		"strImei",
		strImsi:		"strImsi"
	},

	// 子弹发射请求
	FIRE_BULLET_REQ: {
		nBulletIndex:		-1,
		nAngle:				-1,
		nBatteryMulti:		0,
		nTrackFishIndex:	-1,
	},

	// 炮台属性信息
	CANNON_PROPERTY: {
		nCannonNum:		-1,
		nCannonSkinId:	-1,
		nCannonMulti:	-1,
		nCannonSpeed:	-1,

	},

	// 鱼和子弹碰撞请求
	BULLET_AND_FISH_COLLISION_REQ: {
		nBulletIndex:		-1,				// 子弹的索引
		fishIdList:			[],				// 鱼的列表
	}
}

/**
 * [getMsgStructData 获取消息结构的数据]
 * @param {[type]} msgName [消息名]
 */
export function getMsgStructData(msgName) {
	return JSON.parse(JSON.stringify(msgName));
}




