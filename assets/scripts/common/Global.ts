import {AppManager} from "../manager/AppManager";

class GlobalInstance {
    public static readonly Instance: GlobalInstance = new GlobalInstance();
    public app: AppManager = null;

    public visibleSize: any	= cc.view.getVisibleSize();					// 获取屏幕分辨率
    
    public designSize: any	= cc.view.getDesignResolutionSize();		// 获取设计分辨率

    public tempUserId: string = "123456";				// 临时使用的，到时候干掉

    constructor(){

    }
}

export const G = GlobalInstance.Instance