// 碰撞组
export const ColliderGroups = {
	"GroupWall": 	"wall",
	"GroupBullet": 	"bullet",
	"GroupFish":	"fish",
}

// 每帧最多创建的鱼的数量
export const MAX_CREATE_FISH_NUM_EACH_FRAME = 50;

// 池子中最大的鱼的数量
export const MAX_FISH_POOL_NUM				= 50;

// 每次最大的碰撞鱼的数量
export const MAX_FISH_COLLISION_NUM			= 3;

export enum FISH_ROUTE_TYPE {
	ROUTE_TYPE_LINE,				// 直线
	ROUTE_TYPE_BEZIER,				// 贝塞尔曲线
	ROUTE_TYPE_CURVE				// 普通曲线
}

// 玩家当前所处的子弹发射状态，自动、 追踪、 or 其他？ (比如定点)
export enum FIRE_BULLET_STATUS {
	BULLET_STATUS_GENERAL,			// 正常开炮状态
	BULLET_STATUS_TRACK,			// 自动追踪状态
}

// 子弹的属性
export enum BULLET_PROPERTY {
	PROPERTY_REBOUND 		= 1001,			// 反弹
	PROPERTY_DECELERATE						// 减速
}

// 房间中最大的玩家数量
export const ROOM_MAX_PLAYER_NUM			= 4;

//技能
export enum SKILL_NOTIFY {
	ETRACK,        //锁定
	EFROZEN,       //冰冻
	EBOMB,         //炸弹
	ECRAZY,        //狂暴
}

// 炮台动画名
export const BATTERY_ANIMATION = {
	BATTERY_ANIMATION_IDLE 		: 	"battery_idle",				// 闲置动画
	BATTERY_ANIMATION_FIRE 		: 	"battery_fire",				// 开火动画
	BATTERY_ANIMATION_SHOW 		:	"battery_show",				// 炮台显示动画
	BATTERY_ANIMATION_HIDE 		: 	"battery_hide",				// 炮台隐藏动画
	BATTERY_ANIMATION_BULLET	:	"battery_bullet",			// 炮台发射的子弹动画
	BATTERY_ANIMATION_NET		: 	"battery_net",				// 炮台网的动画
}

// 鱼的动画名
export const FISH_ANIMATION = {
	FISH_ANIMATION_MOVE			: 	"fishMove",
	FISH_ANIMATION_DIE			: 	"fishDie",
	FISH_ANIMATION_HURT			:	"fishHurt",
}