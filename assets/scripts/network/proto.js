/*eslint-disable block-scoped-var, id-length, no-control-regex, no-magic-numbers, no-prototype-builtins, no-redeclare, no-shadow, no-var, sort-vars*/
"use strict";

// var $protobuf = require("protobufjs/minimal");
var $protobuf = protobuf;

// Common aliases
var $Reader = $protobuf.Reader, $Writer = $protobuf.Writer, $util = $protobuf.util;

// Exported root namespace
var $root = $protobuf.roots["default"] || ($protobuf.roots["default"] = {});

$root.fish_battle = (function() {

    /**
     * Namespace fish_battle.
     * @exports fish_battle
     * @namespace
     */
    var fish_battle = {};

    fish_battle.fish_battle_sync_roominfo_req = (function() {

        /**
         * Properties of a fish_battle_sync_roominfo_req.
         * @memberof fish_battle
         * @interface Ifish_battle_sync_roominfo_req
         */

        /**
         * Constructs a new fish_battle_sync_roominfo_req.
         * @memberof fish_battle
         * @classdesc Represents a fish_battle_sync_roominfo_req.
         * @implements Ifish_battle_sync_roominfo_req
         * @constructor
         * @param {fish_battle.Ifish_battle_sync_roominfo_req=} [properties] Properties to set
         */
        function fish_battle_sync_roominfo_req(properties) {
            if (properties)
                for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                    if (properties[keys[i]] != null)
                        this[keys[i]] = properties[keys[i]];
        }

        /**
         * Creates a new fish_battle_sync_roominfo_req instance using the specified properties.
         * @function create
         * @memberof fish_battle.fish_battle_sync_roominfo_req
         * @static
         * @param {fish_battle.Ifish_battle_sync_roominfo_req=} [properties] Properties to set
         * @returns {fish_battle.fish_battle_sync_roominfo_req} fish_battle_sync_roominfo_req instance
         */
        fish_battle_sync_roominfo_req.create = function create(properties) {
            return new fish_battle_sync_roominfo_req(properties);
        };

        /**
         * Encodes the specified fish_battle_sync_roominfo_req message. Does not implicitly {@link fish_battle.fish_battle_sync_roominfo_req.verify|verify} messages.
         * @function encode
         * @memberof fish_battle.fish_battle_sync_roominfo_req
         * @static
         * @param {fish_battle.Ifish_battle_sync_roominfo_req} message fish_battle_sync_roominfo_req message or plain object to encode
         * @param {$protobuf.Writer} [writer] Writer to encode to
         * @returns {$protobuf.Writer} Writer
         */
        fish_battle_sync_roominfo_req.encode = function encode(message, writer) {
            if (!writer)
                writer = $Writer.create();
            return writer;
        };

        /**
         * Encodes the specified fish_battle_sync_roominfo_req message, length delimited. Does not implicitly {@link fish_battle.fish_battle_sync_roominfo_req.verify|verify} messages.
         * @function encodeDelimited
         * @memberof fish_battle.fish_battle_sync_roominfo_req
         * @static
         * @param {fish_battle.Ifish_battle_sync_roominfo_req} message fish_battle_sync_roominfo_req message or plain object to encode
         * @param {$protobuf.Writer} [writer] Writer to encode to
         * @returns {$protobuf.Writer} Writer
         */
        fish_battle_sync_roominfo_req.encodeDelimited = function encodeDelimited(message, writer) {
            return this.encode(message, writer).ldelim();
        };

        /**
         * Decodes a fish_battle_sync_roominfo_req message from the specified reader or buffer.
         * @function decode
         * @memberof fish_battle.fish_battle_sync_roominfo_req
         * @static
         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
         * @param {number} [length] Message length if known beforehand
         * @returns {fish_battle.fish_battle_sync_roominfo_req} fish_battle_sync_roominfo_req
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        fish_battle_sync_roominfo_req.decode = function decode(reader, length) {
            if (!(reader instanceof $Reader))
                reader = $Reader.create(reader);
            var end = length === undefined ? reader.len : reader.pos + length, message = new $root.fish_battle.fish_battle_sync_roominfo_req();
            while (reader.pos < end) {
                var tag = reader.uint32();
                switch (tag >>> 3) {
                default:
                    reader.skipType(tag & 7);
                    break;
                }
            }
            return message;
        };

        /**
         * Decodes a fish_battle_sync_roominfo_req message from the specified reader or buffer, length delimited.
         * @function decodeDelimited
         * @memberof fish_battle.fish_battle_sync_roominfo_req
         * @static
         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
         * @returns {fish_battle.fish_battle_sync_roominfo_req} fish_battle_sync_roominfo_req
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        fish_battle_sync_roominfo_req.decodeDelimited = function decodeDelimited(reader) {
            if (!(reader instanceof $Reader))
                reader = new $Reader(reader);
            return this.decode(reader, reader.uint32());
        };

        /**
         * Verifies a fish_battle_sync_roominfo_req message.
         * @function verify
         * @memberof fish_battle.fish_battle_sync_roominfo_req
         * @static
         * @param {Object.<string,*>} message Plain object to verify
         * @returns {string|null} `null` if valid, otherwise the reason why it is not
         */
        fish_battle_sync_roominfo_req.verify = function verify(message) {
            if (typeof message !== "object" || message === null)
                return "object expected";
            return null;
        };

        /**
         * Creates a fish_battle_sync_roominfo_req message from a plain object. Also converts values to their respective internal types.
         * @function fromObject
         * @memberof fish_battle.fish_battle_sync_roominfo_req
         * @static
         * @param {Object.<string,*>} object Plain object
         * @returns {fish_battle.fish_battle_sync_roominfo_req} fish_battle_sync_roominfo_req
         */
        fish_battle_sync_roominfo_req.fromObject = function fromObject(object) {
            if (object instanceof $root.fish_battle.fish_battle_sync_roominfo_req)
                return object;
            return new $root.fish_battle.fish_battle_sync_roominfo_req();
        };

        /**
         * Creates a plain object from a fish_battle_sync_roominfo_req message. Also converts values to other types if specified.
         * @function toObject
         * @memberof fish_battle.fish_battle_sync_roominfo_req
         * @static
         * @param {fish_battle.fish_battle_sync_roominfo_req} message fish_battle_sync_roominfo_req
         * @param {$protobuf.IConversionOptions} [options] Conversion options
         * @returns {Object.<string,*>} Plain object
         */
        fish_battle_sync_roominfo_req.toObject = function toObject() {
            return {};
        };

        /**
         * Converts this fish_battle_sync_roominfo_req to JSON.
         * @function toJSON
         * @memberof fish_battle.fish_battle_sync_roominfo_req
         * @instance
         * @returns {Object.<string,*>} JSON object
         */
        fish_battle_sync_roominfo_req.prototype.toJSON = function toJSON() {
            return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
        };

        return fish_battle_sync_roominfo_req;
    })();

    fish_battle.fish_battle_sync_roominfo_rsp = (function() {

        /**
         * Properties of a fish_battle_sync_roominfo_rsp.
         * @memberof fish_battle
         * @interface Ifish_battle_sync_roominfo_rsp
         * @property {fish_battle.Iroom__info} stRoomInfo fish_battle_sync_roominfo_rsp stRoomInfo
         * @property {Array.<fish_battle.Iuser__info>|null} [veUserList] fish_battle_sync_roominfo_rsp veUserList
         * @property {Array.<fish_battle.Ifish__info>|null} [veFishList] fish_battle_sync_roominfo_rsp veFishList
         */

        /**
         * Constructs a new fish_battle_sync_roominfo_rsp.
         * @memberof fish_battle
         * @classdesc Represents a fish_battle_sync_roominfo_rsp.
         * @implements Ifish_battle_sync_roominfo_rsp
         * @constructor
         * @param {fish_battle.Ifish_battle_sync_roominfo_rsp=} [properties] Properties to set
         */
        function fish_battle_sync_roominfo_rsp(properties) {
            this.veUserList = [];
            this.veFishList = [];
            if (properties)
                for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                    if (properties[keys[i]] != null)
                        this[keys[i]] = properties[keys[i]];
        }

        /**
         * fish_battle_sync_roominfo_rsp stRoomInfo.
         * @member {fish_battle.Iroom__info} stRoomInfo
         * @memberof fish_battle.fish_battle_sync_roominfo_rsp
         * @instance
         */
        fish_battle_sync_roominfo_rsp.prototype.stRoomInfo = null;

        /**
         * fish_battle_sync_roominfo_rsp veUserList.
         * @member {Array.<fish_battle.Iuser__info>} veUserList
         * @memberof fish_battle.fish_battle_sync_roominfo_rsp
         * @instance
         */
        fish_battle_sync_roominfo_rsp.prototype.veUserList = $util.emptyArray;

        /**
         * fish_battle_sync_roominfo_rsp veFishList.
         * @member {Array.<fish_battle.Ifish__info>} veFishList
         * @memberof fish_battle.fish_battle_sync_roominfo_rsp
         * @instance
         */
        fish_battle_sync_roominfo_rsp.prototype.veFishList = $util.emptyArray;

        /**
         * Creates a new fish_battle_sync_roominfo_rsp instance using the specified properties.
         * @function create
         * @memberof fish_battle.fish_battle_sync_roominfo_rsp
         * @static
         * @param {fish_battle.Ifish_battle_sync_roominfo_rsp=} [properties] Properties to set
         * @returns {fish_battle.fish_battle_sync_roominfo_rsp} fish_battle_sync_roominfo_rsp instance
         */
        fish_battle_sync_roominfo_rsp.create = function create(properties) {
            return new fish_battle_sync_roominfo_rsp(properties);
        };

        /**
         * Encodes the specified fish_battle_sync_roominfo_rsp message. Does not implicitly {@link fish_battle.fish_battle_sync_roominfo_rsp.verify|verify} messages.
         * @function encode
         * @memberof fish_battle.fish_battle_sync_roominfo_rsp
         * @static
         * @param {fish_battle.Ifish_battle_sync_roominfo_rsp} message fish_battle_sync_roominfo_rsp message or plain object to encode
         * @param {$protobuf.Writer} [writer] Writer to encode to
         * @returns {$protobuf.Writer} Writer
         */
        fish_battle_sync_roominfo_rsp.encode = function encode(message, writer) {
            if (!writer)
                writer = $Writer.create();
            $root.fish_battle.room__info.encode(message.stRoomInfo, writer.uint32(/* id 1, wireType 2 =*/10).fork()).ldelim();
            if (message.veUserList != null && message.veUserList.length)
                for (var i = 0; i < message.veUserList.length; ++i)
                    $root.fish_battle.user__info.encode(message.veUserList[i], writer.uint32(/* id 2, wireType 2 =*/18).fork()).ldelim();
            if (message.veFishList != null && message.veFishList.length)
                for (var i = 0; i < message.veFishList.length; ++i)
                    $root.fish_battle.fish__info.encode(message.veFishList[i], writer.uint32(/* id 3, wireType 2 =*/26).fork()).ldelim();
            return writer;
        };

        /**
         * Encodes the specified fish_battle_sync_roominfo_rsp message, length delimited. Does not implicitly {@link fish_battle.fish_battle_sync_roominfo_rsp.verify|verify} messages.
         * @function encodeDelimited
         * @memberof fish_battle.fish_battle_sync_roominfo_rsp
         * @static
         * @param {fish_battle.Ifish_battle_sync_roominfo_rsp} message fish_battle_sync_roominfo_rsp message or plain object to encode
         * @param {$protobuf.Writer} [writer] Writer to encode to
         * @returns {$protobuf.Writer} Writer
         */
        fish_battle_sync_roominfo_rsp.encodeDelimited = function encodeDelimited(message, writer) {
            return this.encode(message, writer).ldelim();
        };

        /**
         * Decodes a fish_battle_sync_roominfo_rsp message from the specified reader or buffer.
         * @function decode
         * @memberof fish_battle.fish_battle_sync_roominfo_rsp
         * @static
         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
         * @param {number} [length] Message length if known beforehand
         * @returns {fish_battle.fish_battle_sync_roominfo_rsp} fish_battle_sync_roominfo_rsp
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        fish_battle_sync_roominfo_rsp.decode = function decode(reader, length) {
            if (!(reader instanceof $Reader))
                reader = $Reader.create(reader);
            var end = length === undefined ? reader.len : reader.pos + length, message = new $root.fish_battle.fish_battle_sync_roominfo_rsp();
            while (reader.pos < end) {
                var tag = reader.uint32();
                switch (tag >>> 3) {
                case 1:
                    message.stRoomInfo = $root.fish_battle.room__info.decode(reader, reader.uint32());
                    break;
                case 2:
                    if (!(message.veUserList && message.veUserList.length))
                        message.veUserList = [];
                    message.veUserList.push($root.fish_battle.user__info.decode(reader, reader.uint32()));
                    break;
                case 3:
                    if (!(message.veFishList && message.veFishList.length))
                        message.veFishList = [];
                    message.veFishList.push($root.fish_battle.fish__info.decode(reader, reader.uint32()));
                    break;
                default:
                    reader.skipType(tag & 7);
                    break;
                }
            }
            if (!message.hasOwnProperty("stRoomInfo"))
                throw $util.ProtocolError("missing required 'stRoomInfo'", { instance: message });
            return message;
        };

        /**
         * Decodes a fish_battle_sync_roominfo_rsp message from the specified reader or buffer, length delimited.
         * @function decodeDelimited
         * @memberof fish_battle.fish_battle_sync_roominfo_rsp
         * @static
         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
         * @returns {fish_battle.fish_battle_sync_roominfo_rsp} fish_battle_sync_roominfo_rsp
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        fish_battle_sync_roominfo_rsp.decodeDelimited = function decodeDelimited(reader) {
            if (!(reader instanceof $Reader))
                reader = new $Reader(reader);
            return this.decode(reader, reader.uint32());
        };

        /**
         * Verifies a fish_battle_sync_roominfo_rsp message.
         * @function verify
         * @memberof fish_battle.fish_battle_sync_roominfo_rsp
         * @static
         * @param {Object.<string,*>} message Plain object to verify
         * @returns {string|null} `null` if valid, otherwise the reason why it is not
         */
        fish_battle_sync_roominfo_rsp.verify = function verify(message) {
            if (typeof message !== "object" || message === null)
                return "object expected";
            {
                var error = $root.fish_battle.room__info.verify(message.stRoomInfo);
                if (error)
                    return "stRoomInfo." + error;
            }
            if (message.veUserList != null && message.hasOwnProperty("veUserList")) {
                if (!Array.isArray(message.veUserList))
                    return "veUserList: array expected";
                for (var i = 0; i < message.veUserList.length; ++i) {
                    var error = $root.fish_battle.user__info.verify(message.veUserList[i]);
                    if (error)
                        return "veUserList." + error;
                }
            }
            if (message.veFishList != null && message.hasOwnProperty("veFishList")) {
                if (!Array.isArray(message.veFishList))
                    return "veFishList: array expected";
                for (var i = 0; i < message.veFishList.length; ++i) {
                    var error = $root.fish_battle.fish__info.verify(message.veFishList[i]);
                    if (error)
                        return "veFishList." + error;
                }
            }
            return null;
        };

        /**
         * Creates a fish_battle_sync_roominfo_rsp message from a plain object. Also converts values to their respective internal types.
         * @function fromObject
         * @memberof fish_battle.fish_battle_sync_roominfo_rsp
         * @static
         * @param {Object.<string,*>} object Plain object
         * @returns {fish_battle.fish_battle_sync_roominfo_rsp} fish_battle_sync_roominfo_rsp
         */
        fish_battle_sync_roominfo_rsp.fromObject = function fromObject(object) {
            if (object instanceof $root.fish_battle.fish_battle_sync_roominfo_rsp)
                return object;
            var message = new $root.fish_battle.fish_battle_sync_roominfo_rsp();
            if (object.stRoomInfo != null) {
                if (typeof object.stRoomInfo !== "object")
                    throw TypeError(".fish_battle.fish_battle_sync_roominfo_rsp.stRoomInfo: object expected");
                message.stRoomInfo = $root.fish_battle.room__info.fromObject(object.stRoomInfo);
            }
            if (object.veUserList) {
                if (!Array.isArray(object.veUserList))
                    throw TypeError(".fish_battle.fish_battle_sync_roominfo_rsp.veUserList: array expected");
                message.veUserList = [];
                for (var i = 0; i < object.veUserList.length; ++i) {
                    if (typeof object.veUserList[i] !== "object")
                        throw TypeError(".fish_battle.fish_battle_sync_roominfo_rsp.veUserList: object expected");
                    message.veUserList[i] = $root.fish_battle.user__info.fromObject(object.veUserList[i]);
                }
            }
            if (object.veFishList) {
                if (!Array.isArray(object.veFishList))
                    throw TypeError(".fish_battle.fish_battle_sync_roominfo_rsp.veFishList: array expected");
                message.veFishList = [];
                for (var i = 0; i < object.veFishList.length; ++i) {
                    if (typeof object.veFishList[i] !== "object")
                        throw TypeError(".fish_battle.fish_battle_sync_roominfo_rsp.veFishList: object expected");
                    message.veFishList[i] = $root.fish_battle.fish__info.fromObject(object.veFishList[i]);
                }
            }
            return message;
        };

        /**
         * Creates a plain object from a fish_battle_sync_roominfo_rsp message. Also converts values to other types if specified.
         * @function toObject
         * @memberof fish_battle.fish_battle_sync_roominfo_rsp
         * @static
         * @param {fish_battle.fish_battle_sync_roominfo_rsp} message fish_battle_sync_roominfo_rsp
         * @param {$protobuf.IConversionOptions} [options] Conversion options
         * @returns {Object.<string,*>} Plain object
         */
        fish_battle_sync_roominfo_rsp.toObject = function toObject(message, options) {
            if (!options)
                options = {};
            var object = {};
            if (options.arrays || options.defaults) {
                object.veUserList = [];
                object.veFishList = [];
            }
            if (options.defaults)
                object.stRoomInfo = null;
            if (message.stRoomInfo != null && message.hasOwnProperty("stRoomInfo"))
                object.stRoomInfo = $root.fish_battle.room__info.toObject(message.stRoomInfo, options);
            if (message.veUserList && message.veUserList.length) {
                object.veUserList = [];
                for (var j = 0; j < message.veUserList.length; ++j)
                    object.veUserList[j] = $root.fish_battle.user__info.toObject(message.veUserList[j], options);
            }
            if (message.veFishList && message.veFishList.length) {
                object.veFishList = [];
                for (var j = 0; j < message.veFishList.length; ++j)
                    object.veFishList[j] = $root.fish_battle.fish__info.toObject(message.veFishList[j], options);
            }
            return object;
        };

        /**
         * Converts this fish_battle_sync_roominfo_rsp to JSON.
         * @function toJSON
         * @memberof fish_battle.fish_battle_sync_roominfo_rsp
         * @instance
         * @returns {Object.<string,*>} JSON object
         */
        fish_battle_sync_roominfo_rsp.prototype.toJSON = function toJSON() {
            return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
        };

        return fish_battle_sync_roominfo_rsp;
    })();

    fish_battle.cannon__effect_info = (function() {

        /**
         * Properties of a cannon__effect_info.
         * @memberof fish_battle
         * @interface Icannon__effect_info
         * @property {number} nCannonEffectId cannon__effect_info nCannonEffectId
         * @property {string} strEffectProperties cannon__effect_info strEffectProperties
         */

        /**
         * Constructs a new cannon__effect_info.
         * @memberof fish_battle
         * @classdesc Represents a cannon__effect_info.
         * @implements Icannon__effect_info
         * @constructor
         * @param {fish_battle.Icannon__effect_info=} [properties] Properties to set
         */
        function cannon__effect_info(properties) {
            if (properties)
                for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                    if (properties[keys[i]] != null)
                        this[keys[i]] = properties[keys[i]];
        }

        /**
         * cannon__effect_info nCannonEffectId.
         * @member {number} nCannonEffectId
         * @memberof fish_battle.cannon__effect_info
         * @instance
         */
        cannon__effect_info.prototype.nCannonEffectId = 0;

        /**
         * cannon__effect_info strEffectProperties.
         * @member {string} strEffectProperties
         * @memberof fish_battle.cannon__effect_info
         * @instance
         */
        cannon__effect_info.prototype.strEffectProperties = "";

        /**
         * Creates a new cannon__effect_info instance using the specified properties.
         * @function create
         * @memberof fish_battle.cannon__effect_info
         * @static
         * @param {fish_battle.Icannon__effect_info=} [properties] Properties to set
         * @returns {fish_battle.cannon__effect_info} cannon__effect_info instance
         */
        cannon__effect_info.create = function create(properties) {
            return new cannon__effect_info(properties);
        };

        /**
         * Encodes the specified cannon__effect_info message. Does not implicitly {@link fish_battle.cannon__effect_info.verify|verify} messages.
         * @function encode
         * @memberof fish_battle.cannon__effect_info
         * @static
         * @param {fish_battle.Icannon__effect_info} message cannon__effect_info message or plain object to encode
         * @param {$protobuf.Writer} [writer] Writer to encode to
         * @returns {$protobuf.Writer} Writer
         */
        cannon__effect_info.encode = function encode(message, writer) {
            if (!writer)
                writer = $Writer.create();
            writer.uint32(/* id 1, wireType 0 =*/8).int32(message.nCannonEffectId);
            writer.uint32(/* id 2, wireType 2 =*/18).string(message.strEffectProperties);
            return writer;
        };

        /**
         * Encodes the specified cannon__effect_info message, length delimited. Does not implicitly {@link fish_battle.cannon__effect_info.verify|verify} messages.
         * @function encodeDelimited
         * @memberof fish_battle.cannon__effect_info
         * @static
         * @param {fish_battle.Icannon__effect_info} message cannon__effect_info message or plain object to encode
         * @param {$protobuf.Writer} [writer] Writer to encode to
         * @returns {$protobuf.Writer} Writer
         */
        cannon__effect_info.encodeDelimited = function encodeDelimited(message, writer) {
            return this.encode(message, writer).ldelim();
        };

        /**
         * Decodes a cannon__effect_info message from the specified reader or buffer.
         * @function decode
         * @memberof fish_battle.cannon__effect_info
         * @static
         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
         * @param {number} [length] Message length if known beforehand
         * @returns {fish_battle.cannon__effect_info} cannon__effect_info
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        cannon__effect_info.decode = function decode(reader, length) {
            if (!(reader instanceof $Reader))
                reader = $Reader.create(reader);
            var end = length === undefined ? reader.len : reader.pos + length, message = new $root.fish_battle.cannon__effect_info();
            while (reader.pos < end) {
                var tag = reader.uint32();
                switch (tag >>> 3) {
                case 1:
                    message.nCannonEffectId = reader.int32();
                    break;
                case 2:
                    message.strEffectProperties = reader.string();
                    break;
                default:
                    reader.skipType(tag & 7);
                    break;
                }
            }
            if (!message.hasOwnProperty("nCannonEffectId"))
                throw $util.ProtocolError("missing required 'nCannonEffectId'", { instance: message });
            if (!message.hasOwnProperty("strEffectProperties"))
                throw $util.ProtocolError("missing required 'strEffectProperties'", { instance: message });
            return message;
        };

        /**
         * Decodes a cannon__effect_info message from the specified reader or buffer, length delimited.
         * @function decodeDelimited
         * @memberof fish_battle.cannon__effect_info
         * @static
         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
         * @returns {fish_battle.cannon__effect_info} cannon__effect_info
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        cannon__effect_info.decodeDelimited = function decodeDelimited(reader) {
            if (!(reader instanceof $Reader))
                reader = new $Reader(reader);
            return this.decode(reader, reader.uint32());
        };

        /**
         * Verifies a cannon__effect_info message.
         * @function verify
         * @memberof fish_battle.cannon__effect_info
         * @static
         * @param {Object.<string,*>} message Plain object to verify
         * @returns {string|null} `null` if valid, otherwise the reason why it is not
         */
        cannon__effect_info.verify = function verify(message) {
            if (typeof message !== "object" || message === null)
                return "object expected";
            if (!$util.isInteger(message.nCannonEffectId))
                return "nCannonEffectId: integer expected";
            if (!$util.isString(message.strEffectProperties))
                return "strEffectProperties: string expected";
            return null;
        };

        /**
         * Creates a cannon__effect_info message from a plain object. Also converts values to their respective internal types.
         * @function fromObject
         * @memberof fish_battle.cannon__effect_info
         * @static
         * @param {Object.<string,*>} object Plain object
         * @returns {fish_battle.cannon__effect_info} cannon__effect_info
         */
        cannon__effect_info.fromObject = function fromObject(object) {
            if (object instanceof $root.fish_battle.cannon__effect_info)
                return object;
            var message = new $root.fish_battle.cannon__effect_info();
            if (object.nCannonEffectId != null)
                message.nCannonEffectId = object.nCannonEffectId | 0;
            if (object.strEffectProperties != null)
                message.strEffectProperties = String(object.strEffectProperties);
            return message;
        };

        /**
         * Creates a plain object from a cannon__effect_info message. Also converts values to other types if specified.
         * @function toObject
         * @memberof fish_battle.cannon__effect_info
         * @static
         * @param {fish_battle.cannon__effect_info} message cannon__effect_info
         * @param {$protobuf.IConversionOptions} [options] Conversion options
         * @returns {Object.<string,*>} Plain object
         */
        cannon__effect_info.toObject = function toObject(message, options) {
            if (!options)
                options = {};
            var object = {};
            if (options.defaults) {
                object.nCannonEffectId = 0;
                object.strEffectProperties = "";
            }
            if (message.nCannonEffectId != null && message.hasOwnProperty("nCannonEffectId"))
                object.nCannonEffectId = message.nCannonEffectId;
            if (message.strEffectProperties != null && message.hasOwnProperty("strEffectProperties"))
                object.strEffectProperties = message.strEffectProperties;
            return object;
        };

        /**
         * Converts this cannon__effect_info to JSON.
         * @function toJSON
         * @memberof fish_battle.cannon__effect_info
         * @instance
         * @returns {Object.<string,*>} JSON object
         */
        cannon__effect_info.prototype.toJSON = function toJSON() {
            return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
        };

        return cannon__effect_info;
    })();

    fish_battle.cannon__info = (function() {

        /**
         * Properties of a cannon__info.
         * @memberof fish_battle
         * @interface Icannon__info
         * @property {number} nCannonId cannon__info nCannonId
         * @property {Array.<fish_battle.Icannon__effect_info>|null} [veEffectList] cannon__info veEffectList
         */

        /**
         * Constructs a new cannon__info.
         * @memberof fish_battle
         * @classdesc Represents a cannon__info.
         * @implements Icannon__info
         * @constructor
         * @param {fish_battle.Icannon__info=} [properties] Properties to set
         */
        function cannon__info(properties) {
            this.veEffectList = [];
            if (properties)
                for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                    if (properties[keys[i]] != null)
                        this[keys[i]] = properties[keys[i]];
        }

        /**
         * cannon__info nCannonId.
         * @member {number} nCannonId
         * @memberof fish_battle.cannon__info
         * @instance
         */
        cannon__info.prototype.nCannonId = 0;

        /**
         * cannon__info veEffectList.
         * @member {Array.<fish_battle.Icannon__effect_info>} veEffectList
         * @memberof fish_battle.cannon__info
         * @instance
         */
        cannon__info.prototype.veEffectList = $util.emptyArray;

        /**
         * Creates a new cannon__info instance using the specified properties.
         * @function create
         * @memberof fish_battle.cannon__info
         * @static
         * @param {fish_battle.Icannon__info=} [properties] Properties to set
         * @returns {fish_battle.cannon__info} cannon__info instance
         */
        cannon__info.create = function create(properties) {
            return new cannon__info(properties);
        };

        /**
         * Encodes the specified cannon__info message. Does not implicitly {@link fish_battle.cannon__info.verify|verify} messages.
         * @function encode
         * @memberof fish_battle.cannon__info
         * @static
         * @param {fish_battle.Icannon__info} message cannon__info message or plain object to encode
         * @param {$protobuf.Writer} [writer] Writer to encode to
         * @returns {$protobuf.Writer} Writer
         */
        cannon__info.encode = function encode(message, writer) {
            if (!writer)
                writer = $Writer.create();
            writer.uint32(/* id 1, wireType 0 =*/8).int32(message.nCannonId);
            if (message.veEffectList != null && message.veEffectList.length)
                for (var i = 0; i < message.veEffectList.length; ++i)
                    $root.fish_battle.cannon__effect_info.encode(message.veEffectList[i], writer.uint32(/* id 2, wireType 2 =*/18).fork()).ldelim();
            return writer;
        };

        /**
         * Encodes the specified cannon__info message, length delimited. Does not implicitly {@link fish_battle.cannon__info.verify|verify} messages.
         * @function encodeDelimited
         * @memberof fish_battle.cannon__info
         * @static
         * @param {fish_battle.Icannon__info} message cannon__info message or plain object to encode
         * @param {$protobuf.Writer} [writer] Writer to encode to
         * @returns {$protobuf.Writer} Writer
         */
        cannon__info.encodeDelimited = function encodeDelimited(message, writer) {
            return this.encode(message, writer).ldelim();
        };

        /**
         * Decodes a cannon__info message from the specified reader or buffer.
         * @function decode
         * @memberof fish_battle.cannon__info
         * @static
         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
         * @param {number} [length] Message length if known beforehand
         * @returns {fish_battle.cannon__info} cannon__info
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        cannon__info.decode = function decode(reader, length) {
            if (!(reader instanceof $Reader))
                reader = $Reader.create(reader);
            var end = length === undefined ? reader.len : reader.pos + length, message = new $root.fish_battle.cannon__info();
            while (reader.pos < end) {
                var tag = reader.uint32();
                switch (tag >>> 3) {
                case 1:
                    message.nCannonId = reader.int32();
                    break;
                case 2:
                    if (!(message.veEffectList && message.veEffectList.length))
                        message.veEffectList = [];
                    message.veEffectList.push($root.fish_battle.cannon__effect_info.decode(reader, reader.uint32()));
                    break;
                default:
                    reader.skipType(tag & 7);
                    break;
                }
            }
            if (!message.hasOwnProperty("nCannonId"))
                throw $util.ProtocolError("missing required 'nCannonId'", { instance: message });
            return message;
        };

        /**
         * Decodes a cannon__info message from the specified reader or buffer, length delimited.
         * @function decodeDelimited
         * @memberof fish_battle.cannon__info
         * @static
         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
         * @returns {fish_battle.cannon__info} cannon__info
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        cannon__info.decodeDelimited = function decodeDelimited(reader) {
            if (!(reader instanceof $Reader))
                reader = new $Reader(reader);
            return this.decode(reader, reader.uint32());
        };

        /**
         * Verifies a cannon__info message.
         * @function verify
         * @memberof fish_battle.cannon__info
         * @static
         * @param {Object.<string,*>} message Plain object to verify
         * @returns {string|null} `null` if valid, otherwise the reason why it is not
         */
        cannon__info.verify = function verify(message) {
            if (typeof message !== "object" || message === null)
                return "object expected";
            if (!$util.isInteger(message.nCannonId))
                return "nCannonId: integer expected";
            if (message.veEffectList != null && message.hasOwnProperty("veEffectList")) {
                if (!Array.isArray(message.veEffectList))
                    return "veEffectList: array expected";
                for (var i = 0; i < message.veEffectList.length; ++i) {
                    var error = $root.fish_battle.cannon__effect_info.verify(message.veEffectList[i]);
                    if (error)
                        return "veEffectList." + error;
                }
            }
            return null;
        };

        /**
         * Creates a cannon__info message from a plain object. Also converts values to their respective internal types.
         * @function fromObject
         * @memberof fish_battle.cannon__info
         * @static
         * @param {Object.<string,*>} object Plain object
         * @returns {fish_battle.cannon__info} cannon__info
         */
        cannon__info.fromObject = function fromObject(object) {
            if (object instanceof $root.fish_battle.cannon__info)
                return object;
            var message = new $root.fish_battle.cannon__info();
            if (object.nCannonId != null)
                message.nCannonId = object.nCannonId | 0;
            if (object.veEffectList) {
                if (!Array.isArray(object.veEffectList))
                    throw TypeError(".fish_battle.cannon__info.veEffectList: array expected");
                message.veEffectList = [];
                for (var i = 0; i < object.veEffectList.length; ++i) {
                    if (typeof object.veEffectList[i] !== "object")
                        throw TypeError(".fish_battle.cannon__info.veEffectList: object expected");
                    message.veEffectList[i] = $root.fish_battle.cannon__effect_info.fromObject(object.veEffectList[i]);
                }
            }
            return message;
        };

        /**
         * Creates a plain object from a cannon__info message. Also converts values to other types if specified.
         * @function toObject
         * @memberof fish_battle.cannon__info
         * @static
         * @param {fish_battle.cannon__info} message cannon__info
         * @param {$protobuf.IConversionOptions} [options] Conversion options
         * @returns {Object.<string,*>} Plain object
         */
        cannon__info.toObject = function toObject(message, options) {
            if (!options)
                options = {};
            var object = {};
            if (options.arrays || options.defaults)
                object.veEffectList = [];
            if (options.defaults)
                object.nCannonId = 0;
            if (message.nCannonId != null && message.hasOwnProperty("nCannonId"))
                object.nCannonId = message.nCannonId;
            if (message.veEffectList && message.veEffectList.length) {
                object.veEffectList = [];
                for (var j = 0; j < message.veEffectList.length; ++j)
                    object.veEffectList[j] = $root.fish_battle.cannon__effect_info.toObject(message.veEffectList[j], options);
            }
            return object;
        };

        /**
         * Converts this cannon__info to JSON.
         * @function toJSON
         * @memberof fish_battle.cannon__info
         * @instance
         * @returns {Object.<string,*>} JSON object
         */
        cannon__info.prototype.toJSON = function toJSON() {
            return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
        };

        return cannon__info;
    })();

    fish_battle.user__info = (function() {

        /**
         * Properties of a user__info.
         * @memberof fish_battle
         * @interface Iuser__info
         * @property {string} strUserId user__info strUserId
         * @property {string} name user__info name
         * @property {string} headurl user__info headurl
         * @property {number} nTableNo user__info nTableNo
         * @property {number} nBet user__info nBet
         * @property {number} nMoney user__info nMoney
         * @property {number} nExp user__info nExp
         * @property {number} nVip user__info nVip
         * @property {fish_battle.Icannon__info} cannonInfo user__info cannonInfo
         */

        /**
         * Constructs a new user__info.
         * @memberof fish_battle
         * @classdesc Represents a user__info.
         * @implements Iuser__info
         * @constructor
         * @param {fish_battle.Iuser__info=} [properties] Properties to set
         */
        function user__info(properties) {
            if (properties)
                for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                    if (properties[keys[i]] != null)
                        this[keys[i]] = properties[keys[i]];
        }

        /**
         * user__info strUserId.
         * @member {string} strUserId
         * @memberof fish_battle.user__info
         * @instance
         */
        user__info.prototype.strUserId = "";

        /**
         * user__info name.
         * @member {string} name
         * @memberof fish_battle.user__info
         * @instance
         */
        user__info.prototype.name = "";

        /**
         * user__info headurl.
         * @member {string} headurl
         * @memberof fish_battle.user__info
         * @instance
         */
        user__info.prototype.headurl = "";

        /**
         * user__info nTableNo.
         * @member {number} nTableNo
         * @memberof fish_battle.user__info
         * @instance
         */
        user__info.prototype.nTableNo = 0;

        /**
         * user__info nBet.
         * @member {number} nBet
         * @memberof fish_battle.user__info
         * @instance
         */
        user__info.prototype.nBet = 0;

        /**
         * user__info nMoney.
         * @member {number} nMoney
         * @memberof fish_battle.user__info
         * @instance
         */
        user__info.prototype.nMoney = 0;

        /**
         * user__info nExp.
         * @member {number} nExp
         * @memberof fish_battle.user__info
         * @instance
         */
        user__info.prototype.nExp = 0;

        /**
         * user__info nVip.
         * @member {number} nVip
         * @memberof fish_battle.user__info
         * @instance
         */
        user__info.prototype.nVip = 0;

        /**
         * user__info cannonInfo.
         * @member {fish_battle.Icannon__info} cannonInfo
         * @memberof fish_battle.user__info
         * @instance
         */
        user__info.prototype.cannonInfo = null;

        /**
         * Creates a new user__info instance using the specified properties.
         * @function create
         * @memberof fish_battle.user__info
         * @static
         * @param {fish_battle.Iuser__info=} [properties] Properties to set
         * @returns {fish_battle.user__info} user__info instance
         */
        user__info.create = function create(properties) {
            return new user__info(properties);
        };

        /**
         * Encodes the specified user__info message. Does not implicitly {@link fish_battle.user__info.verify|verify} messages.
         * @function encode
         * @memberof fish_battle.user__info
         * @static
         * @param {fish_battle.Iuser__info} message user__info message or plain object to encode
         * @param {$protobuf.Writer} [writer] Writer to encode to
         * @returns {$protobuf.Writer} Writer
         */
        user__info.encode = function encode(message, writer) {
            if (!writer)
                writer = $Writer.create();
            writer.uint32(/* id 1, wireType 2 =*/10).string(message.strUserId);
            writer.uint32(/* id 2, wireType 2 =*/18).string(message.name);
            writer.uint32(/* id 3, wireType 2 =*/26).string(message.headurl);
            writer.uint32(/* id 4, wireType 0 =*/32).int32(message.nTableNo);
            writer.uint32(/* id 5, wireType 0 =*/40).int32(message.nBet);
            writer.uint32(/* id 6, wireType 0 =*/48).int32(message.nMoney);
            writer.uint32(/* id 7, wireType 0 =*/56).int32(message.nExp);
            writer.uint32(/* id 8, wireType 0 =*/64).int32(message.nVip);
            $root.fish_battle.cannon__info.encode(message.cannonInfo, writer.uint32(/* id 9, wireType 2 =*/74).fork()).ldelim();
            return writer;
        };

        /**
         * Encodes the specified user__info message, length delimited. Does not implicitly {@link fish_battle.user__info.verify|verify} messages.
         * @function encodeDelimited
         * @memberof fish_battle.user__info
         * @static
         * @param {fish_battle.Iuser__info} message user__info message or plain object to encode
         * @param {$protobuf.Writer} [writer] Writer to encode to
         * @returns {$protobuf.Writer} Writer
         */
        user__info.encodeDelimited = function encodeDelimited(message, writer) {
            return this.encode(message, writer).ldelim();
        };

        /**
         * Decodes a user__info message from the specified reader or buffer.
         * @function decode
         * @memberof fish_battle.user__info
         * @static
         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
         * @param {number} [length] Message length if known beforehand
         * @returns {fish_battle.user__info} user__info
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        user__info.decode = function decode(reader, length) {
            if (!(reader instanceof $Reader))
                reader = $Reader.create(reader);
            var end = length === undefined ? reader.len : reader.pos + length, message = new $root.fish_battle.user__info();
            while (reader.pos < end) {
                var tag = reader.uint32();
                switch (tag >>> 3) {
                case 1:
                    message.strUserId = reader.string();
                    break;
                case 2:
                    message.name = reader.string();
                    break;
                case 3:
                    message.headurl = reader.string();
                    break;
                case 4:
                    message.nTableNo = reader.int32();
                    break;
                case 5:
                    message.nBet = reader.int32();
                    break;
                case 6:
                    message.nMoney = reader.int32();
                    break;
                case 7:
                    message.nExp = reader.int32();
                    break;
                case 8:
                    message.nVip = reader.int32();
                    break;
                case 9:
                    message.cannonInfo = $root.fish_battle.cannon__info.decode(reader, reader.uint32());
                    break;
                default:
                    reader.skipType(tag & 7);
                    break;
                }
            }
            if (!message.hasOwnProperty("strUserId"))
                throw $util.ProtocolError("missing required 'strUserId'", { instance: message });
            if (!message.hasOwnProperty("name"))
                throw $util.ProtocolError("missing required 'name'", { instance: message });
            if (!message.hasOwnProperty("headurl"))
                throw $util.ProtocolError("missing required 'headurl'", { instance: message });
            if (!message.hasOwnProperty("nTableNo"))
                throw $util.ProtocolError("missing required 'nTableNo'", { instance: message });
            if (!message.hasOwnProperty("nBet"))
                throw $util.ProtocolError("missing required 'nBet'", { instance: message });
            if (!message.hasOwnProperty("nMoney"))
                throw $util.ProtocolError("missing required 'nMoney'", { instance: message });
            if (!message.hasOwnProperty("nExp"))
                throw $util.ProtocolError("missing required 'nExp'", { instance: message });
            if (!message.hasOwnProperty("nVip"))
                throw $util.ProtocolError("missing required 'nVip'", { instance: message });
            if (!message.hasOwnProperty("cannonInfo"))
                throw $util.ProtocolError("missing required 'cannonInfo'", { instance: message });
            return message;
        };

        /**
         * Decodes a user__info message from the specified reader or buffer, length delimited.
         * @function decodeDelimited
         * @memberof fish_battle.user__info
         * @static
         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
         * @returns {fish_battle.user__info} user__info
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        user__info.decodeDelimited = function decodeDelimited(reader) {
            if (!(reader instanceof $Reader))
                reader = new $Reader(reader);
            return this.decode(reader, reader.uint32());
        };

        /**
         * Verifies a user__info message.
         * @function verify
         * @memberof fish_battle.user__info
         * @static
         * @param {Object.<string,*>} message Plain object to verify
         * @returns {string|null} `null` if valid, otherwise the reason why it is not
         */
        user__info.verify = function verify(message) {
            if (typeof message !== "object" || message === null)
                return "object expected";
            if (!$util.isString(message.strUserId))
                return "strUserId: string expected";
            if (!$util.isString(message.name))
                return "name: string expected";
            if (!$util.isString(message.headurl))
                return "headurl: string expected";
            if (!$util.isInteger(message.nTableNo))
                return "nTableNo: integer expected";
            if (!$util.isInteger(message.nBet))
                return "nBet: integer expected";
            if (!$util.isInteger(message.nMoney))
                return "nMoney: integer expected";
            if (!$util.isInteger(message.nExp))
                return "nExp: integer expected";
            if (!$util.isInteger(message.nVip))
                return "nVip: integer expected";
            {
                var error = $root.fish_battle.cannon__info.verify(message.cannonInfo);
                if (error)
                    return "cannonInfo." + error;
            }
            return null;
        };

        /**
         * Creates a user__info message from a plain object. Also converts values to their respective internal types.
         * @function fromObject
         * @memberof fish_battle.user__info
         * @static
         * @param {Object.<string,*>} object Plain object
         * @returns {fish_battle.user__info} user__info
         */
        user__info.fromObject = function fromObject(object) {
            if (object instanceof $root.fish_battle.user__info)
                return object;
            var message = new $root.fish_battle.user__info();
            if (object.strUserId != null)
                message.strUserId = String(object.strUserId);
            if (object.name != null)
                message.name = String(object.name);
            if (object.headurl != null)
                message.headurl = String(object.headurl);
            if (object.nTableNo != null)
                message.nTableNo = object.nTableNo | 0;
            if (object.nBet != null)
                message.nBet = object.nBet | 0;
            if (object.nMoney != null)
                message.nMoney = object.nMoney | 0;
            if (object.nExp != null)
                message.nExp = object.nExp | 0;
            if (object.nVip != null)
                message.nVip = object.nVip | 0;
            if (object.cannonInfo != null) {
                if (typeof object.cannonInfo !== "object")
                    throw TypeError(".fish_battle.user__info.cannonInfo: object expected");
                message.cannonInfo = $root.fish_battle.cannon__info.fromObject(object.cannonInfo);
            }
            return message;
        };

        /**
         * Creates a plain object from a user__info message. Also converts values to other types if specified.
         * @function toObject
         * @memberof fish_battle.user__info
         * @static
         * @param {fish_battle.user__info} message user__info
         * @param {$protobuf.IConversionOptions} [options] Conversion options
         * @returns {Object.<string,*>} Plain object
         */
        user__info.toObject = function toObject(message, options) {
            if (!options)
                options = {};
            var object = {};
            if (options.defaults) {
                object.strUserId = "";
                object.name = "";
                object.headurl = "";
                object.nTableNo = 0;
                object.nBet = 0;
                object.nMoney = 0;
                object.nExp = 0;
                object.nVip = 0;
                object.cannonInfo = null;
            }
            if (message.strUserId != null && message.hasOwnProperty("strUserId"))
                object.strUserId = message.strUserId;
            if (message.name != null && message.hasOwnProperty("name"))
                object.name = message.name;
            if (message.headurl != null && message.hasOwnProperty("headurl"))
                object.headurl = message.headurl;
            if (message.nTableNo != null && message.hasOwnProperty("nTableNo"))
                object.nTableNo = message.nTableNo;
            if (message.nBet != null && message.hasOwnProperty("nBet"))
                object.nBet = message.nBet;
            if (message.nMoney != null && message.hasOwnProperty("nMoney"))
                object.nMoney = message.nMoney;
            if (message.nExp != null && message.hasOwnProperty("nExp"))
                object.nExp = message.nExp;
            if (message.nVip != null && message.hasOwnProperty("nVip"))
                object.nVip = message.nVip;
            if (message.cannonInfo != null && message.hasOwnProperty("cannonInfo"))
                object.cannonInfo = $root.fish_battle.cannon__info.toObject(message.cannonInfo, options);
            return object;
        };

        /**
         * Converts this user__info to JSON.
         * @function toJSON
         * @memberof fish_battle.user__info
         * @instance
         * @returns {Object.<string,*>} JSON object
         */
        user__info.prototype.toJSON = function toJSON() {
            return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
        };

        return user__info;
    })();

    fish_battle.fish__route = (function() {

        /**
         * Properties of a fish__route.
         * @memberof fish_battle
         * @interface Ifish__route
         * @property {number} nRouteType fish__route nRouteType
         * @property {number} nRouteTime fish__route nRouteTime
         * @property {number|null} [nFishRouteId] fish__route nFishRouteId
         * @property {string|null} [strRouteVecs] fish__route strRouteVecs
         */

        /**
         * Constructs a new fish__route.
         * @memberof fish_battle
         * @classdesc Represents a fish__route.
         * @implements Ifish__route
         * @constructor
         * @param {fish_battle.Ifish__route=} [properties] Properties to set
         */
        function fish__route(properties) {
            if (properties)
                for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                    if (properties[keys[i]] != null)
                        this[keys[i]] = properties[keys[i]];
        }

        /**
         * fish__route nRouteType.
         * @member {number} nRouteType
         * @memberof fish_battle.fish__route
         * @instance
         */
        fish__route.prototype.nRouteType = 0;

        /**
         * fish__route nRouteTime.
         * @member {number} nRouteTime
         * @memberof fish_battle.fish__route
         * @instance
         */
        fish__route.prototype.nRouteTime = 0;

        /**
         * fish__route nFishRouteId.
         * @member {number} nFishRouteId
         * @memberof fish_battle.fish__route
         * @instance
         */
        fish__route.prototype.nFishRouteId = 0;

        /**
         * fish__route strRouteVecs.
         * @member {string} strRouteVecs
         * @memberof fish_battle.fish__route
         * @instance
         */
        fish__route.prototype.strRouteVecs = "";

        /**
         * Creates a new fish__route instance using the specified properties.
         * @function create
         * @memberof fish_battle.fish__route
         * @static
         * @param {fish_battle.Ifish__route=} [properties] Properties to set
         * @returns {fish_battle.fish__route} fish__route instance
         */
        fish__route.create = function create(properties) {
            return new fish__route(properties);
        };

        /**
         * Encodes the specified fish__route message. Does not implicitly {@link fish_battle.fish__route.verify|verify} messages.
         * @function encode
         * @memberof fish_battle.fish__route
         * @static
         * @param {fish_battle.Ifish__route} message fish__route message or plain object to encode
         * @param {$protobuf.Writer} [writer] Writer to encode to
         * @returns {$protobuf.Writer} Writer
         */
        fish__route.encode = function encode(message, writer) {
            if (!writer)
                writer = $Writer.create();
            writer.uint32(/* id 1, wireType 0 =*/8).int32(message.nRouteType);
            writer.uint32(/* id 2, wireType 0 =*/16).int32(message.nRouteTime);
            if (message.nFishRouteId != null && message.hasOwnProperty("nFishRouteId"))
                writer.uint32(/* id 3, wireType 0 =*/24).int32(message.nFishRouteId);
            if (message.strRouteVecs != null && message.hasOwnProperty("strRouteVecs"))
                writer.uint32(/* id 4, wireType 2 =*/34).string(message.strRouteVecs);
            return writer;
        };

        /**
         * Encodes the specified fish__route message, length delimited. Does not implicitly {@link fish_battle.fish__route.verify|verify} messages.
         * @function encodeDelimited
         * @memberof fish_battle.fish__route
         * @static
         * @param {fish_battle.Ifish__route} message fish__route message or plain object to encode
         * @param {$protobuf.Writer} [writer] Writer to encode to
         * @returns {$protobuf.Writer} Writer
         */
        fish__route.encodeDelimited = function encodeDelimited(message, writer) {
            return this.encode(message, writer).ldelim();
        };

        /**
         * Decodes a fish__route message from the specified reader or buffer.
         * @function decode
         * @memberof fish_battle.fish__route
         * @static
         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
         * @param {number} [length] Message length if known beforehand
         * @returns {fish_battle.fish__route} fish__route
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        fish__route.decode = function decode(reader, length) {
            if (!(reader instanceof $Reader))
                reader = $Reader.create(reader);
            var end = length === undefined ? reader.len : reader.pos + length, message = new $root.fish_battle.fish__route();
            while (reader.pos < end) {
                var tag = reader.uint32();
                switch (tag >>> 3) {
                case 1:
                    message.nRouteType = reader.int32();
                    break;
                case 2:
                    message.nRouteTime = reader.int32();
                    break;
                case 3:
                    message.nFishRouteId = reader.int32();
                    break;
                case 4:
                    message.strRouteVecs = reader.string();
                    break;
                default:
                    reader.skipType(tag & 7);
                    break;
                }
            }
            if (!message.hasOwnProperty("nRouteType"))
                throw $util.ProtocolError("missing required 'nRouteType'", { instance: message });
            if (!message.hasOwnProperty("nRouteTime"))
                throw $util.ProtocolError("missing required 'nRouteTime'", { instance: message });
            return message;
        };

        /**
         * Decodes a fish__route message from the specified reader or buffer, length delimited.
         * @function decodeDelimited
         * @memberof fish_battle.fish__route
         * @static
         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
         * @returns {fish_battle.fish__route} fish__route
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        fish__route.decodeDelimited = function decodeDelimited(reader) {
            if (!(reader instanceof $Reader))
                reader = new $Reader(reader);
            return this.decode(reader, reader.uint32());
        };

        /**
         * Verifies a fish__route message.
         * @function verify
         * @memberof fish_battle.fish__route
         * @static
         * @param {Object.<string,*>} message Plain object to verify
         * @returns {string|null} `null` if valid, otherwise the reason why it is not
         */
        fish__route.verify = function verify(message) {
            if (typeof message !== "object" || message === null)
                return "object expected";
            if (!$util.isInteger(message.nRouteType))
                return "nRouteType: integer expected";
            if (!$util.isInteger(message.nRouteTime))
                return "nRouteTime: integer expected";
            if (message.nFishRouteId != null && message.hasOwnProperty("nFishRouteId"))
                if (!$util.isInteger(message.nFishRouteId))
                    return "nFishRouteId: integer expected";
            if (message.strRouteVecs != null && message.hasOwnProperty("strRouteVecs"))
                if (!$util.isString(message.strRouteVecs))
                    return "strRouteVecs: string expected";
            return null;
        };

        /**
         * Creates a fish__route message from a plain object. Also converts values to their respective internal types.
         * @function fromObject
         * @memberof fish_battle.fish__route
         * @static
         * @param {Object.<string,*>} object Plain object
         * @returns {fish_battle.fish__route} fish__route
         */
        fish__route.fromObject = function fromObject(object) {
            if (object instanceof $root.fish_battle.fish__route)
                return object;
            var message = new $root.fish_battle.fish__route();
            if (object.nRouteType != null)
                message.nRouteType = object.nRouteType | 0;
            if (object.nRouteTime != null)
                message.nRouteTime = object.nRouteTime | 0;
            if (object.nFishRouteId != null)
                message.nFishRouteId = object.nFishRouteId | 0;
            if (object.strRouteVecs != null)
                message.strRouteVecs = String(object.strRouteVecs);
            return message;
        };

        /**
         * Creates a plain object from a fish__route message. Also converts values to other types if specified.
         * @function toObject
         * @memberof fish_battle.fish__route
         * @static
         * @param {fish_battle.fish__route} message fish__route
         * @param {$protobuf.IConversionOptions} [options] Conversion options
         * @returns {Object.<string,*>} Plain object
         */
        fish__route.toObject = function toObject(message, options) {
            if (!options)
                options = {};
            var object = {};
            if (options.defaults) {
                object.nRouteType = 0;
                object.nRouteTime = 0;
                object.nFishRouteId = 0;
                object.strRouteVecs = "";
            }
            if (message.nRouteType != null && message.hasOwnProperty("nRouteType"))
                object.nRouteType = message.nRouteType;
            if (message.nRouteTime != null && message.hasOwnProperty("nRouteTime"))
                object.nRouteTime = message.nRouteTime;
            if (message.nFishRouteId != null && message.hasOwnProperty("nFishRouteId"))
                object.nFishRouteId = message.nFishRouteId;
            if (message.strRouteVecs != null && message.hasOwnProperty("strRouteVecs"))
                object.strRouteVecs = message.strRouteVecs;
            return object;
        };

        /**
         * Converts this fish__route to JSON.
         * @function toJSON
         * @memberof fish_battle.fish__route
         * @instance
         * @returns {Object.<string,*>} JSON object
         */
        fish__route.prototype.toJSON = function toJSON() {
            return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
        };

        return fish__route;
    })();

    fish_battle.fish__buff = (function() {

        /**
         * Properties of a fish__buff.
         * @memberof fish_battle
         * @interface Ifish__buff
         * @property {number} nBuffId fish__buff nBuffId
         * @property {number} nBuffValue fish__buff nBuffValue
         * @property {number} nBuffRemainTime fish__buff nBuffRemainTime
         */

        /**
         * Constructs a new fish__buff.
         * @memberof fish_battle
         * @classdesc Represents a fish__buff.
         * @implements Ifish__buff
         * @constructor
         * @param {fish_battle.Ifish__buff=} [properties] Properties to set
         */
        function fish__buff(properties) {
            if (properties)
                for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                    if (properties[keys[i]] != null)
                        this[keys[i]] = properties[keys[i]];
        }

        /**
         * fish__buff nBuffId.
         * @member {number} nBuffId
         * @memberof fish_battle.fish__buff
         * @instance
         */
        fish__buff.prototype.nBuffId = 0;

        /**
         * fish__buff nBuffValue.
         * @member {number} nBuffValue
         * @memberof fish_battle.fish__buff
         * @instance
         */
        fish__buff.prototype.nBuffValue = 0;

        /**
         * fish__buff nBuffRemainTime.
         * @member {number} nBuffRemainTime
         * @memberof fish_battle.fish__buff
         * @instance
         */
        fish__buff.prototype.nBuffRemainTime = 0;

        /**
         * Creates a new fish__buff instance using the specified properties.
         * @function create
         * @memberof fish_battle.fish__buff
         * @static
         * @param {fish_battle.Ifish__buff=} [properties] Properties to set
         * @returns {fish_battle.fish__buff} fish__buff instance
         */
        fish__buff.create = function create(properties) {
            return new fish__buff(properties);
        };

        /**
         * Encodes the specified fish__buff message. Does not implicitly {@link fish_battle.fish__buff.verify|verify} messages.
         * @function encode
         * @memberof fish_battle.fish__buff
         * @static
         * @param {fish_battle.Ifish__buff} message fish__buff message or plain object to encode
         * @param {$protobuf.Writer} [writer] Writer to encode to
         * @returns {$protobuf.Writer} Writer
         */
        fish__buff.encode = function encode(message, writer) {
            if (!writer)
                writer = $Writer.create();
            writer.uint32(/* id 1, wireType 0 =*/8).int32(message.nBuffId);
            writer.uint32(/* id 2, wireType 0 =*/16).int32(message.nBuffValue);
            writer.uint32(/* id 3, wireType 0 =*/24).int32(message.nBuffRemainTime);
            return writer;
        };

        /**
         * Encodes the specified fish__buff message, length delimited. Does not implicitly {@link fish_battle.fish__buff.verify|verify} messages.
         * @function encodeDelimited
         * @memberof fish_battle.fish__buff
         * @static
         * @param {fish_battle.Ifish__buff} message fish__buff message or plain object to encode
         * @param {$protobuf.Writer} [writer] Writer to encode to
         * @returns {$protobuf.Writer} Writer
         */
        fish__buff.encodeDelimited = function encodeDelimited(message, writer) {
            return this.encode(message, writer).ldelim();
        };

        /**
         * Decodes a fish__buff message from the specified reader or buffer.
         * @function decode
         * @memberof fish_battle.fish__buff
         * @static
         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
         * @param {number} [length] Message length if known beforehand
         * @returns {fish_battle.fish__buff} fish__buff
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        fish__buff.decode = function decode(reader, length) {
            if (!(reader instanceof $Reader))
                reader = $Reader.create(reader);
            var end = length === undefined ? reader.len : reader.pos + length, message = new $root.fish_battle.fish__buff();
            while (reader.pos < end) {
                var tag = reader.uint32();
                switch (tag >>> 3) {
                case 1:
                    message.nBuffId = reader.int32();
                    break;
                case 2:
                    message.nBuffValue = reader.int32();
                    break;
                case 3:
                    message.nBuffRemainTime = reader.int32();
                    break;
                default:
                    reader.skipType(tag & 7);
                    break;
                }
            }
            if (!message.hasOwnProperty("nBuffId"))
                throw $util.ProtocolError("missing required 'nBuffId'", { instance: message });
            if (!message.hasOwnProperty("nBuffValue"))
                throw $util.ProtocolError("missing required 'nBuffValue'", { instance: message });
            if (!message.hasOwnProperty("nBuffRemainTime"))
                throw $util.ProtocolError("missing required 'nBuffRemainTime'", { instance: message });
            return message;
        };

        /**
         * Decodes a fish__buff message from the specified reader or buffer, length delimited.
         * @function decodeDelimited
         * @memberof fish_battle.fish__buff
         * @static
         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
         * @returns {fish_battle.fish__buff} fish__buff
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        fish__buff.decodeDelimited = function decodeDelimited(reader) {
            if (!(reader instanceof $Reader))
                reader = new $Reader(reader);
            return this.decode(reader, reader.uint32());
        };

        /**
         * Verifies a fish__buff message.
         * @function verify
         * @memberof fish_battle.fish__buff
         * @static
         * @param {Object.<string,*>} message Plain object to verify
         * @returns {string|null} `null` if valid, otherwise the reason why it is not
         */
        fish__buff.verify = function verify(message) {
            if (typeof message !== "object" || message === null)
                return "object expected";
            if (!$util.isInteger(message.nBuffId))
                return "nBuffId: integer expected";
            if (!$util.isInteger(message.nBuffValue))
                return "nBuffValue: integer expected";
            if (!$util.isInteger(message.nBuffRemainTime))
                return "nBuffRemainTime: integer expected";
            return null;
        };

        /**
         * Creates a fish__buff message from a plain object. Also converts values to their respective internal types.
         * @function fromObject
         * @memberof fish_battle.fish__buff
         * @static
         * @param {Object.<string,*>} object Plain object
         * @returns {fish_battle.fish__buff} fish__buff
         */
        fish__buff.fromObject = function fromObject(object) {
            if (object instanceof $root.fish_battle.fish__buff)
                return object;
            var message = new $root.fish_battle.fish__buff();
            if (object.nBuffId != null)
                message.nBuffId = object.nBuffId | 0;
            if (object.nBuffValue != null)
                message.nBuffValue = object.nBuffValue | 0;
            if (object.nBuffRemainTime != null)
                message.nBuffRemainTime = object.nBuffRemainTime | 0;
            return message;
        };

        /**
         * Creates a plain object from a fish__buff message. Also converts values to other types if specified.
         * @function toObject
         * @memberof fish_battle.fish__buff
         * @static
         * @param {fish_battle.fish__buff} message fish__buff
         * @param {$protobuf.IConversionOptions} [options] Conversion options
         * @returns {Object.<string,*>} Plain object
         */
        fish__buff.toObject = function toObject(message, options) {
            if (!options)
                options = {};
            var object = {};
            if (options.defaults) {
                object.nBuffId = 0;
                object.nBuffValue = 0;
                object.nBuffRemainTime = 0;
            }
            if (message.nBuffId != null && message.hasOwnProperty("nBuffId"))
                object.nBuffId = message.nBuffId;
            if (message.nBuffValue != null && message.hasOwnProperty("nBuffValue"))
                object.nBuffValue = message.nBuffValue;
            if (message.nBuffRemainTime != null && message.hasOwnProperty("nBuffRemainTime"))
                object.nBuffRemainTime = message.nBuffRemainTime;
            return object;
        };

        /**
         * Converts this fish__buff to JSON.
         * @function toJSON
         * @memberof fish_battle.fish__buff
         * @instance
         * @returns {Object.<string,*>} JSON object
         */
        fish__buff.prototype.toJSON = function toJSON() {
            return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
        };

        return fish__buff;
    })();

    fish_battle.fish__info = (function() {

        /**
         * Properties of a fish__info.
         * @memberof fish_battle
         * @interface Ifish__info
         * @property {number} nFishId fish__info nFishId
         * @property {number} nFishIndex fish__info nFishIndex
         * @property {Array.<fish_battle.Ifish__route>|null} [fishRoutes] fish__info fishRoutes
         * @property {Array.<fish_battle.Ifish__buff>|null} [fishBuffs] fish__info fishBuffs
         */

        /**
         * Constructs a new fish__info.
         * @memberof fish_battle
         * @classdesc Represents a fish__info.
         * @implements Ifish__info
         * @constructor
         * @param {fish_battle.Ifish__info=} [properties] Properties to set
         */
        function fish__info(properties) {
            this.fishRoutes = [];
            this.fishBuffs = [];
            if (properties)
                for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                    if (properties[keys[i]] != null)
                        this[keys[i]] = properties[keys[i]];
        }

        /**
         * fish__info nFishId.
         * @member {number} nFishId
         * @memberof fish_battle.fish__info
         * @instance
         */
        fish__info.prototype.nFishId = 0;

        /**
         * fish__info nFishIndex.
         * @member {number} nFishIndex
         * @memberof fish_battle.fish__info
         * @instance
         */
        fish__info.prototype.nFishIndex = 0;

        /**
         * fish__info fishRoutes.
         * @member {Array.<fish_battle.Ifish__route>} fishRoutes
         * @memberof fish_battle.fish__info
         * @instance
         */
        fish__info.prototype.fishRoutes = $util.emptyArray;

        /**
         * fish__info fishBuffs.
         * @member {Array.<fish_battle.Ifish__buff>} fishBuffs
         * @memberof fish_battle.fish__info
         * @instance
         */
        fish__info.prototype.fishBuffs = $util.emptyArray;

        /**
         * Creates a new fish__info instance using the specified properties.
         * @function create
         * @memberof fish_battle.fish__info
         * @static
         * @param {fish_battle.Ifish__info=} [properties] Properties to set
         * @returns {fish_battle.fish__info} fish__info instance
         */
        fish__info.create = function create(properties) {
            return new fish__info(properties);
        };

        /**
         * Encodes the specified fish__info message. Does not implicitly {@link fish_battle.fish__info.verify|verify} messages.
         * @function encode
         * @memberof fish_battle.fish__info
         * @static
         * @param {fish_battle.Ifish__info} message fish__info message or plain object to encode
         * @param {$protobuf.Writer} [writer] Writer to encode to
         * @returns {$protobuf.Writer} Writer
         */
        fish__info.encode = function encode(message, writer) {
            if (!writer)
                writer = $Writer.create();
            writer.uint32(/* id 1, wireType 0 =*/8).int32(message.nFishId);
            writer.uint32(/* id 2, wireType 0 =*/16).int32(message.nFishIndex);
            if (message.fishRoutes != null && message.fishRoutes.length)
                for (var i = 0; i < message.fishRoutes.length; ++i)
                    $root.fish_battle.fish__route.encode(message.fishRoutes[i], writer.uint32(/* id 4, wireType 2 =*/34).fork()).ldelim();
            if (message.fishBuffs != null && message.fishBuffs.length)
                for (var i = 0; i < message.fishBuffs.length; ++i)
                    $root.fish_battle.fish__buff.encode(message.fishBuffs[i], writer.uint32(/* id 5, wireType 2 =*/42).fork()).ldelim();
            return writer;
        };

        /**
         * Encodes the specified fish__info message, length delimited. Does not implicitly {@link fish_battle.fish__info.verify|verify} messages.
         * @function encodeDelimited
         * @memberof fish_battle.fish__info
         * @static
         * @param {fish_battle.Ifish__info} message fish__info message or plain object to encode
         * @param {$protobuf.Writer} [writer] Writer to encode to
         * @returns {$protobuf.Writer} Writer
         */
        fish__info.encodeDelimited = function encodeDelimited(message, writer) {
            return this.encode(message, writer).ldelim();
        };

        /**
         * Decodes a fish__info message from the specified reader or buffer.
         * @function decode
         * @memberof fish_battle.fish__info
         * @static
         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
         * @param {number} [length] Message length if known beforehand
         * @returns {fish_battle.fish__info} fish__info
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        fish__info.decode = function decode(reader, length) {
            if (!(reader instanceof $Reader))
                reader = $Reader.create(reader);
            var end = length === undefined ? reader.len : reader.pos + length, message = new $root.fish_battle.fish__info();
            while (reader.pos < end) {
                var tag = reader.uint32();
                switch (tag >>> 3) {
                case 1:
                    message.nFishId = reader.int32();
                    break;
                case 2:
                    message.nFishIndex = reader.int32();
                    break;
                case 4:
                    if (!(message.fishRoutes && message.fishRoutes.length))
                        message.fishRoutes = [];
                    message.fishRoutes.push($root.fish_battle.fish__route.decode(reader, reader.uint32()));
                    break;
                case 5:
                    if (!(message.fishBuffs && message.fishBuffs.length))
                        message.fishBuffs = [];
                    message.fishBuffs.push($root.fish_battle.fish__buff.decode(reader, reader.uint32()));
                    break;
                default:
                    reader.skipType(tag & 7);
                    break;
                }
            }
            if (!message.hasOwnProperty("nFishId"))
                throw $util.ProtocolError("missing required 'nFishId'", { instance: message });
            if (!message.hasOwnProperty("nFishIndex"))
                throw $util.ProtocolError("missing required 'nFishIndex'", { instance: message });
            return message;
        };

        /**
         * Decodes a fish__info message from the specified reader or buffer, length delimited.
         * @function decodeDelimited
         * @memberof fish_battle.fish__info
         * @static
         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
         * @returns {fish_battle.fish__info} fish__info
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        fish__info.decodeDelimited = function decodeDelimited(reader) {
            if (!(reader instanceof $Reader))
                reader = new $Reader(reader);
            return this.decode(reader, reader.uint32());
        };

        /**
         * Verifies a fish__info message.
         * @function verify
         * @memberof fish_battle.fish__info
         * @static
         * @param {Object.<string,*>} message Plain object to verify
         * @returns {string|null} `null` if valid, otherwise the reason why it is not
         */
        fish__info.verify = function verify(message) {
            if (typeof message !== "object" || message === null)
                return "object expected";
            if (!$util.isInteger(message.nFishId))
                return "nFishId: integer expected";
            if (!$util.isInteger(message.nFishIndex))
                return "nFishIndex: integer expected";
            if (message.fishRoutes != null && message.hasOwnProperty("fishRoutes")) {
                if (!Array.isArray(message.fishRoutes))
                    return "fishRoutes: array expected";
                for (var i = 0; i < message.fishRoutes.length; ++i) {
                    var error = $root.fish_battle.fish__route.verify(message.fishRoutes[i]);
                    if (error)
                        return "fishRoutes." + error;
                }
            }
            if (message.fishBuffs != null && message.hasOwnProperty("fishBuffs")) {
                if (!Array.isArray(message.fishBuffs))
                    return "fishBuffs: array expected";
                for (var i = 0; i < message.fishBuffs.length; ++i) {
                    var error = $root.fish_battle.fish__buff.verify(message.fishBuffs[i]);
                    if (error)
                        return "fishBuffs." + error;
                }
            }
            return null;
        };

        /**
         * Creates a fish__info message from a plain object. Also converts values to their respective internal types.
         * @function fromObject
         * @memberof fish_battle.fish__info
         * @static
         * @param {Object.<string,*>} object Plain object
         * @returns {fish_battle.fish__info} fish__info
         */
        fish__info.fromObject = function fromObject(object) {
            if (object instanceof $root.fish_battle.fish__info)
                return object;
            var message = new $root.fish_battle.fish__info();
            if (object.nFishId != null)
                message.nFishId = object.nFishId | 0;
            if (object.nFishIndex != null)
                message.nFishIndex = object.nFishIndex | 0;
            if (object.fishRoutes) {
                if (!Array.isArray(object.fishRoutes))
                    throw TypeError(".fish_battle.fish__info.fishRoutes: array expected");
                message.fishRoutes = [];
                for (var i = 0; i < object.fishRoutes.length; ++i) {
                    if (typeof object.fishRoutes[i] !== "object")
                        throw TypeError(".fish_battle.fish__info.fishRoutes: object expected");
                    message.fishRoutes[i] = $root.fish_battle.fish__route.fromObject(object.fishRoutes[i]);
                }
            }
            if (object.fishBuffs) {
                if (!Array.isArray(object.fishBuffs))
                    throw TypeError(".fish_battle.fish__info.fishBuffs: array expected");
                message.fishBuffs = [];
                for (var i = 0; i < object.fishBuffs.length; ++i) {
                    if (typeof object.fishBuffs[i] !== "object")
                        throw TypeError(".fish_battle.fish__info.fishBuffs: object expected");
                    message.fishBuffs[i] = $root.fish_battle.fish__buff.fromObject(object.fishBuffs[i]);
                }
            }
            return message;
        };

        /**
         * Creates a plain object from a fish__info message. Also converts values to other types if specified.
         * @function toObject
         * @memberof fish_battle.fish__info
         * @static
         * @param {fish_battle.fish__info} message fish__info
         * @param {$protobuf.IConversionOptions} [options] Conversion options
         * @returns {Object.<string,*>} Plain object
         */
        fish__info.toObject = function toObject(message, options) {
            if (!options)
                options = {};
            var object = {};
            if (options.arrays || options.defaults) {
                object.fishRoutes = [];
                object.fishBuffs = [];
            }
            if (options.defaults) {
                object.nFishId = 0;
                object.nFishIndex = 0;
            }
            if (message.nFishId != null && message.hasOwnProperty("nFishId"))
                object.nFishId = message.nFishId;
            if (message.nFishIndex != null && message.hasOwnProperty("nFishIndex"))
                object.nFishIndex = message.nFishIndex;
            if (message.fishRoutes && message.fishRoutes.length) {
                object.fishRoutes = [];
                for (var j = 0; j < message.fishRoutes.length; ++j)
                    object.fishRoutes[j] = $root.fish_battle.fish__route.toObject(message.fishRoutes[j], options);
            }
            if (message.fishBuffs && message.fishBuffs.length) {
                object.fishBuffs = [];
                for (var j = 0; j < message.fishBuffs.length; ++j)
                    object.fishBuffs[j] = $root.fish_battle.fish__buff.toObject(message.fishBuffs[j], options);
            }
            return object;
        };

        /**
         * Converts this fish__info to JSON.
         * @function toJSON
         * @memberof fish_battle.fish__info
         * @instance
         * @returns {Object.<string,*>} JSON object
         */
        fish__info.prototype.toJSON = function toJSON() {
            return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
        };

        return fish__info;
    })();

    fish_battle.fish_notify = (function() {

        /**
         * Properties of a fish_notify.
         * @memberof fish_battle
         * @interface Ifish_notify
         * @property {Array.<fish_battle.Ifish__info>|null} [veFishList] fish_notify veFishList
         */

        /**
         * Constructs a new fish_notify.
         * @memberof fish_battle
         * @classdesc Represents a fish_notify.
         * @implements Ifish_notify
         * @constructor
         * @param {fish_battle.Ifish_notify=} [properties] Properties to set
         */
        function fish_notify(properties) {
            this.veFishList = [];
            if (properties)
                for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                    if (properties[keys[i]] != null)
                        this[keys[i]] = properties[keys[i]];
        }

        /**
         * fish_notify veFishList.
         * @member {Array.<fish_battle.Ifish__info>} veFishList
         * @memberof fish_battle.fish_notify
         * @instance
         */
        fish_notify.prototype.veFishList = $util.emptyArray;

        /**
         * Creates a new fish_notify instance using the specified properties.
         * @function create
         * @memberof fish_battle.fish_notify
         * @static
         * @param {fish_battle.Ifish_notify=} [properties] Properties to set
         * @returns {fish_battle.fish_notify} fish_notify instance
         */
        fish_notify.create = function create(properties) {
            return new fish_notify(properties);
        };

        /**
         * Encodes the specified fish_notify message. Does not implicitly {@link fish_battle.fish_notify.verify|verify} messages.
         * @function encode
         * @memberof fish_battle.fish_notify
         * @static
         * @param {fish_battle.Ifish_notify} message fish_notify message or plain object to encode
         * @param {$protobuf.Writer} [writer] Writer to encode to
         * @returns {$protobuf.Writer} Writer
         */
        fish_notify.encode = function encode(message, writer) {
            if (!writer)
                writer = $Writer.create();
            if (message.veFishList != null && message.veFishList.length)
                for (var i = 0; i < message.veFishList.length; ++i)
                    $root.fish_battle.fish__info.encode(message.veFishList[i], writer.uint32(/* id 1, wireType 2 =*/10).fork()).ldelim();
            return writer;
        };

        /**
         * Encodes the specified fish_notify message, length delimited. Does not implicitly {@link fish_battle.fish_notify.verify|verify} messages.
         * @function encodeDelimited
         * @memberof fish_battle.fish_notify
         * @static
         * @param {fish_battle.Ifish_notify} message fish_notify message or plain object to encode
         * @param {$protobuf.Writer} [writer] Writer to encode to
         * @returns {$protobuf.Writer} Writer
         */
        fish_notify.encodeDelimited = function encodeDelimited(message, writer) {
            return this.encode(message, writer).ldelim();
        };

        /**
         * Decodes a fish_notify message from the specified reader or buffer.
         * @function decode
         * @memberof fish_battle.fish_notify
         * @static
         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
         * @param {number} [length] Message length if known beforehand
         * @returns {fish_battle.fish_notify} fish_notify
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        fish_notify.decode = function decode(reader, length) {
            if (!(reader instanceof $Reader))
                reader = $Reader.create(reader);
            var end = length === undefined ? reader.len : reader.pos + length, message = new $root.fish_battle.fish_notify();
            while (reader.pos < end) {
                var tag = reader.uint32();
                switch (tag >>> 3) {
                case 1:
                    if (!(message.veFishList && message.veFishList.length))
                        message.veFishList = [];
                    message.veFishList.push($root.fish_battle.fish__info.decode(reader, reader.uint32()));
                    break;
                default:
                    reader.skipType(tag & 7);
                    break;
                }
            }
            return message;
        };

        /**
         * Decodes a fish_notify message from the specified reader or buffer, length delimited.
         * @function decodeDelimited
         * @memberof fish_battle.fish_notify
         * @static
         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
         * @returns {fish_battle.fish_notify} fish_notify
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        fish_notify.decodeDelimited = function decodeDelimited(reader) {
            if (!(reader instanceof $Reader))
                reader = new $Reader(reader);
            return this.decode(reader, reader.uint32());
        };

        /**
         * Verifies a fish_notify message.
         * @function verify
         * @memberof fish_battle.fish_notify
         * @static
         * @param {Object.<string,*>} message Plain object to verify
         * @returns {string|null} `null` if valid, otherwise the reason why it is not
         */
        fish_notify.verify = function verify(message) {
            if (typeof message !== "object" || message === null)
                return "object expected";
            if (message.veFishList != null && message.hasOwnProperty("veFishList")) {
                if (!Array.isArray(message.veFishList))
                    return "veFishList: array expected";
                for (var i = 0; i < message.veFishList.length; ++i) {
                    var error = $root.fish_battle.fish__info.verify(message.veFishList[i]);
                    if (error)
                        return "veFishList." + error;
                }
            }
            return null;
        };

        /**
         * Creates a fish_notify message from a plain object. Also converts values to their respective internal types.
         * @function fromObject
         * @memberof fish_battle.fish_notify
         * @static
         * @param {Object.<string,*>} object Plain object
         * @returns {fish_battle.fish_notify} fish_notify
         */
        fish_notify.fromObject = function fromObject(object) {
            if (object instanceof $root.fish_battle.fish_notify)
                return object;
            var message = new $root.fish_battle.fish_notify();
            if (object.veFishList) {
                if (!Array.isArray(object.veFishList))
                    throw TypeError(".fish_battle.fish_notify.veFishList: array expected");
                message.veFishList = [];
                for (var i = 0; i < object.veFishList.length; ++i) {
                    if (typeof object.veFishList[i] !== "object")
                        throw TypeError(".fish_battle.fish_notify.veFishList: object expected");
                    message.veFishList[i] = $root.fish_battle.fish__info.fromObject(object.veFishList[i]);
                }
            }
            return message;
        };

        /**
         * Creates a plain object from a fish_notify message. Also converts values to other types if specified.
         * @function toObject
         * @memberof fish_battle.fish_notify
         * @static
         * @param {fish_battle.fish_notify} message fish_notify
         * @param {$protobuf.IConversionOptions} [options] Conversion options
         * @returns {Object.<string,*>} Plain object
         */
        fish_notify.toObject = function toObject(message, options) {
            if (!options)
                options = {};
            var object = {};
            if (options.arrays || options.defaults)
                object.veFishList = [];
            if (message.veFishList && message.veFishList.length) {
                object.veFishList = [];
                for (var j = 0; j < message.veFishList.length; ++j)
                    object.veFishList[j] = $root.fish_battle.fish__info.toObject(message.veFishList[j], options);
            }
            return object;
        };

        /**
         * Converts this fish_notify to JSON.
         * @function toJSON
         * @memberof fish_battle.fish_notify
         * @instance
         * @returns {Object.<string,*>} JSON object
         */
        fish_notify.prototype.toJSON = function toJSON() {
            return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
        };

        return fish_notify;
    })();

    fish_battle.room__info = (function() {

        /**
         * Properties of a room__info.
         * @memberof fish_battle
         * @interface Iroom__info
         * @property {number} nRoomIndex room__info nRoomIndex
         */

        /**
         * Constructs a new room__info.
         * @memberof fish_battle
         * @classdesc Represents a room__info.
         * @implements Iroom__info
         * @constructor
         * @param {fish_battle.Iroom__info=} [properties] Properties to set
         */
        function room__info(properties) {
            if (properties)
                for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                    if (properties[keys[i]] != null)
                        this[keys[i]] = properties[keys[i]];
        }

        /**
         * room__info nRoomIndex.
         * @member {number} nRoomIndex
         * @memberof fish_battle.room__info
         * @instance
         */
        room__info.prototype.nRoomIndex = 0;

        /**
         * Creates a new room__info instance using the specified properties.
         * @function create
         * @memberof fish_battle.room__info
         * @static
         * @param {fish_battle.Iroom__info=} [properties] Properties to set
         * @returns {fish_battle.room__info} room__info instance
         */
        room__info.create = function create(properties) {
            return new room__info(properties);
        };

        /**
         * Encodes the specified room__info message. Does not implicitly {@link fish_battle.room__info.verify|verify} messages.
         * @function encode
         * @memberof fish_battle.room__info
         * @static
         * @param {fish_battle.Iroom__info} message room__info message or plain object to encode
         * @param {$protobuf.Writer} [writer] Writer to encode to
         * @returns {$protobuf.Writer} Writer
         */
        room__info.encode = function encode(message, writer) {
            if (!writer)
                writer = $Writer.create();
            writer.uint32(/* id 1, wireType 0 =*/8).int32(message.nRoomIndex);
            return writer;
        };

        /**
         * Encodes the specified room__info message, length delimited. Does not implicitly {@link fish_battle.room__info.verify|verify} messages.
         * @function encodeDelimited
         * @memberof fish_battle.room__info
         * @static
         * @param {fish_battle.Iroom__info} message room__info message or plain object to encode
         * @param {$protobuf.Writer} [writer] Writer to encode to
         * @returns {$protobuf.Writer} Writer
         */
        room__info.encodeDelimited = function encodeDelimited(message, writer) {
            return this.encode(message, writer).ldelim();
        };

        /**
         * Decodes a room__info message from the specified reader or buffer.
         * @function decode
         * @memberof fish_battle.room__info
         * @static
         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
         * @param {number} [length] Message length if known beforehand
         * @returns {fish_battle.room__info} room__info
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        room__info.decode = function decode(reader, length) {
            if (!(reader instanceof $Reader))
                reader = $Reader.create(reader);
            var end = length === undefined ? reader.len : reader.pos + length, message = new $root.fish_battle.room__info();
            while (reader.pos < end) {
                var tag = reader.uint32();
                switch (tag >>> 3) {
                case 1:
                    message.nRoomIndex = reader.int32();
                    break;
                default:
                    reader.skipType(tag & 7);
                    break;
                }
            }
            if (!message.hasOwnProperty("nRoomIndex"))
                throw $util.ProtocolError("missing required 'nRoomIndex'", { instance: message });
            return message;
        };

        /**
         * Decodes a room__info message from the specified reader or buffer, length delimited.
         * @function decodeDelimited
         * @memberof fish_battle.room__info
         * @static
         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
         * @returns {fish_battle.room__info} room__info
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        room__info.decodeDelimited = function decodeDelimited(reader) {
            if (!(reader instanceof $Reader))
                reader = new $Reader(reader);
            return this.decode(reader, reader.uint32());
        };

        /**
         * Verifies a room__info message.
         * @function verify
         * @memberof fish_battle.room__info
         * @static
         * @param {Object.<string,*>} message Plain object to verify
         * @returns {string|null} `null` if valid, otherwise the reason why it is not
         */
        room__info.verify = function verify(message) {
            if (typeof message !== "object" || message === null)
                return "object expected";
            if (!$util.isInteger(message.nRoomIndex))
                return "nRoomIndex: integer expected";
            return null;
        };

        /**
         * Creates a room__info message from a plain object. Also converts values to their respective internal types.
         * @function fromObject
         * @memberof fish_battle.room__info
         * @static
         * @param {Object.<string,*>} object Plain object
         * @returns {fish_battle.room__info} room__info
         */
        room__info.fromObject = function fromObject(object) {
            if (object instanceof $root.fish_battle.room__info)
                return object;
            var message = new $root.fish_battle.room__info();
            if (object.nRoomIndex != null)
                message.nRoomIndex = object.nRoomIndex | 0;
            return message;
        };

        /**
         * Creates a plain object from a room__info message. Also converts values to other types if specified.
         * @function toObject
         * @memberof fish_battle.room__info
         * @static
         * @param {fish_battle.room__info} message room__info
         * @param {$protobuf.IConversionOptions} [options] Conversion options
         * @returns {Object.<string,*>} Plain object
         */
        room__info.toObject = function toObject(message, options) {
            if (!options)
                options = {};
            var object = {};
            if (options.defaults)
                object.nRoomIndex = 0;
            if (message.nRoomIndex != null && message.hasOwnProperty("nRoomIndex"))
                object.nRoomIndex = message.nRoomIndex;
            return object;
        };

        /**
         * Converts this room__info to JSON.
         * @function toJSON
         * @memberof fish_battle.room__info
         * @instance
         * @returns {Object.<string,*>} JSON object
         */
        room__info.prototype.toJSON = function toJSON() {
            return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
        };

        return room__info;
    })();

    fish_battle.fire_bullet_req = (function() {

        /**
         * Properties of a fire_bullet_req.
         * @memberof fish_battle
         * @interface Ifire_bullet_req
         * @property {number} nBulletIndex fire_bullet_req nBulletIndex
         * @property {number} nAngle fire_bullet_req nAngle
         * @property {number} nBatteryMulti fire_bullet_req nBatteryMulti
         * @property {number|null} [nTrackFishIndex] fire_bullet_req nTrackFishIndex
         */

        /**
         * Constructs a new fire_bullet_req.
         * @memberof fish_battle
         * @classdesc Represents a fire_bullet_req.
         * @implements Ifire_bullet_req
         * @constructor
         * @param {fish_battle.Ifire_bullet_req=} [properties] Properties to set
         */
        function fire_bullet_req(properties) {
            if (properties)
                for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                    if (properties[keys[i]] != null)
                        this[keys[i]] = properties[keys[i]];
        }

        /**
         * fire_bullet_req nBulletIndex.
         * @member {number} nBulletIndex
         * @memberof fish_battle.fire_bullet_req
         * @instance
         */
        fire_bullet_req.prototype.nBulletIndex = 0;

        /**
         * fire_bullet_req nAngle.
         * @member {number} nAngle
         * @memberof fish_battle.fire_bullet_req
         * @instance
         */
        fire_bullet_req.prototype.nAngle = 0;

        /**
         * fire_bullet_req nBatteryMulti.
         * @member {number} nBatteryMulti
         * @memberof fish_battle.fire_bullet_req
         * @instance
         */
        fire_bullet_req.prototype.nBatteryMulti = 0;

        /**
         * fire_bullet_req nTrackFishIndex.
         * @member {number} nTrackFishIndex
         * @memberof fish_battle.fire_bullet_req
         * @instance
         */
        fire_bullet_req.prototype.nTrackFishIndex = 0;

        /**
         * Creates a new fire_bullet_req instance using the specified properties.
         * @function create
         * @memberof fish_battle.fire_bullet_req
         * @static
         * @param {fish_battle.Ifire_bullet_req=} [properties] Properties to set
         * @returns {fish_battle.fire_bullet_req} fire_bullet_req instance
         */
        fire_bullet_req.create = function create(properties) {
            return new fire_bullet_req(properties);
        };

        /**
         * Encodes the specified fire_bullet_req message. Does not implicitly {@link fish_battle.fire_bullet_req.verify|verify} messages.
         * @function encode
         * @memberof fish_battle.fire_bullet_req
         * @static
         * @param {fish_battle.Ifire_bullet_req} message fire_bullet_req message or plain object to encode
         * @param {$protobuf.Writer} [writer] Writer to encode to
         * @returns {$protobuf.Writer} Writer
         */
        fire_bullet_req.encode = function encode(message, writer) {
            if (!writer)
                writer = $Writer.create();
            writer.uint32(/* id 1, wireType 0 =*/8).int32(message.nBulletIndex);
            writer.uint32(/* id 2, wireType 0 =*/16).int32(message.nAngle);
            writer.uint32(/* id 3, wireType 0 =*/24).int32(message.nBatteryMulti);
            if (message.nTrackFishIndex != null && message.hasOwnProperty("nTrackFishIndex"))
                writer.uint32(/* id 4, wireType 0 =*/32).int32(message.nTrackFishIndex);
            return writer;
        };

        /**
         * Encodes the specified fire_bullet_req message, length delimited. Does not implicitly {@link fish_battle.fire_bullet_req.verify|verify} messages.
         * @function encodeDelimited
         * @memberof fish_battle.fire_bullet_req
         * @static
         * @param {fish_battle.Ifire_bullet_req} message fire_bullet_req message or plain object to encode
         * @param {$protobuf.Writer} [writer] Writer to encode to
         * @returns {$protobuf.Writer} Writer
         */
        fire_bullet_req.encodeDelimited = function encodeDelimited(message, writer) {
            return this.encode(message, writer).ldelim();
        };

        /**
         * Decodes a fire_bullet_req message from the specified reader or buffer.
         * @function decode
         * @memberof fish_battle.fire_bullet_req
         * @static
         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
         * @param {number} [length] Message length if known beforehand
         * @returns {fish_battle.fire_bullet_req} fire_bullet_req
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        fire_bullet_req.decode = function decode(reader, length) {
            if (!(reader instanceof $Reader))
                reader = $Reader.create(reader);
            var end = length === undefined ? reader.len : reader.pos + length, message = new $root.fish_battle.fire_bullet_req();
            while (reader.pos < end) {
                var tag = reader.uint32();
                switch (tag >>> 3) {
                case 1:
                    message.nBulletIndex = reader.int32();
                    break;
                case 2:
                    message.nAngle = reader.int32();
                    break;
                case 3:
                    message.nBatteryMulti = reader.int32();
                    break;
                case 4:
                    message.nTrackFishIndex = reader.int32();
                    break;
                default:
                    reader.skipType(tag & 7);
                    break;
                }
            }
            if (!message.hasOwnProperty("nBulletIndex"))
                throw $util.ProtocolError("missing required 'nBulletIndex'", { instance: message });
            if (!message.hasOwnProperty("nAngle"))
                throw $util.ProtocolError("missing required 'nAngle'", { instance: message });
            if (!message.hasOwnProperty("nBatteryMulti"))
                throw $util.ProtocolError("missing required 'nBatteryMulti'", { instance: message });
            return message;
        };

        /**
         * Decodes a fire_bullet_req message from the specified reader or buffer, length delimited.
         * @function decodeDelimited
         * @memberof fish_battle.fire_bullet_req
         * @static
         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
         * @returns {fish_battle.fire_bullet_req} fire_bullet_req
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        fire_bullet_req.decodeDelimited = function decodeDelimited(reader) {
            if (!(reader instanceof $Reader))
                reader = new $Reader(reader);
            return this.decode(reader, reader.uint32());
        };

        /**
         * Verifies a fire_bullet_req message.
         * @function verify
         * @memberof fish_battle.fire_bullet_req
         * @static
         * @param {Object.<string,*>} message Plain object to verify
         * @returns {string|null} `null` if valid, otherwise the reason why it is not
         */
        fire_bullet_req.verify = function verify(message) {
            if (typeof message !== "object" || message === null)
                return "object expected";
            if (!$util.isInteger(message.nBulletIndex))
                return "nBulletIndex: integer expected";
            if (!$util.isInteger(message.nAngle))
                return "nAngle: integer expected";
            if (!$util.isInteger(message.nBatteryMulti))
                return "nBatteryMulti: integer expected";
            if (message.nTrackFishIndex != null && message.hasOwnProperty("nTrackFishIndex"))
                if (!$util.isInteger(message.nTrackFishIndex))
                    return "nTrackFishIndex: integer expected";
            return null;
        };

        /**
         * Creates a fire_bullet_req message from a plain object. Also converts values to their respective internal types.
         * @function fromObject
         * @memberof fish_battle.fire_bullet_req
         * @static
         * @param {Object.<string,*>} object Plain object
         * @returns {fish_battle.fire_bullet_req} fire_bullet_req
         */
        fire_bullet_req.fromObject = function fromObject(object) {
            if (object instanceof $root.fish_battle.fire_bullet_req)
                return object;
            var message = new $root.fish_battle.fire_bullet_req();
            if (object.nBulletIndex != null)
                message.nBulletIndex = object.nBulletIndex | 0;
            if (object.nAngle != null)
                message.nAngle = object.nAngle | 0;
            if (object.nBatteryMulti != null)
                message.nBatteryMulti = object.nBatteryMulti | 0;
            if (object.nTrackFishIndex != null)
                message.nTrackFishIndex = object.nTrackFishIndex | 0;
            return message;
        };

        /**
         * Creates a plain object from a fire_bullet_req message. Also converts values to other types if specified.
         * @function toObject
         * @memberof fish_battle.fire_bullet_req
         * @static
         * @param {fish_battle.fire_bullet_req} message fire_bullet_req
         * @param {$protobuf.IConversionOptions} [options] Conversion options
         * @returns {Object.<string,*>} Plain object
         */
        fire_bullet_req.toObject = function toObject(message, options) {
            if (!options)
                options = {};
            var object = {};
            if (options.defaults) {
                object.nBulletIndex = 0;
                object.nAngle = 0;
                object.nBatteryMulti = 0;
                object.nTrackFishIndex = 0;
            }
            if (message.nBulletIndex != null && message.hasOwnProperty("nBulletIndex"))
                object.nBulletIndex = message.nBulletIndex;
            if (message.nAngle != null && message.hasOwnProperty("nAngle"))
                object.nAngle = message.nAngle;
            if (message.nBatteryMulti != null && message.hasOwnProperty("nBatteryMulti"))
                object.nBatteryMulti = message.nBatteryMulti;
            if (message.nTrackFishIndex != null && message.hasOwnProperty("nTrackFishIndex"))
                object.nTrackFishIndex = message.nTrackFishIndex;
            return object;
        };

        /**
         * Converts this fire_bullet_req to JSON.
         * @function toJSON
         * @memberof fish_battle.fire_bullet_req
         * @instance
         * @returns {Object.<string,*>} JSON object
         */
        fire_bullet_req.prototype.toJSON = function toJSON() {
            return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
        };

        return fire_bullet_req;
    })();

    fish_battle.fire_bullet_notify = (function() {

        /**
         * Properties of a fire_bullet_notify.
         * @memberof fish_battle
         * @interface Ifire_bullet_notify
         * @property {string} strUserId fire_bullet_notify strUserId
         * @property {number} nConsumeMoney fire_bullet_notify nConsumeMoney
         * @property {number} nAngle fire_bullet_notify nAngle
         * @property {number|null} [nTrackFishIndex] fire_bullet_notify nTrackFishIndex
         */

        /**
         * Constructs a new fire_bullet_notify.
         * @memberof fish_battle
         * @classdesc Represents a fire_bullet_notify.
         * @implements Ifire_bullet_notify
         * @constructor
         * @param {fish_battle.Ifire_bullet_notify=} [properties] Properties to set
         */
        function fire_bullet_notify(properties) {
            if (properties)
                for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                    if (properties[keys[i]] != null)
                        this[keys[i]] = properties[keys[i]];
        }

        /**
         * fire_bullet_notify strUserId.
         * @member {string} strUserId
         * @memberof fish_battle.fire_bullet_notify
         * @instance
         */
        fire_bullet_notify.prototype.strUserId = "";

        /**
         * fire_bullet_notify nConsumeMoney.
         * @member {number} nConsumeMoney
         * @memberof fish_battle.fire_bullet_notify
         * @instance
         */
        fire_bullet_notify.prototype.nConsumeMoney = 0;

        /**
         * fire_bullet_notify nAngle.
         * @member {number} nAngle
         * @memberof fish_battle.fire_bullet_notify
         * @instance
         */
        fire_bullet_notify.prototype.nAngle = 0;

        /**
         * fire_bullet_notify nTrackFishIndex.
         * @member {number} nTrackFishIndex
         * @memberof fish_battle.fire_bullet_notify
         * @instance
         */
        fire_bullet_notify.prototype.nTrackFishIndex = 0;

        /**
         * Creates a new fire_bullet_notify instance using the specified properties.
         * @function create
         * @memberof fish_battle.fire_bullet_notify
         * @static
         * @param {fish_battle.Ifire_bullet_notify=} [properties] Properties to set
         * @returns {fish_battle.fire_bullet_notify} fire_bullet_notify instance
         */
        fire_bullet_notify.create = function create(properties) {
            return new fire_bullet_notify(properties);
        };

        /**
         * Encodes the specified fire_bullet_notify message. Does not implicitly {@link fish_battle.fire_bullet_notify.verify|verify} messages.
         * @function encode
         * @memberof fish_battle.fire_bullet_notify
         * @static
         * @param {fish_battle.Ifire_bullet_notify} message fire_bullet_notify message or plain object to encode
         * @param {$protobuf.Writer} [writer] Writer to encode to
         * @returns {$protobuf.Writer} Writer
         */
        fire_bullet_notify.encode = function encode(message, writer) {
            if (!writer)
                writer = $Writer.create();
            writer.uint32(/* id 1, wireType 2 =*/10).string(message.strUserId);
            writer.uint32(/* id 2, wireType 0 =*/16).int32(message.nConsumeMoney);
            writer.uint32(/* id 3, wireType 0 =*/24).int32(message.nAngle);
            if (message.nTrackFishIndex != null && message.hasOwnProperty("nTrackFishIndex"))
                writer.uint32(/* id 4, wireType 0 =*/32).int32(message.nTrackFishIndex);
            return writer;
        };

        /**
         * Encodes the specified fire_bullet_notify message, length delimited. Does not implicitly {@link fish_battle.fire_bullet_notify.verify|verify} messages.
         * @function encodeDelimited
         * @memberof fish_battle.fire_bullet_notify
         * @static
         * @param {fish_battle.Ifire_bullet_notify} message fire_bullet_notify message or plain object to encode
         * @param {$protobuf.Writer} [writer] Writer to encode to
         * @returns {$protobuf.Writer} Writer
         */
        fire_bullet_notify.encodeDelimited = function encodeDelimited(message, writer) {
            return this.encode(message, writer).ldelim();
        };

        /**
         * Decodes a fire_bullet_notify message from the specified reader or buffer.
         * @function decode
         * @memberof fish_battle.fire_bullet_notify
         * @static
         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
         * @param {number} [length] Message length if known beforehand
         * @returns {fish_battle.fire_bullet_notify} fire_bullet_notify
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        fire_bullet_notify.decode = function decode(reader, length) {
            if (!(reader instanceof $Reader))
                reader = $Reader.create(reader);
            var end = length === undefined ? reader.len : reader.pos + length, message = new $root.fish_battle.fire_bullet_notify();
            while (reader.pos < end) {
                var tag = reader.uint32();
                switch (tag >>> 3) {
                case 1:
                    message.strUserId = reader.string();
                    break;
                case 2:
                    message.nConsumeMoney = reader.int32();
                    break;
                case 3:
                    message.nAngle = reader.int32();
                    break;
                case 4:
                    message.nTrackFishIndex = reader.int32();
                    break;
                default:
                    reader.skipType(tag & 7);
                    break;
                }
            }
            if (!message.hasOwnProperty("strUserId"))
                throw $util.ProtocolError("missing required 'strUserId'", { instance: message });
            if (!message.hasOwnProperty("nConsumeMoney"))
                throw $util.ProtocolError("missing required 'nConsumeMoney'", { instance: message });
            if (!message.hasOwnProperty("nAngle"))
                throw $util.ProtocolError("missing required 'nAngle'", { instance: message });
            return message;
        };

        /**
         * Decodes a fire_bullet_notify message from the specified reader or buffer, length delimited.
         * @function decodeDelimited
         * @memberof fish_battle.fire_bullet_notify
         * @static
         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
         * @returns {fish_battle.fire_bullet_notify} fire_bullet_notify
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        fire_bullet_notify.decodeDelimited = function decodeDelimited(reader) {
            if (!(reader instanceof $Reader))
                reader = new $Reader(reader);
            return this.decode(reader, reader.uint32());
        };

        /**
         * Verifies a fire_bullet_notify message.
         * @function verify
         * @memberof fish_battle.fire_bullet_notify
         * @static
         * @param {Object.<string,*>} message Plain object to verify
         * @returns {string|null} `null` if valid, otherwise the reason why it is not
         */
        fire_bullet_notify.verify = function verify(message) {
            if (typeof message !== "object" || message === null)
                return "object expected";
            if (!$util.isString(message.strUserId))
                return "strUserId: string expected";
            if (!$util.isInteger(message.nConsumeMoney))
                return "nConsumeMoney: integer expected";
            if (!$util.isInteger(message.nAngle))
                return "nAngle: integer expected";
            if (message.nTrackFishIndex != null && message.hasOwnProperty("nTrackFishIndex"))
                if (!$util.isInteger(message.nTrackFishIndex))
                    return "nTrackFishIndex: integer expected";
            return null;
        };

        /**
         * Creates a fire_bullet_notify message from a plain object. Also converts values to their respective internal types.
         * @function fromObject
         * @memberof fish_battle.fire_bullet_notify
         * @static
         * @param {Object.<string,*>} object Plain object
         * @returns {fish_battle.fire_bullet_notify} fire_bullet_notify
         */
        fire_bullet_notify.fromObject = function fromObject(object) {
            if (object instanceof $root.fish_battle.fire_bullet_notify)
                return object;
            var message = new $root.fish_battle.fire_bullet_notify();
            if (object.strUserId != null)
                message.strUserId = String(object.strUserId);
            if (object.nConsumeMoney != null)
                message.nConsumeMoney = object.nConsumeMoney | 0;
            if (object.nAngle != null)
                message.nAngle = object.nAngle | 0;
            if (object.nTrackFishIndex != null)
                message.nTrackFishIndex = object.nTrackFishIndex | 0;
            return message;
        };

        /**
         * Creates a plain object from a fire_bullet_notify message. Also converts values to other types if specified.
         * @function toObject
         * @memberof fish_battle.fire_bullet_notify
         * @static
         * @param {fish_battle.fire_bullet_notify} message fire_bullet_notify
         * @param {$protobuf.IConversionOptions} [options] Conversion options
         * @returns {Object.<string,*>} Plain object
         */
        fire_bullet_notify.toObject = function toObject(message, options) {
            if (!options)
                options = {};
            var object = {};
            if (options.defaults) {
                object.strUserId = "";
                object.nConsumeMoney = 0;
                object.nAngle = 0;
                object.nTrackFishIndex = 0;
            }
            if (message.strUserId != null && message.hasOwnProperty("strUserId"))
                object.strUserId = message.strUserId;
            if (message.nConsumeMoney != null && message.hasOwnProperty("nConsumeMoney"))
                object.nConsumeMoney = message.nConsumeMoney;
            if (message.nAngle != null && message.hasOwnProperty("nAngle"))
                object.nAngle = message.nAngle;
            if (message.nTrackFishIndex != null && message.hasOwnProperty("nTrackFishIndex"))
                object.nTrackFishIndex = message.nTrackFishIndex;
            return object;
        };

        /**
         * Converts this fire_bullet_notify to JSON.
         * @function toJSON
         * @memberof fish_battle.fire_bullet_notify
         * @instance
         * @returns {Object.<string,*>} JSON object
         */
        fire_bullet_notify.prototype.toJSON = function toJSON() {
            return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
        };

        return fire_bullet_notify;
    })();

    fish_battle.bullet_and_fish_collision_req = (function() {

        /**
         * Properties of a bullet_and_fish_collision_req.
         * @memberof fish_battle
         * @interface Ibullet_and_fish_collision_req
         * @property {number} nBulletIndex bullet_and_fish_collision_req nBulletIndex
         * @property {Array.<number>|null} [fishIdList] bullet_and_fish_collision_req fishIdList
         */

        /**
         * Constructs a new bullet_and_fish_collision_req.
         * @memberof fish_battle
         * @classdesc Represents a bullet_and_fish_collision_req.
         * @implements Ibullet_and_fish_collision_req
         * @constructor
         * @param {fish_battle.Ibullet_and_fish_collision_req=} [properties] Properties to set
         */
        function bullet_and_fish_collision_req(properties) {
            this.fishIdList = [];
            if (properties)
                for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                    if (properties[keys[i]] != null)
                        this[keys[i]] = properties[keys[i]];
        }

        /**
         * bullet_and_fish_collision_req nBulletIndex.
         * @member {number} nBulletIndex
         * @memberof fish_battle.bullet_and_fish_collision_req
         * @instance
         */
        bullet_and_fish_collision_req.prototype.nBulletIndex = 0;

        /**
         * bullet_and_fish_collision_req fishIdList.
         * @member {Array.<number>} fishIdList
         * @memberof fish_battle.bullet_and_fish_collision_req
         * @instance
         */
        bullet_and_fish_collision_req.prototype.fishIdList = $util.emptyArray;

        /**
         * Creates a new bullet_and_fish_collision_req instance using the specified properties.
         * @function create
         * @memberof fish_battle.bullet_and_fish_collision_req
         * @static
         * @param {fish_battle.Ibullet_and_fish_collision_req=} [properties] Properties to set
         * @returns {fish_battle.bullet_and_fish_collision_req} bullet_and_fish_collision_req instance
         */
        bullet_and_fish_collision_req.create = function create(properties) {
            return new bullet_and_fish_collision_req(properties);
        };

        /**
         * Encodes the specified bullet_and_fish_collision_req message. Does not implicitly {@link fish_battle.bullet_and_fish_collision_req.verify|verify} messages.
         * @function encode
         * @memberof fish_battle.bullet_and_fish_collision_req
         * @static
         * @param {fish_battle.Ibullet_and_fish_collision_req} message bullet_and_fish_collision_req message or plain object to encode
         * @param {$protobuf.Writer} [writer] Writer to encode to
         * @returns {$protobuf.Writer} Writer
         */
        bullet_and_fish_collision_req.encode = function encode(message, writer) {
            if (!writer)
                writer = $Writer.create();
            writer.uint32(/* id 1, wireType 0 =*/8).int32(message.nBulletIndex);
            if (message.fishIdList != null && message.fishIdList.length)
                for (var i = 0; i < message.fishIdList.length; ++i)
                    writer.uint32(/* id 2, wireType 0 =*/16).int32(message.fishIdList[i]);
            return writer;
        };

        /**
         * Encodes the specified bullet_and_fish_collision_req message, length delimited. Does not implicitly {@link fish_battle.bullet_and_fish_collision_req.verify|verify} messages.
         * @function encodeDelimited
         * @memberof fish_battle.bullet_and_fish_collision_req
         * @static
         * @param {fish_battle.Ibullet_and_fish_collision_req} message bullet_and_fish_collision_req message or plain object to encode
         * @param {$protobuf.Writer} [writer] Writer to encode to
         * @returns {$protobuf.Writer} Writer
         */
        bullet_and_fish_collision_req.encodeDelimited = function encodeDelimited(message, writer) {
            return this.encode(message, writer).ldelim();
        };

        /**
         * Decodes a bullet_and_fish_collision_req message from the specified reader or buffer.
         * @function decode
         * @memberof fish_battle.bullet_and_fish_collision_req
         * @static
         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
         * @param {number} [length] Message length if known beforehand
         * @returns {fish_battle.bullet_and_fish_collision_req} bullet_and_fish_collision_req
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        bullet_and_fish_collision_req.decode = function decode(reader, length) {
            if (!(reader instanceof $Reader))
                reader = $Reader.create(reader);
            var end = length === undefined ? reader.len : reader.pos + length, message = new $root.fish_battle.bullet_and_fish_collision_req();
            while (reader.pos < end) {
                var tag = reader.uint32();
                switch (tag >>> 3) {
                case 1:
                    message.nBulletIndex = reader.int32();
                    break;
                case 2:
                    if (!(message.fishIdList && message.fishIdList.length))
                        message.fishIdList = [];
                    if ((tag & 7) === 2) {
                        var end2 = reader.uint32() + reader.pos;
                        while (reader.pos < end2)
                            message.fishIdList.push(reader.int32());
                    } else
                        message.fishIdList.push(reader.int32());
                    break;
                default:
                    reader.skipType(tag & 7);
                    break;
                }
            }
            if (!message.hasOwnProperty("nBulletIndex"))
                throw $util.ProtocolError("missing required 'nBulletIndex'", { instance: message });
            return message;
        };

        /**
         * Decodes a bullet_and_fish_collision_req message from the specified reader or buffer, length delimited.
         * @function decodeDelimited
         * @memberof fish_battle.bullet_and_fish_collision_req
         * @static
         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
         * @returns {fish_battle.bullet_and_fish_collision_req} bullet_and_fish_collision_req
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        bullet_and_fish_collision_req.decodeDelimited = function decodeDelimited(reader) {
            if (!(reader instanceof $Reader))
                reader = new $Reader(reader);
            return this.decode(reader, reader.uint32());
        };

        /**
         * Verifies a bullet_and_fish_collision_req message.
         * @function verify
         * @memberof fish_battle.bullet_and_fish_collision_req
         * @static
         * @param {Object.<string,*>} message Plain object to verify
         * @returns {string|null} `null` if valid, otherwise the reason why it is not
         */
        bullet_and_fish_collision_req.verify = function verify(message) {
            if (typeof message !== "object" || message === null)
                return "object expected";
            if (!$util.isInteger(message.nBulletIndex))
                return "nBulletIndex: integer expected";
            if (message.fishIdList != null && message.hasOwnProperty("fishIdList")) {
                if (!Array.isArray(message.fishIdList))
                    return "fishIdList: array expected";
                for (var i = 0; i < message.fishIdList.length; ++i)
                    if (!$util.isInteger(message.fishIdList[i]))
                        return "fishIdList: integer[] expected";
            }
            return null;
        };

        /**
         * Creates a bullet_and_fish_collision_req message from a plain object. Also converts values to their respective internal types.
         * @function fromObject
         * @memberof fish_battle.bullet_and_fish_collision_req
         * @static
         * @param {Object.<string,*>} object Plain object
         * @returns {fish_battle.bullet_and_fish_collision_req} bullet_and_fish_collision_req
         */
        bullet_and_fish_collision_req.fromObject = function fromObject(object) {
            if (object instanceof $root.fish_battle.bullet_and_fish_collision_req)
                return object;
            var message = new $root.fish_battle.bullet_and_fish_collision_req();
            if (object.nBulletIndex != null)
                message.nBulletIndex = object.nBulletIndex | 0;
            if (object.fishIdList) {
                if (!Array.isArray(object.fishIdList))
                    throw TypeError(".fish_battle.bullet_and_fish_collision_req.fishIdList: array expected");
                message.fishIdList = [];
                for (var i = 0; i < object.fishIdList.length; ++i)
                    message.fishIdList[i] = object.fishIdList[i] | 0;
            }
            return message;
        };

        /**
         * Creates a plain object from a bullet_and_fish_collision_req message. Also converts values to other types if specified.
         * @function toObject
         * @memberof fish_battle.bullet_and_fish_collision_req
         * @static
         * @param {fish_battle.bullet_and_fish_collision_req} message bullet_and_fish_collision_req
         * @param {$protobuf.IConversionOptions} [options] Conversion options
         * @returns {Object.<string,*>} Plain object
         */
        bullet_and_fish_collision_req.toObject = function toObject(message, options) {
            if (!options)
                options = {};
            var object = {};
            if (options.arrays || options.defaults)
                object.fishIdList = [];
            if (options.defaults)
                object.nBulletIndex = 0;
            if (message.nBulletIndex != null && message.hasOwnProperty("nBulletIndex"))
                object.nBulletIndex = message.nBulletIndex;
            if (message.fishIdList && message.fishIdList.length) {
                object.fishIdList = [];
                for (var j = 0; j < message.fishIdList.length; ++j)
                    object.fishIdList[j] = message.fishIdList[j];
            }
            return object;
        };

        /**
         * Converts this bullet_and_fish_collision_req to JSON.
         * @function toJSON
         * @memberof fish_battle.bullet_and_fish_collision_req
         * @instance
         * @returns {Object.<string,*>} JSON object
         */
        bullet_and_fish_collision_req.prototype.toJSON = function toJSON() {
            return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
        };

        return bullet_and_fish_collision_req;
    })();

    fish_battle.reward_item_info = (function() {

        /**
         * Properties of a reward_item_info.
         * @memberof fish_battle
         * @interface Ireward_item_info
         * @property {number} nItemId reward_item_info nItemId
         * @property {number} nItemCount reward_item_info nItemCount
         */

        /**
         * Constructs a new reward_item_info.
         * @memberof fish_battle
         * @classdesc Represents a reward_item_info.
         * @implements Ireward_item_info
         * @constructor
         * @param {fish_battle.Ireward_item_info=} [properties] Properties to set
         */
        function reward_item_info(properties) {
            if (properties)
                for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                    if (properties[keys[i]] != null)
                        this[keys[i]] = properties[keys[i]];
        }

        /**
         * reward_item_info nItemId.
         * @member {number} nItemId
         * @memberof fish_battle.reward_item_info
         * @instance
         */
        reward_item_info.prototype.nItemId = 0;

        /**
         * reward_item_info nItemCount.
         * @member {number} nItemCount
         * @memberof fish_battle.reward_item_info
         * @instance
         */
        reward_item_info.prototype.nItemCount = 0;

        /**
         * Creates a new reward_item_info instance using the specified properties.
         * @function create
         * @memberof fish_battle.reward_item_info
         * @static
         * @param {fish_battle.Ireward_item_info=} [properties] Properties to set
         * @returns {fish_battle.reward_item_info} reward_item_info instance
         */
        reward_item_info.create = function create(properties) {
            return new reward_item_info(properties);
        };

        /**
         * Encodes the specified reward_item_info message. Does not implicitly {@link fish_battle.reward_item_info.verify|verify} messages.
         * @function encode
         * @memberof fish_battle.reward_item_info
         * @static
         * @param {fish_battle.Ireward_item_info} message reward_item_info message or plain object to encode
         * @param {$protobuf.Writer} [writer] Writer to encode to
         * @returns {$protobuf.Writer} Writer
         */
        reward_item_info.encode = function encode(message, writer) {
            if (!writer)
                writer = $Writer.create();
            writer.uint32(/* id 1, wireType 0 =*/8).int32(message.nItemId);
            writer.uint32(/* id 2, wireType 0 =*/16).int32(message.nItemCount);
            return writer;
        };

        /**
         * Encodes the specified reward_item_info message, length delimited. Does not implicitly {@link fish_battle.reward_item_info.verify|verify} messages.
         * @function encodeDelimited
         * @memberof fish_battle.reward_item_info
         * @static
         * @param {fish_battle.Ireward_item_info} message reward_item_info message or plain object to encode
         * @param {$protobuf.Writer} [writer] Writer to encode to
         * @returns {$protobuf.Writer} Writer
         */
        reward_item_info.encodeDelimited = function encodeDelimited(message, writer) {
            return this.encode(message, writer).ldelim();
        };

        /**
         * Decodes a reward_item_info message from the specified reader or buffer.
         * @function decode
         * @memberof fish_battle.reward_item_info
         * @static
         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
         * @param {number} [length] Message length if known beforehand
         * @returns {fish_battle.reward_item_info} reward_item_info
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        reward_item_info.decode = function decode(reader, length) {
            if (!(reader instanceof $Reader))
                reader = $Reader.create(reader);
            var end = length === undefined ? reader.len : reader.pos + length, message = new $root.fish_battle.reward_item_info();
            while (reader.pos < end) {
                var tag = reader.uint32();
                switch (tag >>> 3) {
                case 1:
                    message.nItemId = reader.int32();
                    break;
                case 2:
                    message.nItemCount = reader.int32();
                    break;
                default:
                    reader.skipType(tag & 7);
                    break;
                }
            }
            if (!message.hasOwnProperty("nItemId"))
                throw $util.ProtocolError("missing required 'nItemId'", { instance: message });
            if (!message.hasOwnProperty("nItemCount"))
                throw $util.ProtocolError("missing required 'nItemCount'", { instance: message });
            return message;
        };

        /**
         * Decodes a reward_item_info message from the specified reader or buffer, length delimited.
         * @function decodeDelimited
         * @memberof fish_battle.reward_item_info
         * @static
         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
         * @returns {fish_battle.reward_item_info} reward_item_info
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        reward_item_info.decodeDelimited = function decodeDelimited(reader) {
            if (!(reader instanceof $Reader))
                reader = new $Reader(reader);
            return this.decode(reader, reader.uint32());
        };

        /**
         * Verifies a reward_item_info message.
         * @function verify
         * @memberof fish_battle.reward_item_info
         * @static
         * @param {Object.<string,*>} message Plain object to verify
         * @returns {string|null} `null` if valid, otherwise the reason why it is not
         */
        reward_item_info.verify = function verify(message) {
            if (typeof message !== "object" || message === null)
                return "object expected";
            if (!$util.isInteger(message.nItemId))
                return "nItemId: integer expected";
            if (!$util.isInteger(message.nItemCount))
                return "nItemCount: integer expected";
            return null;
        };

        /**
         * Creates a reward_item_info message from a plain object. Also converts values to their respective internal types.
         * @function fromObject
         * @memberof fish_battle.reward_item_info
         * @static
         * @param {Object.<string,*>} object Plain object
         * @returns {fish_battle.reward_item_info} reward_item_info
         */
        reward_item_info.fromObject = function fromObject(object) {
            if (object instanceof $root.fish_battle.reward_item_info)
                return object;
            var message = new $root.fish_battle.reward_item_info();
            if (object.nItemId != null)
                message.nItemId = object.nItemId | 0;
            if (object.nItemCount != null)
                message.nItemCount = object.nItemCount | 0;
            return message;
        };

        /**
         * Creates a plain object from a reward_item_info message. Also converts values to other types if specified.
         * @function toObject
         * @memberof fish_battle.reward_item_info
         * @static
         * @param {fish_battle.reward_item_info} message reward_item_info
         * @param {$protobuf.IConversionOptions} [options] Conversion options
         * @returns {Object.<string,*>} Plain object
         */
        reward_item_info.toObject = function toObject(message, options) {
            if (!options)
                options = {};
            var object = {};
            if (options.defaults) {
                object.nItemId = 0;
                object.nItemCount = 0;
            }
            if (message.nItemId != null && message.hasOwnProperty("nItemId"))
                object.nItemId = message.nItemId;
            if (message.nItemCount != null && message.hasOwnProperty("nItemCount"))
                object.nItemCount = message.nItemCount;
            return object;
        };

        /**
         * Converts this reward_item_info to JSON.
         * @function toJSON
         * @memberof fish_battle.reward_item_info
         * @instance
         * @returns {Object.<string,*>} JSON object
         */
        reward_item_info.prototype.toJSON = function toJSON() {
            return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
        };

        return reward_item_info;
    })();

    fish_battle.fish_reward_info = (function() {

        /**
         * Properties of a fish_reward_info.
         * @memberof fish_battle
         * @interface Ifish_reward_info
         * @property {number} nFishId fish_reward_info nFishId
         * @property {Array.<fish_battle.Ireward_item_info>|null} [rewardInfo] fish_reward_info rewardInfo
         */

        /**
         * Constructs a new fish_reward_info.
         * @memberof fish_battle
         * @classdesc Represents a fish_reward_info.
         * @implements Ifish_reward_info
         * @constructor
         * @param {fish_battle.Ifish_reward_info=} [properties] Properties to set
         */
        function fish_reward_info(properties) {
            this.rewardInfo = [];
            if (properties)
                for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                    if (properties[keys[i]] != null)
                        this[keys[i]] = properties[keys[i]];
        }

        /**
         * fish_reward_info nFishId.
         * @member {number} nFishId
         * @memberof fish_battle.fish_reward_info
         * @instance
         */
        fish_reward_info.prototype.nFishId = 0;

        /**
         * fish_reward_info rewardInfo.
         * @member {Array.<fish_battle.Ireward_item_info>} rewardInfo
         * @memberof fish_battle.fish_reward_info
         * @instance
         */
        fish_reward_info.prototype.rewardInfo = $util.emptyArray;

        /**
         * Creates a new fish_reward_info instance using the specified properties.
         * @function create
         * @memberof fish_battle.fish_reward_info
         * @static
         * @param {fish_battle.Ifish_reward_info=} [properties] Properties to set
         * @returns {fish_battle.fish_reward_info} fish_reward_info instance
         */
        fish_reward_info.create = function create(properties) {
            return new fish_reward_info(properties);
        };

        /**
         * Encodes the specified fish_reward_info message. Does not implicitly {@link fish_battle.fish_reward_info.verify|verify} messages.
         * @function encode
         * @memberof fish_battle.fish_reward_info
         * @static
         * @param {fish_battle.Ifish_reward_info} message fish_reward_info message or plain object to encode
         * @param {$protobuf.Writer} [writer] Writer to encode to
         * @returns {$protobuf.Writer} Writer
         */
        fish_reward_info.encode = function encode(message, writer) {
            if (!writer)
                writer = $Writer.create();
            writer.uint32(/* id 1, wireType 0 =*/8).int32(message.nFishId);
            if (message.rewardInfo != null && message.rewardInfo.length)
                for (var i = 0; i < message.rewardInfo.length; ++i)
                    $root.fish_battle.reward_item_info.encode(message.rewardInfo[i], writer.uint32(/* id 2, wireType 2 =*/18).fork()).ldelim();
            return writer;
        };

        /**
         * Encodes the specified fish_reward_info message, length delimited. Does not implicitly {@link fish_battle.fish_reward_info.verify|verify} messages.
         * @function encodeDelimited
         * @memberof fish_battle.fish_reward_info
         * @static
         * @param {fish_battle.Ifish_reward_info} message fish_reward_info message or plain object to encode
         * @param {$protobuf.Writer} [writer] Writer to encode to
         * @returns {$protobuf.Writer} Writer
         */
        fish_reward_info.encodeDelimited = function encodeDelimited(message, writer) {
            return this.encode(message, writer).ldelim();
        };

        /**
         * Decodes a fish_reward_info message from the specified reader or buffer.
         * @function decode
         * @memberof fish_battle.fish_reward_info
         * @static
         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
         * @param {number} [length] Message length if known beforehand
         * @returns {fish_battle.fish_reward_info} fish_reward_info
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        fish_reward_info.decode = function decode(reader, length) {
            if (!(reader instanceof $Reader))
                reader = $Reader.create(reader);
            var end = length === undefined ? reader.len : reader.pos + length, message = new $root.fish_battle.fish_reward_info();
            while (reader.pos < end) {
                var tag = reader.uint32();
                switch (tag >>> 3) {
                case 1:
                    message.nFishId = reader.int32();
                    break;
                case 2:
                    if (!(message.rewardInfo && message.rewardInfo.length))
                        message.rewardInfo = [];
                    message.rewardInfo.push($root.fish_battle.reward_item_info.decode(reader, reader.uint32()));
                    break;
                default:
                    reader.skipType(tag & 7);
                    break;
                }
            }
            if (!message.hasOwnProperty("nFishId"))
                throw $util.ProtocolError("missing required 'nFishId'", { instance: message });
            return message;
        };

        /**
         * Decodes a fish_reward_info message from the specified reader or buffer, length delimited.
         * @function decodeDelimited
         * @memberof fish_battle.fish_reward_info
         * @static
         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
         * @returns {fish_battle.fish_reward_info} fish_reward_info
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        fish_reward_info.decodeDelimited = function decodeDelimited(reader) {
            if (!(reader instanceof $Reader))
                reader = new $Reader(reader);
            return this.decode(reader, reader.uint32());
        };

        /**
         * Verifies a fish_reward_info message.
         * @function verify
         * @memberof fish_battle.fish_reward_info
         * @static
         * @param {Object.<string,*>} message Plain object to verify
         * @returns {string|null} `null` if valid, otherwise the reason why it is not
         */
        fish_reward_info.verify = function verify(message) {
            if (typeof message !== "object" || message === null)
                return "object expected";
            if (!$util.isInteger(message.nFishId))
                return "nFishId: integer expected";
            if (message.rewardInfo != null && message.hasOwnProperty("rewardInfo")) {
                if (!Array.isArray(message.rewardInfo))
                    return "rewardInfo: array expected";
                for (var i = 0; i < message.rewardInfo.length; ++i) {
                    var error = $root.fish_battle.reward_item_info.verify(message.rewardInfo[i]);
                    if (error)
                        return "rewardInfo." + error;
                }
            }
            return null;
        };

        /**
         * Creates a fish_reward_info message from a plain object. Also converts values to their respective internal types.
         * @function fromObject
         * @memberof fish_battle.fish_reward_info
         * @static
         * @param {Object.<string,*>} object Plain object
         * @returns {fish_battle.fish_reward_info} fish_reward_info
         */
        fish_reward_info.fromObject = function fromObject(object) {
            if (object instanceof $root.fish_battle.fish_reward_info)
                return object;
            var message = new $root.fish_battle.fish_reward_info();
            if (object.nFishId != null)
                message.nFishId = object.nFishId | 0;
            if (object.rewardInfo) {
                if (!Array.isArray(object.rewardInfo))
                    throw TypeError(".fish_battle.fish_reward_info.rewardInfo: array expected");
                message.rewardInfo = [];
                for (var i = 0; i < object.rewardInfo.length; ++i) {
                    if (typeof object.rewardInfo[i] !== "object")
                        throw TypeError(".fish_battle.fish_reward_info.rewardInfo: object expected");
                    message.rewardInfo[i] = $root.fish_battle.reward_item_info.fromObject(object.rewardInfo[i]);
                }
            }
            return message;
        };

        /**
         * Creates a plain object from a fish_reward_info message. Also converts values to other types if specified.
         * @function toObject
         * @memberof fish_battle.fish_reward_info
         * @static
         * @param {fish_battle.fish_reward_info} message fish_reward_info
         * @param {$protobuf.IConversionOptions} [options] Conversion options
         * @returns {Object.<string,*>} Plain object
         */
        fish_reward_info.toObject = function toObject(message, options) {
            if (!options)
                options = {};
            var object = {};
            if (options.arrays || options.defaults)
                object.rewardInfo = [];
            if (options.defaults)
                object.nFishId = 0;
            if (message.nFishId != null && message.hasOwnProperty("nFishId"))
                object.nFishId = message.nFishId;
            if (message.rewardInfo && message.rewardInfo.length) {
                object.rewardInfo = [];
                for (var j = 0; j < message.rewardInfo.length; ++j)
                    object.rewardInfo[j] = $root.fish_battle.reward_item_info.toObject(message.rewardInfo[j], options);
            }
            return object;
        };

        /**
         * Converts this fish_reward_info to JSON.
         * @function toJSON
         * @memberof fish_battle.fish_reward_info
         * @instance
         * @returns {Object.<string,*>} JSON object
         */
        fish_reward_info.prototype.toJSON = function toJSON() {
            return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
        };

        return fish_reward_info;
    })();

    fish_battle.bullet_and_fish_collision_notify = (function() {

        /**
         * Properties of a bullet_and_fish_collision_notify.
         * @memberof fish_battle
         * @interface Ibullet_and_fish_collision_notify
         * @property {string} strUserId bullet_and_fish_collision_notify strUserId
         * @property {Array.<fish_battle.Ifish_reward_info>|null} [fishInfo] bullet_and_fish_collision_notify fishInfo
         * @property {number} nDie bullet_and_fish_collision_notify nDie
         */

        /**
         * Constructs a new bullet_and_fish_collision_notify.
         * @memberof fish_battle
         * @classdesc Represents a bullet_and_fish_collision_notify.
         * @implements Ibullet_and_fish_collision_notify
         * @constructor
         * @param {fish_battle.Ibullet_and_fish_collision_notify=} [properties] Properties to set
         */
        function bullet_and_fish_collision_notify(properties) {
            this.fishInfo = [];
            if (properties)
                for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                    if (properties[keys[i]] != null)
                        this[keys[i]] = properties[keys[i]];
        }

        /**
         * bullet_and_fish_collision_notify strUserId.
         * @member {string} strUserId
         * @memberof fish_battle.bullet_and_fish_collision_notify
         * @instance
         */
        bullet_and_fish_collision_notify.prototype.strUserId = "";

        /**
         * bullet_and_fish_collision_notify fishInfo.
         * @member {Array.<fish_battle.Ifish_reward_info>} fishInfo
         * @memberof fish_battle.bullet_and_fish_collision_notify
         * @instance
         */
        bullet_and_fish_collision_notify.prototype.fishInfo = $util.emptyArray;

        /**
         * bullet_and_fish_collision_notify nDie.
         * @member {number} nDie
         * @memberof fish_battle.bullet_and_fish_collision_notify
         * @instance
         */
        bullet_and_fish_collision_notify.prototype.nDie = 0;

        /**
         * Creates a new bullet_and_fish_collision_notify instance using the specified properties.
         * @function create
         * @memberof fish_battle.bullet_and_fish_collision_notify
         * @static
         * @param {fish_battle.Ibullet_and_fish_collision_notify=} [properties] Properties to set
         * @returns {fish_battle.bullet_and_fish_collision_notify} bullet_and_fish_collision_notify instance
         */
        bullet_and_fish_collision_notify.create = function create(properties) {
            return new bullet_and_fish_collision_notify(properties);
        };

        /**
         * Encodes the specified bullet_and_fish_collision_notify message. Does not implicitly {@link fish_battle.bullet_and_fish_collision_notify.verify|verify} messages.
         * @function encode
         * @memberof fish_battle.bullet_and_fish_collision_notify
         * @static
         * @param {fish_battle.Ibullet_and_fish_collision_notify} message bullet_and_fish_collision_notify message or plain object to encode
         * @param {$protobuf.Writer} [writer] Writer to encode to
         * @returns {$protobuf.Writer} Writer
         */
        bullet_and_fish_collision_notify.encode = function encode(message, writer) {
            if (!writer)
                writer = $Writer.create();
            writer.uint32(/* id 1, wireType 2 =*/10).string(message.strUserId);
            if (message.fishInfo != null && message.fishInfo.length)
                for (var i = 0; i < message.fishInfo.length; ++i)
                    $root.fish_battle.fish_reward_info.encode(message.fishInfo[i], writer.uint32(/* id 2, wireType 2 =*/18).fork()).ldelim();
            writer.uint32(/* id 3, wireType 0 =*/24).int32(message.nDie);
            return writer;
        };

        /**
         * Encodes the specified bullet_and_fish_collision_notify message, length delimited. Does not implicitly {@link fish_battle.bullet_and_fish_collision_notify.verify|verify} messages.
         * @function encodeDelimited
         * @memberof fish_battle.bullet_and_fish_collision_notify
         * @static
         * @param {fish_battle.Ibullet_and_fish_collision_notify} message bullet_and_fish_collision_notify message or plain object to encode
         * @param {$protobuf.Writer} [writer] Writer to encode to
         * @returns {$protobuf.Writer} Writer
         */
        bullet_and_fish_collision_notify.encodeDelimited = function encodeDelimited(message, writer) {
            return this.encode(message, writer).ldelim();
        };

        /**
         * Decodes a bullet_and_fish_collision_notify message from the specified reader or buffer.
         * @function decode
         * @memberof fish_battle.bullet_and_fish_collision_notify
         * @static
         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
         * @param {number} [length] Message length if known beforehand
         * @returns {fish_battle.bullet_and_fish_collision_notify} bullet_and_fish_collision_notify
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        bullet_and_fish_collision_notify.decode = function decode(reader, length) {
            if (!(reader instanceof $Reader))
                reader = $Reader.create(reader);
            var end = length === undefined ? reader.len : reader.pos + length, message = new $root.fish_battle.bullet_and_fish_collision_notify();
            while (reader.pos < end) {
                var tag = reader.uint32();
                switch (tag >>> 3) {
                case 1:
                    message.strUserId = reader.string();
                    break;
                case 2:
                    if (!(message.fishInfo && message.fishInfo.length))
                        message.fishInfo = [];
                    message.fishInfo.push($root.fish_battle.fish_reward_info.decode(reader, reader.uint32()));
                    break;
                case 3:
                    message.nDie = reader.int32();
                    break;
                default:
                    reader.skipType(tag & 7);
                    break;
                }
            }
            if (!message.hasOwnProperty("strUserId"))
                throw $util.ProtocolError("missing required 'strUserId'", { instance: message });
            if (!message.hasOwnProperty("nDie"))
                throw $util.ProtocolError("missing required 'nDie'", { instance: message });
            return message;
        };

        /**
         * Decodes a bullet_and_fish_collision_notify message from the specified reader or buffer, length delimited.
         * @function decodeDelimited
         * @memberof fish_battle.bullet_and_fish_collision_notify
         * @static
         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
         * @returns {fish_battle.bullet_and_fish_collision_notify} bullet_and_fish_collision_notify
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        bullet_and_fish_collision_notify.decodeDelimited = function decodeDelimited(reader) {
            if (!(reader instanceof $Reader))
                reader = new $Reader(reader);
            return this.decode(reader, reader.uint32());
        };

        /**
         * Verifies a bullet_and_fish_collision_notify message.
         * @function verify
         * @memberof fish_battle.bullet_and_fish_collision_notify
         * @static
         * @param {Object.<string,*>} message Plain object to verify
         * @returns {string|null} `null` if valid, otherwise the reason why it is not
         */
        bullet_and_fish_collision_notify.verify = function verify(message) {
            if (typeof message !== "object" || message === null)
                return "object expected";
            if (!$util.isString(message.strUserId))
                return "strUserId: string expected";
            if (message.fishInfo != null && message.hasOwnProperty("fishInfo")) {
                if (!Array.isArray(message.fishInfo))
                    return "fishInfo: array expected";
                for (var i = 0; i < message.fishInfo.length; ++i) {
                    var error = $root.fish_battle.fish_reward_info.verify(message.fishInfo[i]);
                    if (error)
                        return "fishInfo." + error;
                }
            }
            if (!$util.isInteger(message.nDie))
                return "nDie: integer expected";
            return null;
        };

        /**
         * Creates a bullet_and_fish_collision_notify message from a plain object. Also converts values to their respective internal types.
         * @function fromObject
         * @memberof fish_battle.bullet_and_fish_collision_notify
         * @static
         * @param {Object.<string,*>} object Plain object
         * @returns {fish_battle.bullet_and_fish_collision_notify} bullet_and_fish_collision_notify
         */
        bullet_and_fish_collision_notify.fromObject = function fromObject(object) {
            if (object instanceof $root.fish_battle.bullet_and_fish_collision_notify)
                return object;
            var message = new $root.fish_battle.bullet_and_fish_collision_notify();
            if (object.strUserId != null)
                message.strUserId = String(object.strUserId);
            if (object.fishInfo) {
                if (!Array.isArray(object.fishInfo))
                    throw TypeError(".fish_battle.bullet_and_fish_collision_notify.fishInfo: array expected");
                message.fishInfo = [];
                for (var i = 0; i < object.fishInfo.length; ++i) {
                    if (typeof object.fishInfo[i] !== "object")
                        throw TypeError(".fish_battle.bullet_and_fish_collision_notify.fishInfo: object expected");
                    message.fishInfo[i] = $root.fish_battle.fish_reward_info.fromObject(object.fishInfo[i]);
                }
            }
            if (object.nDie != null)
                message.nDie = object.nDie | 0;
            return message;
        };

        /**
         * Creates a plain object from a bullet_and_fish_collision_notify message. Also converts values to other types if specified.
         * @function toObject
         * @memberof fish_battle.bullet_and_fish_collision_notify
         * @static
         * @param {fish_battle.bullet_and_fish_collision_notify} message bullet_and_fish_collision_notify
         * @param {$protobuf.IConversionOptions} [options] Conversion options
         * @returns {Object.<string,*>} Plain object
         */
        bullet_and_fish_collision_notify.toObject = function toObject(message, options) {
            if (!options)
                options = {};
            var object = {};
            if (options.arrays || options.defaults)
                object.fishInfo = [];
            if (options.defaults) {
                object.strUserId = "";
                object.nDie = 0;
            }
            if (message.strUserId != null && message.hasOwnProperty("strUserId"))
                object.strUserId = message.strUserId;
            if (message.fishInfo && message.fishInfo.length) {
                object.fishInfo = [];
                for (var j = 0; j < message.fishInfo.length; ++j)
                    object.fishInfo[j] = $root.fish_battle.fish_reward_info.toObject(message.fishInfo[j], options);
            }
            if (message.nDie != null && message.hasOwnProperty("nDie"))
                object.nDie = message.nDie;
            return object;
        };

        /**
         * Converts this bullet_and_fish_collision_notify to JSON.
         * @function toJSON
         * @memberof fish_battle.bullet_and_fish_collision_notify
         * @instance
         * @returns {Object.<string,*>} JSON object
         */
        bullet_and_fish_collision_notify.prototype.toJSON = function toJSON() {
            return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
        };

        return bullet_and_fish_collision_notify;
    })();

    fish_battle.fish_buff_info = (function() {

        /**
         * Properties of a fish_buff_info.
         * @memberof fish_battle
         * @interface Ifish_buff_info
         * @property {number} nFishId fish_buff_info nFishId
         * @property {string} strFishBuff fish_buff_info strFishBuff
         */

        /**
         * Constructs a new fish_buff_info.
         * @memberof fish_battle
         * @classdesc Represents a fish_buff_info.
         * @implements Ifish_buff_info
         * @constructor
         * @param {fish_battle.Ifish_buff_info=} [properties] Properties to set
         */
        function fish_buff_info(properties) {
            if (properties)
                for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                    if (properties[keys[i]] != null)
                        this[keys[i]] = properties[keys[i]];
        }

        /**
         * fish_buff_info nFishId.
         * @member {number} nFishId
         * @memberof fish_battle.fish_buff_info
         * @instance
         */
        fish_buff_info.prototype.nFishId = 0;

        /**
         * fish_buff_info strFishBuff.
         * @member {string} strFishBuff
         * @memberof fish_battle.fish_buff_info
         * @instance
         */
        fish_buff_info.prototype.strFishBuff = "";

        /**
         * Creates a new fish_buff_info instance using the specified properties.
         * @function create
         * @memberof fish_battle.fish_buff_info
         * @static
         * @param {fish_battle.Ifish_buff_info=} [properties] Properties to set
         * @returns {fish_battle.fish_buff_info} fish_buff_info instance
         */
        fish_buff_info.create = function create(properties) {
            return new fish_buff_info(properties);
        };

        /**
         * Encodes the specified fish_buff_info message. Does not implicitly {@link fish_battle.fish_buff_info.verify|verify} messages.
         * @function encode
         * @memberof fish_battle.fish_buff_info
         * @static
         * @param {fish_battle.Ifish_buff_info} message fish_buff_info message or plain object to encode
         * @param {$protobuf.Writer} [writer] Writer to encode to
         * @returns {$protobuf.Writer} Writer
         */
        fish_buff_info.encode = function encode(message, writer) {
            if (!writer)
                writer = $Writer.create();
            writer.uint32(/* id 1, wireType 0 =*/8).int32(message.nFishId);
            writer.uint32(/* id 2, wireType 2 =*/18).string(message.strFishBuff);
            return writer;
        };

        /**
         * Encodes the specified fish_buff_info message, length delimited. Does not implicitly {@link fish_battle.fish_buff_info.verify|verify} messages.
         * @function encodeDelimited
         * @memberof fish_battle.fish_buff_info
         * @static
         * @param {fish_battle.Ifish_buff_info} message fish_buff_info message or plain object to encode
         * @param {$protobuf.Writer} [writer] Writer to encode to
         * @returns {$protobuf.Writer} Writer
         */
        fish_buff_info.encodeDelimited = function encodeDelimited(message, writer) {
            return this.encode(message, writer).ldelim();
        };

        /**
         * Decodes a fish_buff_info message from the specified reader or buffer.
         * @function decode
         * @memberof fish_battle.fish_buff_info
         * @static
         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
         * @param {number} [length] Message length if known beforehand
         * @returns {fish_battle.fish_buff_info} fish_buff_info
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        fish_buff_info.decode = function decode(reader, length) {
            if (!(reader instanceof $Reader))
                reader = $Reader.create(reader);
            var end = length === undefined ? reader.len : reader.pos + length, message = new $root.fish_battle.fish_buff_info();
            while (reader.pos < end) {
                var tag = reader.uint32();
                switch (tag >>> 3) {
                case 1:
                    message.nFishId = reader.int32();
                    break;
                case 2:
                    message.strFishBuff = reader.string();
                    break;
                default:
                    reader.skipType(tag & 7);
                    break;
                }
            }
            if (!message.hasOwnProperty("nFishId"))
                throw $util.ProtocolError("missing required 'nFishId'", { instance: message });
            if (!message.hasOwnProperty("strFishBuff"))
                throw $util.ProtocolError("missing required 'strFishBuff'", { instance: message });
            return message;
        };

        /**
         * Decodes a fish_buff_info message from the specified reader or buffer, length delimited.
         * @function decodeDelimited
         * @memberof fish_battle.fish_buff_info
         * @static
         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
         * @returns {fish_battle.fish_buff_info} fish_buff_info
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        fish_buff_info.decodeDelimited = function decodeDelimited(reader) {
            if (!(reader instanceof $Reader))
                reader = new $Reader(reader);
            return this.decode(reader, reader.uint32());
        };

        /**
         * Verifies a fish_buff_info message.
         * @function verify
         * @memberof fish_battle.fish_buff_info
         * @static
         * @param {Object.<string,*>} message Plain object to verify
         * @returns {string|null} `null` if valid, otherwise the reason why it is not
         */
        fish_buff_info.verify = function verify(message) {
            if (typeof message !== "object" || message === null)
                return "object expected";
            if (!$util.isInteger(message.nFishId))
                return "nFishId: integer expected";
            if (!$util.isString(message.strFishBuff))
                return "strFishBuff: string expected";
            return null;
        };

        /**
         * Creates a fish_buff_info message from a plain object. Also converts values to their respective internal types.
         * @function fromObject
         * @memberof fish_battle.fish_buff_info
         * @static
         * @param {Object.<string,*>} object Plain object
         * @returns {fish_battle.fish_buff_info} fish_buff_info
         */
        fish_buff_info.fromObject = function fromObject(object) {
            if (object instanceof $root.fish_battle.fish_buff_info)
                return object;
            var message = new $root.fish_battle.fish_buff_info();
            if (object.nFishId != null)
                message.nFishId = object.nFishId | 0;
            if (object.strFishBuff != null)
                message.strFishBuff = String(object.strFishBuff);
            return message;
        };

        /**
         * Creates a plain object from a fish_buff_info message. Also converts values to other types if specified.
         * @function toObject
         * @memberof fish_battle.fish_buff_info
         * @static
         * @param {fish_battle.fish_buff_info} message fish_buff_info
         * @param {$protobuf.IConversionOptions} [options] Conversion options
         * @returns {Object.<string,*>} Plain object
         */
        fish_buff_info.toObject = function toObject(message, options) {
            if (!options)
                options = {};
            var object = {};
            if (options.defaults) {
                object.nFishId = 0;
                object.strFishBuff = "";
            }
            if (message.nFishId != null && message.hasOwnProperty("nFishId"))
                object.nFishId = message.nFishId;
            if (message.strFishBuff != null && message.hasOwnProperty("strFishBuff"))
                object.strFishBuff = message.strFishBuff;
            return object;
        };

        /**
         * Converts this fish_buff_info to JSON.
         * @function toJSON
         * @memberof fish_battle.fish_buff_info
         * @instance
         * @returns {Object.<string,*>} JSON object
         */
        fish_buff_info.prototype.toJSON = function toJSON() {
            return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
        };

        return fish_buff_info;
    })();

    fish_battle.bullet_and_fish_collision_buff_notify = (function() {

        /**
         * Properties of a bullet_and_fish_collision_buff_notify.
         * @memberof fish_battle
         * @interface Ibullet_and_fish_collision_buff_notify
         * @property {string} strUserId bullet_and_fish_collision_buff_notify strUserId
         * @property {Array.<fish_battle.Ifish_buff_info>|null} [fishBuffInfo] bullet_and_fish_collision_buff_notify fishBuffInfo
         */

        /**
         * Constructs a new bullet_and_fish_collision_buff_notify.
         * @memberof fish_battle
         * @classdesc Represents a bullet_and_fish_collision_buff_notify.
         * @implements Ibullet_and_fish_collision_buff_notify
         * @constructor
         * @param {fish_battle.Ibullet_and_fish_collision_buff_notify=} [properties] Properties to set
         */
        function bullet_and_fish_collision_buff_notify(properties) {
            this.fishBuffInfo = [];
            if (properties)
                for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                    if (properties[keys[i]] != null)
                        this[keys[i]] = properties[keys[i]];
        }

        /**
         * bullet_and_fish_collision_buff_notify strUserId.
         * @member {string} strUserId
         * @memberof fish_battle.bullet_and_fish_collision_buff_notify
         * @instance
         */
        bullet_and_fish_collision_buff_notify.prototype.strUserId = "";

        /**
         * bullet_and_fish_collision_buff_notify fishBuffInfo.
         * @member {Array.<fish_battle.Ifish_buff_info>} fishBuffInfo
         * @memberof fish_battle.bullet_and_fish_collision_buff_notify
         * @instance
         */
        bullet_and_fish_collision_buff_notify.prototype.fishBuffInfo = $util.emptyArray;

        /**
         * Creates a new bullet_and_fish_collision_buff_notify instance using the specified properties.
         * @function create
         * @memberof fish_battle.bullet_and_fish_collision_buff_notify
         * @static
         * @param {fish_battle.Ibullet_and_fish_collision_buff_notify=} [properties] Properties to set
         * @returns {fish_battle.bullet_and_fish_collision_buff_notify} bullet_and_fish_collision_buff_notify instance
         */
        bullet_and_fish_collision_buff_notify.create = function create(properties) {
            return new bullet_and_fish_collision_buff_notify(properties);
        };

        /**
         * Encodes the specified bullet_and_fish_collision_buff_notify message. Does not implicitly {@link fish_battle.bullet_and_fish_collision_buff_notify.verify|verify} messages.
         * @function encode
         * @memberof fish_battle.bullet_and_fish_collision_buff_notify
         * @static
         * @param {fish_battle.Ibullet_and_fish_collision_buff_notify} message bullet_and_fish_collision_buff_notify message or plain object to encode
         * @param {$protobuf.Writer} [writer] Writer to encode to
         * @returns {$protobuf.Writer} Writer
         */
        bullet_and_fish_collision_buff_notify.encode = function encode(message, writer) {
            if (!writer)
                writer = $Writer.create();
            writer.uint32(/* id 1, wireType 2 =*/10).string(message.strUserId);
            if (message.fishBuffInfo != null && message.fishBuffInfo.length)
                for (var i = 0; i < message.fishBuffInfo.length; ++i)
                    $root.fish_battle.fish_buff_info.encode(message.fishBuffInfo[i], writer.uint32(/* id 2, wireType 2 =*/18).fork()).ldelim();
            return writer;
        };

        /**
         * Encodes the specified bullet_and_fish_collision_buff_notify message, length delimited. Does not implicitly {@link fish_battle.bullet_and_fish_collision_buff_notify.verify|verify} messages.
         * @function encodeDelimited
         * @memberof fish_battle.bullet_and_fish_collision_buff_notify
         * @static
         * @param {fish_battle.Ibullet_and_fish_collision_buff_notify} message bullet_and_fish_collision_buff_notify message or plain object to encode
         * @param {$protobuf.Writer} [writer] Writer to encode to
         * @returns {$protobuf.Writer} Writer
         */
        bullet_and_fish_collision_buff_notify.encodeDelimited = function encodeDelimited(message, writer) {
            return this.encode(message, writer).ldelim();
        };

        /**
         * Decodes a bullet_and_fish_collision_buff_notify message from the specified reader or buffer.
         * @function decode
         * @memberof fish_battle.bullet_and_fish_collision_buff_notify
         * @static
         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
         * @param {number} [length] Message length if known beforehand
         * @returns {fish_battle.bullet_and_fish_collision_buff_notify} bullet_and_fish_collision_buff_notify
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        bullet_and_fish_collision_buff_notify.decode = function decode(reader, length) {
            if (!(reader instanceof $Reader))
                reader = $Reader.create(reader);
            var end = length === undefined ? reader.len : reader.pos + length, message = new $root.fish_battle.bullet_and_fish_collision_buff_notify();
            while (reader.pos < end) {
                var tag = reader.uint32();
                switch (tag >>> 3) {
                case 1:
                    message.strUserId = reader.string();
                    break;
                case 2:
                    if (!(message.fishBuffInfo && message.fishBuffInfo.length))
                        message.fishBuffInfo = [];
                    message.fishBuffInfo.push($root.fish_battle.fish_buff_info.decode(reader, reader.uint32()));
                    break;
                default:
                    reader.skipType(tag & 7);
                    break;
                }
            }
            if (!message.hasOwnProperty("strUserId"))
                throw $util.ProtocolError("missing required 'strUserId'", { instance: message });
            return message;
        };

        /**
         * Decodes a bullet_and_fish_collision_buff_notify message from the specified reader or buffer, length delimited.
         * @function decodeDelimited
         * @memberof fish_battle.bullet_and_fish_collision_buff_notify
         * @static
         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
         * @returns {fish_battle.bullet_and_fish_collision_buff_notify} bullet_and_fish_collision_buff_notify
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        bullet_and_fish_collision_buff_notify.decodeDelimited = function decodeDelimited(reader) {
            if (!(reader instanceof $Reader))
                reader = new $Reader(reader);
            return this.decode(reader, reader.uint32());
        };

        /**
         * Verifies a bullet_and_fish_collision_buff_notify message.
         * @function verify
         * @memberof fish_battle.bullet_and_fish_collision_buff_notify
         * @static
         * @param {Object.<string,*>} message Plain object to verify
         * @returns {string|null} `null` if valid, otherwise the reason why it is not
         */
        bullet_and_fish_collision_buff_notify.verify = function verify(message) {
            if (typeof message !== "object" || message === null)
                return "object expected";
            if (!$util.isString(message.strUserId))
                return "strUserId: string expected";
            if (message.fishBuffInfo != null && message.hasOwnProperty("fishBuffInfo")) {
                if (!Array.isArray(message.fishBuffInfo))
                    return "fishBuffInfo: array expected";
                for (var i = 0; i < message.fishBuffInfo.length; ++i) {
                    var error = $root.fish_battle.fish_buff_info.verify(message.fishBuffInfo[i]);
                    if (error)
                        return "fishBuffInfo." + error;
                }
            }
            return null;
        };

        /**
         * Creates a bullet_and_fish_collision_buff_notify message from a plain object. Also converts values to their respective internal types.
         * @function fromObject
         * @memberof fish_battle.bullet_and_fish_collision_buff_notify
         * @static
         * @param {Object.<string,*>} object Plain object
         * @returns {fish_battle.bullet_and_fish_collision_buff_notify} bullet_and_fish_collision_buff_notify
         */
        bullet_and_fish_collision_buff_notify.fromObject = function fromObject(object) {
            if (object instanceof $root.fish_battle.bullet_and_fish_collision_buff_notify)
                return object;
            var message = new $root.fish_battle.bullet_and_fish_collision_buff_notify();
            if (object.strUserId != null)
                message.strUserId = String(object.strUserId);
            if (object.fishBuffInfo) {
                if (!Array.isArray(object.fishBuffInfo))
                    throw TypeError(".fish_battle.bullet_and_fish_collision_buff_notify.fishBuffInfo: array expected");
                message.fishBuffInfo = [];
                for (var i = 0; i < object.fishBuffInfo.length; ++i) {
                    if (typeof object.fishBuffInfo[i] !== "object")
                        throw TypeError(".fish_battle.bullet_and_fish_collision_buff_notify.fishBuffInfo: object expected");
                    message.fishBuffInfo[i] = $root.fish_battle.fish_buff_info.fromObject(object.fishBuffInfo[i]);
                }
            }
            return message;
        };

        /**
         * Creates a plain object from a bullet_and_fish_collision_buff_notify message. Also converts values to other types if specified.
         * @function toObject
         * @memberof fish_battle.bullet_and_fish_collision_buff_notify
         * @static
         * @param {fish_battle.bullet_and_fish_collision_buff_notify} message bullet_and_fish_collision_buff_notify
         * @param {$protobuf.IConversionOptions} [options] Conversion options
         * @returns {Object.<string,*>} Plain object
         */
        bullet_and_fish_collision_buff_notify.toObject = function toObject(message, options) {
            if (!options)
                options = {};
            var object = {};
            if (options.arrays || options.defaults)
                object.fishBuffInfo = [];
            if (options.defaults)
                object.strUserId = "";
            if (message.strUserId != null && message.hasOwnProperty("strUserId"))
                object.strUserId = message.strUserId;
            if (message.fishBuffInfo && message.fishBuffInfo.length) {
                object.fishBuffInfo = [];
                for (var j = 0; j < message.fishBuffInfo.length; ++j)
                    object.fishBuffInfo[j] = $root.fish_battle.fish_buff_info.toObject(message.fishBuffInfo[j], options);
            }
            return object;
        };

        /**
         * Converts this bullet_and_fish_collision_buff_notify to JSON.
         * @function toJSON
         * @memberof fish_battle.bullet_and_fish_collision_buff_notify
         * @instance
         * @returns {Object.<string,*>} JSON object
         */
        bullet_and_fish_collision_buff_notify.prototype.toJSON = function toJSON() {
            return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
        };

        return bullet_and_fish_collision_buff_notify;
    })();

    return fish_battle;
})();

$root.fish_login = (function() {

    /**
     * Namespace fish_login.
     * @exports fish_login
     * @namespace
     */
    var fish_login = {};

    fish_login.fish_login_req = (function() {

        /**
         * Properties of a fish_login_req.
         * @memberof fish_login
         * @interface Ifish_login_req
         * @property {string|null} [strTcyUserId] fish_login_req strTcyUserId
         * @property {number|null} [nChannelId] fish_login_req nChannelId
         * @property {number|null} [nOsType] fish_login_req nOsType
         * @property {number|null} [nSex] fish_login_req nSex
         * @property {number|null} [nFromId] fish_login_req nFromId
         * @property {string|null} [strDeviceId] fish_login_req strDeviceId
         * @property {string|null} [strNickName] fish_login_req strNickName
         * @property {string|null} [strVersion] fish_login_req strVersion
         * @property {string|null} [strImei] fish_login_req strImei
         * @property {string|null} [strImsi] fish_login_req strImsi
         */

        /**
         * Constructs a new fish_login_req.
         * @memberof fish_login
         * @classdesc Represents a fish_login_req.
         * @implements Ifish_login_req
         * @constructor
         * @param {fish_login.Ifish_login_req=} [properties] Properties to set
         */
        function fish_login_req(properties) {
            if (properties)
                for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                    if (properties[keys[i]] != null)
                        this[keys[i]] = properties[keys[i]];
        }

        /**
         * fish_login_req strTcyUserId.
         * @member {string} strTcyUserId
         * @memberof fish_login.fish_login_req
         * @instance
         */
        fish_login_req.prototype.strTcyUserId = "";

        /**
         * fish_login_req nChannelId.
         * @member {number} nChannelId
         * @memberof fish_login.fish_login_req
         * @instance
         */
        fish_login_req.prototype.nChannelId = 0;

        /**
         * fish_login_req nOsType.
         * @member {number} nOsType
         * @memberof fish_login.fish_login_req
         * @instance
         */
        fish_login_req.prototype.nOsType = 0;

        /**
         * fish_login_req nSex.
         * @member {number} nSex
         * @memberof fish_login.fish_login_req
         * @instance
         */
        fish_login_req.prototype.nSex = 0;

        /**
         * fish_login_req nFromId.
         * @member {number} nFromId
         * @memberof fish_login.fish_login_req
         * @instance
         */
        fish_login_req.prototype.nFromId = 0;

        /**
         * fish_login_req strDeviceId.
         * @member {string} strDeviceId
         * @memberof fish_login.fish_login_req
         * @instance
         */
        fish_login_req.prototype.strDeviceId = "";

        /**
         * fish_login_req strNickName.
         * @member {string} strNickName
         * @memberof fish_login.fish_login_req
         * @instance
         */
        fish_login_req.prototype.strNickName = "";

        /**
         * fish_login_req strVersion.
         * @member {string} strVersion
         * @memberof fish_login.fish_login_req
         * @instance
         */
        fish_login_req.prototype.strVersion = "";

        /**
         * fish_login_req strImei.
         * @member {string} strImei
         * @memberof fish_login.fish_login_req
         * @instance
         */
        fish_login_req.prototype.strImei = "";

        /**
         * fish_login_req strImsi.
         * @member {string} strImsi
         * @memberof fish_login.fish_login_req
         * @instance
         */
        fish_login_req.prototype.strImsi = "";

        /**
         * Creates a new fish_login_req instance using the specified properties.
         * @function create
         * @memberof fish_login.fish_login_req
         * @static
         * @param {fish_login.Ifish_login_req=} [properties] Properties to set
         * @returns {fish_login.fish_login_req} fish_login_req instance
         */
        fish_login_req.create = function create(properties) {
            return new fish_login_req(properties);
        };

        /**
         * Encodes the specified fish_login_req message. Does not implicitly {@link fish_login.fish_login_req.verify|verify} messages.
         * @function encode
         * @memberof fish_login.fish_login_req
         * @static
         * @param {fish_login.Ifish_login_req} message fish_login_req message or plain object to encode
         * @param {$protobuf.Writer} [writer] Writer to encode to
         * @returns {$protobuf.Writer} Writer
         */
        fish_login_req.encode = function encode(message, writer) {
            if (!writer)
                writer = $Writer.create();
            if (message.strTcyUserId != null && message.hasOwnProperty("strTcyUserId"))
                writer.uint32(/* id 1, wireType 2 =*/10).string(message.strTcyUserId);
            if (message.nChannelId != null && message.hasOwnProperty("nChannelId"))
                writer.uint32(/* id 2, wireType 0 =*/16).int32(message.nChannelId);
            if (message.nOsType != null && message.hasOwnProperty("nOsType"))
                writer.uint32(/* id 3, wireType 0 =*/24).int32(message.nOsType);
            if (message.nSex != null && message.hasOwnProperty("nSex"))
                writer.uint32(/* id 4, wireType 0 =*/32).int32(message.nSex);
            if (message.nFromId != null && message.hasOwnProperty("nFromId"))
                writer.uint32(/* id 5, wireType 0 =*/40).int32(message.nFromId);
            if (message.strDeviceId != null && message.hasOwnProperty("strDeviceId"))
                writer.uint32(/* id 6, wireType 2 =*/50).string(message.strDeviceId);
            if (message.strNickName != null && message.hasOwnProperty("strNickName"))
                writer.uint32(/* id 7, wireType 2 =*/58).string(message.strNickName);
            if (message.strVersion != null && message.hasOwnProperty("strVersion"))
                writer.uint32(/* id 8, wireType 2 =*/66).string(message.strVersion);
            if (message.strImei != null && message.hasOwnProperty("strImei"))
                writer.uint32(/* id 9, wireType 2 =*/74).string(message.strImei);
            if (message.strImsi != null && message.hasOwnProperty("strImsi"))
                writer.uint32(/* id 10, wireType 2 =*/82).string(message.strImsi);
            return writer;
        };

        /**
         * Encodes the specified fish_login_req message, length delimited. Does not implicitly {@link fish_login.fish_login_req.verify|verify} messages.
         * @function encodeDelimited
         * @memberof fish_login.fish_login_req
         * @static
         * @param {fish_login.Ifish_login_req} message fish_login_req message or plain object to encode
         * @param {$protobuf.Writer} [writer] Writer to encode to
         * @returns {$protobuf.Writer} Writer
         */
        fish_login_req.encodeDelimited = function encodeDelimited(message, writer) {
            return this.encode(message, writer).ldelim();
        };

        /**
         * Decodes a fish_login_req message from the specified reader or buffer.
         * @function decode
         * @memberof fish_login.fish_login_req
         * @static
         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
         * @param {number} [length] Message length if known beforehand
         * @returns {fish_login.fish_login_req} fish_login_req
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        fish_login_req.decode = function decode(reader, length) {
            if (!(reader instanceof $Reader))
                reader = $Reader.create(reader);
            var end = length === undefined ? reader.len : reader.pos + length, message = new $root.fish_login.fish_login_req();
            while (reader.pos < end) {
                var tag = reader.uint32();
                switch (tag >>> 3) {
                case 1:
                    message.strTcyUserId = reader.string();
                    break;
                case 2:
                    message.nChannelId = reader.int32();
                    break;
                case 3:
                    message.nOsType = reader.int32();
                    break;
                case 4:
                    message.nSex = reader.int32();
                    break;
                case 5:
                    message.nFromId = reader.int32();
                    break;
                case 6:
                    message.strDeviceId = reader.string();
                    break;
                case 7:
                    message.strNickName = reader.string();
                    break;
                case 8:
                    message.strVersion = reader.string();
                    break;
                case 9:
                    message.strImei = reader.string();
                    break;
                case 10:
                    message.strImsi = reader.string();
                    break;
                default:
                    reader.skipType(tag & 7);
                    break;
                }
            }
            return message;
        };

        /**
         * Decodes a fish_login_req message from the specified reader or buffer, length delimited.
         * @function decodeDelimited
         * @memberof fish_login.fish_login_req
         * @static
         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
         * @returns {fish_login.fish_login_req} fish_login_req
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        fish_login_req.decodeDelimited = function decodeDelimited(reader) {
            if (!(reader instanceof $Reader))
                reader = new $Reader(reader);
            return this.decode(reader, reader.uint32());
        };

        /**
         * Verifies a fish_login_req message.
         * @function verify
         * @memberof fish_login.fish_login_req
         * @static
         * @param {Object.<string,*>} message Plain object to verify
         * @returns {string|null} `null` if valid, otherwise the reason why it is not
         */
        fish_login_req.verify = function verify(message) {
            if (typeof message !== "object" || message === null)
                return "object expected";
            if (message.strTcyUserId != null && message.hasOwnProperty("strTcyUserId"))
                if (!$util.isString(message.strTcyUserId))
                    return "strTcyUserId: string expected";
            if (message.nChannelId != null && message.hasOwnProperty("nChannelId"))
                if (!$util.isInteger(message.nChannelId))
                    return "nChannelId: integer expected";
            if (message.nOsType != null && message.hasOwnProperty("nOsType"))
                if (!$util.isInteger(message.nOsType))
                    return "nOsType: integer expected";
            if (message.nSex != null && message.hasOwnProperty("nSex"))
                if (!$util.isInteger(message.nSex))
                    return "nSex: integer expected";
            if (message.nFromId != null && message.hasOwnProperty("nFromId"))
                if (!$util.isInteger(message.nFromId))
                    return "nFromId: integer expected";
            if (message.strDeviceId != null && message.hasOwnProperty("strDeviceId"))
                if (!$util.isString(message.strDeviceId))
                    return "strDeviceId: string expected";
            if (message.strNickName != null && message.hasOwnProperty("strNickName"))
                if (!$util.isString(message.strNickName))
                    return "strNickName: string expected";
            if (message.strVersion != null && message.hasOwnProperty("strVersion"))
                if (!$util.isString(message.strVersion))
                    return "strVersion: string expected";
            if (message.strImei != null && message.hasOwnProperty("strImei"))
                if (!$util.isString(message.strImei))
                    return "strImei: string expected";
            if (message.strImsi != null && message.hasOwnProperty("strImsi"))
                if (!$util.isString(message.strImsi))
                    return "strImsi: string expected";
            return null;
        };

        /**
         * Creates a fish_login_req message from a plain object. Also converts values to their respective internal types.
         * @function fromObject
         * @memberof fish_login.fish_login_req
         * @static
         * @param {Object.<string,*>} object Plain object
         * @returns {fish_login.fish_login_req} fish_login_req
         */
        fish_login_req.fromObject = function fromObject(object) {
            if (object instanceof $root.fish_login.fish_login_req)
                return object;
            var message = new $root.fish_login.fish_login_req();
            if (object.strTcyUserId != null)
                message.strTcyUserId = String(object.strTcyUserId);
            if (object.nChannelId != null)
                message.nChannelId = object.nChannelId | 0;
            if (object.nOsType != null)
                message.nOsType = object.nOsType | 0;
            if (object.nSex != null)
                message.nSex = object.nSex | 0;
            if (object.nFromId != null)
                message.nFromId = object.nFromId | 0;
            if (object.strDeviceId != null)
                message.strDeviceId = String(object.strDeviceId);
            if (object.strNickName != null)
                message.strNickName = String(object.strNickName);
            if (object.strVersion != null)
                message.strVersion = String(object.strVersion);
            if (object.strImei != null)
                message.strImei = String(object.strImei);
            if (object.strImsi != null)
                message.strImsi = String(object.strImsi);
            return message;
        };

        /**
         * Creates a plain object from a fish_login_req message. Also converts values to other types if specified.
         * @function toObject
         * @memberof fish_login.fish_login_req
         * @static
         * @param {fish_login.fish_login_req} message fish_login_req
         * @param {$protobuf.IConversionOptions} [options] Conversion options
         * @returns {Object.<string,*>} Plain object
         */
        fish_login_req.toObject = function toObject(message, options) {
            if (!options)
                options = {};
            var object = {};
            if (options.defaults) {
                object.strTcyUserId = "";
                object.nChannelId = 0;
                object.nOsType = 0;
                object.nSex = 0;
                object.nFromId = 0;
                object.strDeviceId = "";
                object.strNickName = "";
                object.strVersion = "";
                object.strImei = "";
                object.strImsi = "";
            }
            if (message.strTcyUserId != null && message.hasOwnProperty("strTcyUserId"))
                object.strTcyUserId = message.strTcyUserId;
            if (message.nChannelId != null && message.hasOwnProperty("nChannelId"))
                object.nChannelId = message.nChannelId;
            if (message.nOsType != null && message.hasOwnProperty("nOsType"))
                object.nOsType = message.nOsType;
            if (message.nSex != null && message.hasOwnProperty("nSex"))
                object.nSex = message.nSex;
            if (message.nFromId != null && message.hasOwnProperty("nFromId"))
                object.nFromId = message.nFromId;
            if (message.strDeviceId != null && message.hasOwnProperty("strDeviceId"))
                object.strDeviceId = message.strDeviceId;
            if (message.strNickName != null && message.hasOwnProperty("strNickName"))
                object.strNickName = message.strNickName;
            if (message.strVersion != null && message.hasOwnProperty("strVersion"))
                object.strVersion = message.strVersion;
            if (message.strImei != null && message.hasOwnProperty("strImei"))
                object.strImei = message.strImei;
            if (message.strImsi != null && message.hasOwnProperty("strImsi"))
                object.strImsi = message.strImsi;
            return object;
        };

        /**
         * Converts this fish_login_req to JSON.
         * @function toJSON
         * @memberof fish_login.fish_login_req
         * @instance
         * @returns {Object.<string,*>} JSON object
         */
        fish_login_req.prototype.toJSON = function toJSON() {
            return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
        };

        return fish_login_req;
    })();

    fish_login.fish_login_rsp = (function() {

        /**
         * Properties of a fish_login_rsp.
         * @memberof fish_login
         * @interface Ifish_login_rsp
         * @property {number} nStatus fish_login_rsp nStatus
         * @property {number|null} [nResult] fish_login_rsp nResult
         * @property {number|null} [nRoomId] fish_login_rsp nRoomId
         * @property {string|null} [strMsg] fish_login_rsp strMsg
         * @property {string|null} [strServerTime] fish_login_rsp strServerTime
         * @property {number|null} [nSwitch] fish_login_rsp nSwitch
         */

        /**
         * Constructs a new fish_login_rsp.
         * @memberof fish_login
         * @classdesc Represents a fish_login_rsp.
         * @implements Ifish_login_rsp
         * @constructor
         * @param {fish_login.Ifish_login_rsp=} [properties] Properties to set
         */
        function fish_login_rsp(properties) {
            if (properties)
                for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                    if (properties[keys[i]] != null)
                        this[keys[i]] = properties[keys[i]];
        }

        /**
         * fish_login_rsp nStatus.
         * @member {number} nStatus
         * @memberof fish_login.fish_login_rsp
         * @instance
         */
        fish_login_rsp.prototype.nStatus = 0;

        /**
         * fish_login_rsp nResult.
         * @member {number} nResult
         * @memberof fish_login.fish_login_rsp
         * @instance
         */
        fish_login_rsp.prototype.nResult = 0;

        /**
         * fish_login_rsp nRoomId.
         * @member {number} nRoomId
         * @memberof fish_login.fish_login_rsp
         * @instance
         */
        fish_login_rsp.prototype.nRoomId = 0;

        /**
         * fish_login_rsp strMsg.
         * @member {string} strMsg
         * @memberof fish_login.fish_login_rsp
         * @instance
         */
        fish_login_rsp.prototype.strMsg = "";

        /**
         * fish_login_rsp strServerTime.
         * @member {string} strServerTime
         * @memberof fish_login.fish_login_rsp
         * @instance
         */
        fish_login_rsp.prototype.strServerTime = "";

        /**
         * fish_login_rsp nSwitch.
         * @member {number} nSwitch
         * @memberof fish_login.fish_login_rsp
         * @instance
         */
        fish_login_rsp.prototype.nSwitch = 0;

        /**
         * Creates a new fish_login_rsp instance using the specified properties.
         * @function create
         * @memberof fish_login.fish_login_rsp
         * @static
         * @param {fish_login.Ifish_login_rsp=} [properties] Properties to set
         * @returns {fish_login.fish_login_rsp} fish_login_rsp instance
         */
        fish_login_rsp.create = function create(properties) {
            return new fish_login_rsp(properties);
        };

        /**
         * Encodes the specified fish_login_rsp message. Does not implicitly {@link fish_login.fish_login_rsp.verify|verify} messages.
         * @function encode
         * @memberof fish_login.fish_login_rsp
         * @static
         * @param {fish_login.Ifish_login_rsp} message fish_login_rsp message or plain object to encode
         * @param {$protobuf.Writer} [writer] Writer to encode to
         * @returns {$protobuf.Writer} Writer
         */
        fish_login_rsp.encode = function encode(message, writer) {
            if (!writer)
                writer = $Writer.create();
            writer.uint32(/* id 1, wireType 0 =*/8).int32(message.nStatus);
            if (message.nResult != null && message.hasOwnProperty("nResult"))
                writer.uint32(/* id 2, wireType 0 =*/16).int32(message.nResult);
            if (message.nRoomId != null && message.hasOwnProperty("nRoomId"))
                writer.uint32(/* id 3, wireType 0 =*/24).int32(message.nRoomId);
            if (message.strMsg != null && message.hasOwnProperty("strMsg"))
                writer.uint32(/* id 4, wireType 2 =*/34).string(message.strMsg);
            if (message.strServerTime != null && message.hasOwnProperty("strServerTime"))
                writer.uint32(/* id 5, wireType 2 =*/42).string(message.strServerTime);
            if (message.nSwitch != null && message.hasOwnProperty("nSwitch"))
                writer.uint32(/* id 6, wireType 0 =*/48).int32(message.nSwitch);
            return writer;
        };

        /**
         * Encodes the specified fish_login_rsp message, length delimited. Does not implicitly {@link fish_login.fish_login_rsp.verify|verify} messages.
         * @function encodeDelimited
         * @memberof fish_login.fish_login_rsp
         * @static
         * @param {fish_login.Ifish_login_rsp} message fish_login_rsp message or plain object to encode
         * @param {$protobuf.Writer} [writer] Writer to encode to
         * @returns {$protobuf.Writer} Writer
         */
        fish_login_rsp.encodeDelimited = function encodeDelimited(message, writer) {
            return this.encode(message, writer).ldelim();
        };

        /**
         * Decodes a fish_login_rsp message from the specified reader or buffer.
         * @function decode
         * @memberof fish_login.fish_login_rsp
         * @static
         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
         * @param {number} [length] Message length if known beforehand
         * @returns {fish_login.fish_login_rsp} fish_login_rsp
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        fish_login_rsp.decode = function decode(reader, length) {
            if (!(reader instanceof $Reader))
                reader = $Reader.create(reader);
            var end = length === undefined ? reader.len : reader.pos + length, message = new $root.fish_login.fish_login_rsp();
            while (reader.pos < end) {
                var tag = reader.uint32();
                switch (tag >>> 3) {
                case 1:
                    message.nStatus = reader.int32();
                    break;
                case 2:
                    message.nResult = reader.int32();
                    break;
                case 3:
                    message.nRoomId = reader.int32();
                    break;
                case 4:
                    message.strMsg = reader.string();
                    break;
                case 5:
                    message.strServerTime = reader.string();
                    break;
                case 6:
                    message.nSwitch = reader.int32();
                    break;
                default:
                    reader.skipType(tag & 7);
                    break;
                }
            }
            if (!message.hasOwnProperty("nStatus"))
                throw $util.ProtocolError("missing required 'nStatus'", { instance: message });
            return message;
        };

        /**
         * Decodes a fish_login_rsp message from the specified reader or buffer, length delimited.
         * @function decodeDelimited
         * @memberof fish_login.fish_login_rsp
         * @static
         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
         * @returns {fish_login.fish_login_rsp} fish_login_rsp
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        fish_login_rsp.decodeDelimited = function decodeDelimited(reader) {
            if (!(reader instanceof $Reader))
                reader = new $Reader(reader);
            return this.decode(reader, reader.uint32());
        };

        /**
         * Verifies a fish_login_rsp message.
         * @function verify
         * @memberof fish_login.fish_login_rsp
         * @static
         * @param {Object.<string,*>} message Plain object to verify
         * @returns {string|null} `null` if valid, otherwise the reason why it is not
         */
        fish_login_rsp.verify = function verify(message) {
            if (typeof message !== "object" || message === null)
                return "object expected";
            if (!$util.isInteger(message.nStatus))
                return "nStatus: integer expected";
            if (message.nResult != null && message.hasOwnProperty("nResult"))
                if (!$util.isInteger(message.nResult))
                    return "nResult: integer expected";
            if (message.nRoomId != null && message.hasOwnProperty("nRoomId"))
                if (!$util.isInteger(message.nRoomId))
                    return "nRoomId: integer expected";
            if (message.strMsg != null && message.hasOwnProperty("strMsg"))
                if (!$util.isString(message.strMsg))
                    return "strMsg: string expected";
            if (message.strServerTime != null && message.hasOwnProperty("strServerTime"))
                if (!$util.isString(message.strServerTime))
                    return "strServerTime: string expected";
            if (message.nSwitch != null && message.hasOwnProperty("nSwitch"))
                if (!$util.isInteger(message.nSwitch))
                    return "nSwitch: integer expected";
            return null;
        };

        /**
         * Creates a fish_login_rsp message from a plain object. Also converts values to their respective internal types.
         * @function fromObject
         * @memberof fish_login.fish_login_rsp
         * @static
         * @param {Object.<string,*>} object Plain object
         * @returns {fish_login.fish_login_rsp} fish_login_rsp
         */
        fish_login_rsp.fromObject = function fromObject(object) {
            if (object instanceof $root.fish_login.fish_login_rsp)
                return object;
            var message = new $root.fish_login.fish_login_rsp();
            if (object.nStatus != null)
                message.nStatus = object.nStatus | 0;
            if (object.nResult != null)
                message.nResult = object.nResult | 0;
            if (object.nRoomId != null)
                message.nRoomId = object.nRoomId | 0;
            if (object.strMsg != null)
                message.strMsg = String(object.strMsg);
            if (object.strServerTime != null)
                message.strServerTime = String(object.strServerTime);
            if (object.nSwitch != null)
                message.nSwitch = object.nSwitch | 0;
            return message;
        };

        /**
         * Creates a plain object from a fish_login_rsp message. Also converts values to other types if specified.
         * @function toObject
         * @memberof fish_login.fish_login_rsp
         * @static
         * @param {fish_login.fish_login_rsp} message fish_login_rsp
         * @param {$protobuf.IConversionOptions} [options] Conversion options
         * @returns {Object.<string,*>} Plain object
         */
        fish_login_rsp.toObject = function toObject(message, options) {
            if (!options)
                options = {};
            var object = {};
            if (options.defaults) {
                object.nStatus = 0;
                object.nResult = 0;
                object.nRoomId = 0;
                object.strMsg = "";
                object.strServerTime = "";
                object.nSwitch = 0;
            }
            if (message.nStatus != null && message.hasOwnProperty("nStatus"))
                object.nStatus = message.nStatus;
            if (message.nResult != null && message.hasOwnProperty("nResult"))
                object.nResult = message.nResult;
            if (message.nRoomId != null && message.hasOwnProperty("nRoomId"))
                object.nRoomId = message.nRoomId;
            if (message.strMsg != null && message.hasOwnProperty("strMsg"))
                object.strMsg = message.strMsg;
            if (message.strServerTime != null && message.hasOwnProperty("strServerTime"))
                object.strServerTime = message.strServerTime;
            if (message.nSwitch != null && message.hasOwnProperty("nSwitch"))
                object.nSwitch = message.nSwitch;
            return object;
        };

        /**
         * Converts this fish_login_rsp to JSON.
         * @function toJSON
         * @memberof fish_login.fish_login_rsp
         * @instance
         * @returns {Object.<string,*>} JSON object
         */
        fish_login_rsp.prototype.toJSON = function toJSON() {
            return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
        };

        return fish_login_rsp;
    })();

    fish_login.heartbeat = (function() {

        /**
         * Properties of a heartbeat.
         * @memberof fish_login
         * @interface Iheartbeat
         */

        /**
         * Constructs a new heartbeat.
         * @memberof fish_login
         * @classdesc Represents a heartbeat.
         * @implements Iheartbeat
         * @constructor
         * @param {fish_login.Iheartbeat=} [properties] Properties to set
         */
        function heartbeat(properties) {
            if (properties)
                for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                    if (properties[keys[i]] != null)
                        this[keys[i]] = properties[keys[i]];
        }

        /**
         * Creates a new heartbeat instance using the specified properties.
         * @function create
         * @memberof fish_login.heartbeat
         * @static
         * @param {fish_login.Iheartbeat=} [properties] Properties to set
         * @returns {fish_login.heartbeat} heartbeat instance
         */
        heartbeat.create = function create(properties) {
            return new heartbeat(properties);
        };

        /**
         * Encodes the specified heartbeat message. Does not implicitly {@link fish_login.heartbeat.verify|verify} messages.
         * @function encode
         * @memberof fish_login.heartbeat
         * @static
         * @param {fish_login.Iheartbeat} message heartbeat message or plain object to encode
         * @param {$protobuf.Writer} [writer] Writer to encode to
         * @returns {$protobuf.Writer} Writer
         */
        heartbeat.encode = function encode(message, writer) {
            if (!writer)
                writer = $Writer.create();
            return writer;
        };

        /**
         * Encodes the specified heartbeat message, length delimited. Does not implicitly {@link fish_login.heartbeat.verify|verify} messages.
         * @function encodeDelimited
         * @memberof fish_login.heartbeat
         * @static
         * @param {fish_login.Iheartbeat} message heartbeat message or plain object to encode
         * @param {$protobuf.Writer} [writer] Writer to encode to
         * @returns {$protobuf.Writer} Writer
         */
        heartbeat.encodeDelimited = function encodeDelimited(message, writer) {
            return this.encode(message, writer).ldelim();
        };

        /**
         * Decodes a heartbeat message from the specified reader or buffer.
         * @function decode
         * @memberof fish_login.heartbeat
         * @static
         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
         * @param {number} [length] Message length if known beforehand
         * @returns {fish_login.heartbeat} heartbeat
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        heartbeat.decode = function decode(reader, length) {
            if (!(reader instanceof $Reader))
                reader = $Reader.create(reader);
            var end = length === undefined ? reader.len : reader.pos + length, message = new $root.fish_login.heartbeat();
            while (reader.pos < end) {
                var tag = reader.uint32();
                switch (tag >>> 3) {
                default:
                    reader.skipType(tag & 7);
                    break;
                }
            }
            return message;
        };

        /**
         * Decodes a heartbeat message from the specified reader or buffer, length delimited.
         * @function decodeDelimited
         * @memberof fish_login.heartbeat
         * @static
         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
         * @returns {fish_login.heartbeat} heartbeat
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        heartbeat.decodeDelimited = function decodeDelimited(reader) {
            if (!(reader instanceof $Reader))
                reader = new $Reader(reader);
            return this.decode(reader, reader.uint32());
        };

        /**
         * Verifies a heartbeat message.
         * @function verify
         * @memberof fish_login.heartbeat
         * @static
         * @param {Object.<string,*>} message Plain object to verify
         * @returns {string|null} `null` if valid, otherwise the reason why it is not
         */
        heartbeat.verify = function verify(message) {
            if (typeof message !== "object" || message === null)
                return "object expected";
            return null;
        };

        /**
         * Creates a heartbeat message from a plain object. Also converts values to their respective internal types.
         * @function fromObject
         * @memberof fish_login.heartbeat
         * @static
         * @param {Object.<string,*>} object Plain object
         * @returns {fish_login.heartbeat} heartbeat
         */
        heartbeat.fromObject = function fromObject(object) {
            if (object instanceof $root.fish_login.heartbeat)
                return object;
            return new $root.fish_login.heartbeat();
        };

        /**
         * Creates a plain object from a heartbeat message. Also converts values to other types if specified.
         * @function toObject
         * @memberof fish_login.heartbeat
         * @static
         * @param {fish_login.heartbeat} message heartbeat
         * @param {$protobuf.IConversionOptions} [options] Conversion options
         * @returns {Object.<string,*>} Plain object
         */
        heartbeat.toObject = function toObject() {
            return {};
        };

        /**
         * Converts this heartbeat to JSON.
         * @function toJSON
         * @memberof fish_login.heartbeat
         * @instance
         * @returns {Object.<string,*>} JSON object
         */
        heartbeat.prototype.toJSON = function toJSON() {
            return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
        };

        return heartbeat;
    })();

    fish_login.heartbeat_ack = (function() {

        /**
         * Properties of a heartbeat_ack.
         * @memberof fish_login
         * @interface Iheartbeat_ack
         * @property {number|null} [time] heartbeat_ack time
         */

        /**
         * Constructs a new heartbeat_ack.
         * @memberof fish_login
         * @classdesc Represents a heartbeat_ack.
         * @implements Iheartbeat_ack
         * @constructor
         * @param {fish_login.Iheartbeat_ack=} [properties] Properties to set
         */
        function heartbeat_ack(properties) {
            if (properties)
                for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                    if (properties[keys[i]] != null)
                        this[keys[i]] = properties[keys[i]];
        }

        /**
         * heartbeat_ack time.
         * @member {number} time
         * @memberof fish_login.heartbeat_ack
         * @instance
         */
        heartbeat_ack.prototype.time = 0;

        /**
         * Creates a new heartbeat_ack instance using the specified properties.
         * @function create
         * @memberof fish_login.heartbeat_ack
         * @static
         * @param {fish_login.Iheartbeat_ack=} [properties] Properties to set
         * @returns {fish_login.heartbeat_ack} heartbeat_ack instance
         */
        heartbeat_ack.create = function create(properties) {
            return new heartbeat_ack(properties);
        };

        /**
         * Encodes the specified heartbeat_ack message. Does not implicitly {@link fish_login.heartbeat_ack.verify|verify} messages.
         * @function encode
         * @memberof fish_login.heartbeat_ack
         * @static
         * @param {fish_login.Iheartbeat_ack} message heartbeat_ack message or plain object to encode
         * @param {$protobuf.Writer} [writer] Writer to encode to
         * @returns {$protobuf.Writer} Writer
         */
        heartbeat_ack.encode = function encode(message, writer) {
            if (!writer)
                writer = $Writer.create();
            if (message.time != null && message.hasOwnProperty("time"))
                writer.uint32(/* id 1, wireType 0 =*/8).int32(message.time);
            return writer;
        };

        /**
         * Encodes the specified heartbeat_ack message, length delimited. Does not implicitly {@link fish_login.heartbeat_ack.verify|verify} messages.
         * @function encodeDelimited
         * @memberof fish_login.heartbeat_ack
         * @static
         * @param {fish_login.Iheartbeat_ack} message heartbeat_ack message or plain object to encode
         * @param {$protobuf.Writer} [writer] Writer to encode to
         * @returns {$protobuf.Writer} Writer
         */
        heartbeat_ack.encodeDelimited = function encodeDelimited(message, writer) {
            return this.encode(message, writer).ldelim();
        };

        /**
         * Decodes a heartbeat_ack message from the specified reader or buffer.
         * @function decode
         * @memberof fish_login.heartbeat_ack
         * @static
         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
         * @param {number} [length] Message length if known beforehand
         * @returns {fish_login.heartbeat_ack} heartbeat_ack
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        heartbeat_ack.decode = function decode(reader, length) {
            if (!(reader instanceof $Reader))
                reader = $Reader.create(reader);
            var end = length === undefined ? reader.len : reader.pos + length, message = new $root.fish_login.heartbeat_ack();
            while (reader.pos < end) {
                var tag = reader.uint32();
                switch (tag >>> 3) {
                case 1:
                    message.time = reader.int32();
                    break;
                default:
                    reader.skipType(tag & 7);
                    break;
                }
            }
            return message;
        };

        /**
         * Decodes a heartbeat_ack message from the specified reader or buffer, length delimited.
         * @function decodeDelimited
         * @memberof fish_login.heartbeat_ack
         * @static
         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
         * @returns {fish_login.heartbeat_ack} heartbeat_ack
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        heartbeat_ack.decodeDelimited = function decodeDelimited(reader) {
            if (!(reader instanceof $Reader))
                reader = new $Reader(reader);
            return this.decode(reader, reader.uint32());
        };

        /**
         * Verifies a heartbeat_ack message.
         * @function verify
         * @memberof fish_login.heartbeat_ack
         * @static
         * @param {Object.<string,*>} message Plain object to verify
         * @returns {string|null} `null` if valid, otherwise the reason why it is not
         */
        heartbeat_ack.verify = function verify(message) {
            if (typeof message !== "object" || message === null)
                return "object expected";
            if (message.time != null && message.hasOwnProperty("time"))
                if (!$util.isInteger(message.time))
                    return "time: integer expected";
            return null;
        };

        /**
         * Creates a heartbeat_ack message from a plain object. Also converts values to their respective internal types.
         * @function fromObject
         * @memberof fish_login.heartbeat_ack
         * @static
         * @param {Object.<string,*>} object Plain object
         * @returns {fish_login.heartbeat_ack} heartbeat_ack
         */
        heartbeat_ack.fromObject = function fromObject(object) {
            if (object instanceof $root.fish_login.heartbeat_ack)
                return object;
            var message = new $root.fish_login.heartbeat_ack();
            if (object.time != null)
                message.time = object.time | 0;
            return message;
        };

        /**
         * Creates a plain object from a heartbeat_ack message. Also converts values to other types if specified.
         * @function toObject
         * @memberof fish_login.heartbeat_ack
         * @static
         * @param {fish_login.heartbeat_ack} message heartbeat_ack
         * @param {$protobuf.IConversionOptions} [options] Conversion options
         * @returns {Object.<string,*>} Plain object
         */
        heartbeat_ack.toObject = function toObject(message, options) {
            if (!options)
                options = {};
            var object = {};
            if (options.defaults)
                object.time = 0;
            if (message.time != null && message.hasOwnProperty("time"))
                object.time = message.time;
            return object;
        };

        /**
         * Converts this heartbeat_ack to JSON.
         * @function toJSON
         * @memberof fish_login.heartbeat_ack
         * @instance
         * @returns {Object.<string,*>} JSON object
         */
        heartbeat_ack.prototype.toJSON = function toJSON() {
            return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
        };

        return heartbeat_ack;
    })();

    fish_login.repeatlogin = (function() {

        /**
         * Properties of a repeatlogin.
         * @memberof fish_login
         * @interface Irepeatlogin
         */

        /**
         * Constructs a new repeatlogin.
         * @memberof fish_login
         * @classdesc Represents a repeatlogin.
         * @implements Irepeatlogin
         * @constructor
         * @param {fish_login.Irepeatlogin=} [properties] Properties to set
         */
        function repeatlogin(properties) {
            if (properties)
                for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                    if (properties[keys[i]] != null)
                        this[keys[i]] = properties[keys[i]];
        }

        /**
         * Creates a new repeatlogin instance using the specified properties.
         * @function create
         * @memberof fish_login.repeatlogin
         * @static
         * @param {fish_login.Irepeatlogin=} [properties] Properties to set
         * @returns {fish_login.repeatlogin} repeatlogin instance
         */
        repeatlogin.create = function create(properties) {
            return new repeatlogin(properties);
        };

        /**
         * Encodes the specified repeatlogin message. Does not implicitly {@link fish_login.repeatlogin.verify|verify} messages.
         * @function encode
         * @memberof fish_login.repeatlogin
         * @static
         * @param {fish_login.Irepeatlogin} message repeatlogin message or plain object to encode
         * @param {$protobuf.Writer} [writer] Writer to encode to
         * @returns {$protobuf.Writer} Writer
         */
        repeatlogin.encode = function encode(message, writer) {
            if (!writer)
                writer = $Writer.create();
            return writer;
        };

        /**
         * Encodes the specified repeatlogin message, length delimited. Does not implicitly {@link fish_login.repeatlogin.verify|verify} messages.
         * @function encodeDelimited
         * @memberof fish_login.repeatlogin
         * @static
         * @param {fish_login.Irepeatlogin} message repeatlogin message or plain object to encode
         * @param {$protobuf.Writer} [writer] Writer to encode to
         * @returns {$protobuf.Writer} Writer
         */
        repeatlogin.encodeDelimited = function encodeDelimited(message, writer) {
            return this.encode(message, writer).ldelim();
        };

        /**
         * Decodes a repeatlogin message from the specified reader or buffer.
         * @function decode
         * @memberof fish_login.repeatlogin
         * @static
         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
         * @param {number} [length] Message length if known beforehand
         * @returns {fish_login.repeatlogin} repeatlogin
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        repeatlogin.decode = function decode(reader, length) {
            if (!(reader instanceof $Reader))
                reader = $Reader.create(reader);
            var end = length === undefined ? reader.len : reader.pos + length, message = new $root.fish_login.repeatlogin();
            while (reader.pos < end) {
                var tag = reader.uint32();
                switch (tag >>> 3) {
                default:
                    reader.skipType(tag & 7);
                    break;
                }
            }
            return message;
        };

        /**
         * Decodes a repeatlogin message from the specified reader or buffer, length delimited.
         * @function decodeDelimited
         * @memberof fish_login.repeatlogin
         * @static
         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
         * @returns {fish_login.repeatlogin} repeatlogin
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        repeatlogin.decodeDelimited = function decodeDelimited(reader) {
            if (!(reader instanceof $Reader))
                reader = new $Reader(reader);
            return this.decode(reader, reader.uint32());
        };

        /**
         * Verifies a repeatlogin message.
         * @function verify
         * @memberof fish_login.repeatlogin
         * @static
         * @param {Object.<string,*>} message Plain object to verify
         * @returns {string|null} `null` if valid, otherwise the reason why it is not
         */
        repeatlogin.verify = function verify(message) {
            if (typeof message !== "object" || message === null)
                return "object expected";
            return null;
        };

        /**
         * Creates a repeatlogin message from a plain object. Also converts values to their respective internal types.
         * @function fromObject
         * @memberof fish_login.repeatlogin
         * @static
         * @param {Object.<string,*>} object Plain object
         * @returns {fish_login.repeatlogin} repeatlogin
         */
        repeatlogin.fromObject = function fromObject(object) {
            if (object instanceof $root.fish_login.repeatlogin)
                return object;
            return new $root.fish_login.repeatlogin();
        };

        /**
         * Creates a plain object from a repeatlogin message. Also converts values to other types if specified.
         * @function toObject
         * @memberof fish_login.repeatlogin
         * @static
         * @param {fish_login.repeatlogin} message repeatlogin
         * @param {$protobuf.IConversionOptions} [options] Conversion options
         * @returns {Object.<string,*>} Plain object
         */
        repeatlogin.toObject = function toObject() {
            return {};
        };

        /**
         * Converts this repeatlogin to JSON.
         * @function toJSON
         * @memberof fish_login.repeatlogin
         * @instance
         * @returns {Object.<string,*>} JSON object
         */
        repeatlogin.prototype.toJSON = function toJSON() {
            return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
        };

        return repeatlogin;
    })();

    return fish_login;
})();

$root.fish_match = (function() {

    /**
     * Namespace fish_match.
     * @exports fish_match
     * @namespace
     */
    var fish_match = {};

    fish_match.fish_match_enter_req = (function() {

        /**
         * Properties of a fish_match_enter_req.
         * @memberof fish_match
         * @interface Ifish_match_enter_req
         * @property {number|null} [roomCfgId] fish_match_enter_req roomCfgId
         */

        /**
         * Constructs a new fish_match_enter_req.
         * @memberof fish_match
         * @classdesc Represents a fish_match_enter_req.
         * @implements Ifish_match_enter_req
         * @constructor
         * @param {fish_match.Ifish_match_enter_req=} [properties] Properties to set
         */
        function fish_match_enter_req(properties) {
            if (properties)
                for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                    if (properties[keys[i]] != null)
                        this[keys[i]] = properties[keys[i]];
        }

        /**
         * fish_match_enter_req roomCfgId.
         * @member {number} roomCfgId
         * @memberof fish_match.fish_match_enter_req
         * @instance
         */
        fish_match_enter_req.prototype.roomCfgId = 0;

        /**
         * Creates a new fish_match_enter_req instance using the specified properties.
         * @function create
         * @memberof fish_match.fish_match_enter_req
         * @static
         * @param {fish_match.Ifish_match_enter_req=} [properties] Properties to set
         * @returns {fish_match.fish_match_enter_req} fish_match_enter_req instance
         */
        fish_match_enter_req.create = function create(properties) {
            return new fish_match_enter_req(properties);
        };

        /**
         * Encodes the specified fish_match_enter_req message. Does not implicitly {@link fish_match.fish_match_enter_req.verify|verify} messages.
         * @function encode
         * @memberof fish_match.fish_match_enter_req
         * @static
         * @param {fish_match.Ifish_match_enter_req} message fish_match_enter_req message or plain object to encode
         * @param {$protobuf.Writer} [writer] Writer to encode to
         * @returns {$protobuf.Writer} Writer
         */
        fish_match_enter_req.encode = function encode(message, writer) {
            if (!writer)
                writer = $Writer.create();
            if (message.roomCfgId != null && message.hasOwnProperty("roomCfgId"))
                writer.uint32(/* id 1, wireType 0 =*/8).int32(message.roomCfgId);
            return writer;
        };

        /**
         * Encodes the specified fish_match_enter_req message, length delimited. Does not implicitly {@link fish_match.fish_match_enter_req.verify|verify} messages.
         * @function encodeDelimited
         * @memberof fish_match.fish_match_enter_req
         * @static
         * @param {fish_match.Ifish_match_enter_req} message fish_match_enter_req message or plain object to encode
         * @param {$protobuf.Writer} [writer] Writer to encode to
         * @returns {$protobuf.Writer} Writer
         */
        fish_match_enter_req.encodeDelimited = function encodeDelimited(message, writer) {
            return this.encode(message, writer).ldelim();
        };

        /**
         * Decodes a fish_match_enter_req message from the specified reader or buffer.
         * @function decode
         * @memberof fish_match.fish_match_enter_req
         * @static
         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
         * @param {number} [length] Message length if known beforehand
         * @returns {fish_match.fish_match_enter_req} fish_match_enter_req
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        fish_match_enter_req.decode = function decode(reader, length) {
            if (!(reader instanceof $Reader))
                reader = $Reader.create(reader);
            var end = length === undefined ? reader.len : reader.pos + length, message = new $root.fish_match.fish_match_enter_req();
            while (reader.pos < end) {
                var tag = reader.uint32();
                switch (tag >>> 3) {
                case 1:
                    message.roomCfgId = reader.int32();
                    break;
                default:
                    reader.skipType(tag & 7);
                    break;
                }
            }
            return message;
        };

        /**
         * Decodes a fish_match_enter_req message from the specified reader or buffer, length delimited.
         * @function decodeDelimited
         * @memberof fish_match.fish_match_enter_req
         * @static
         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
         * @returns {fish_match.fish_match_enter_req} fish_match_enter_req
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        fish_match_enter_req.decodeDelimited = function decodeDelimited(reader) {
            if (!(reader instanceof $Reader))
                reader = new $Reader(reader);
            return this.decode(reader, reader.uint32());
        };

        /**
         * Verifies a fish_match_enter_req message.
         * @function verify
         * @memberof fish_match.fish_match_enter_req
         * @static
         * @param {Object.<string,*>} message Plain object to verify
         * @returns {string|null} `null` if valid, otherwise the reason why it is not
         */
        fish_match_enter_req.verify = function verify(message) {
            if (typeof message !== "object" || message === null)
                return "object expected";
            if (message.roomCfgId != null && message.hasOwnProperty("roomCfgId"))
                if (!$util.isInteger(message.roomCfgId))
                    return "roomCfgId: integer expected";
            return null;
        };

        /**
         * Creates a fish_match_enter_req message from a plain object. Also converts values to their respective internal types.
         * @function fromObject
         * @memberof fish_match.fish_match_enter_req
         * @static
         * @param {Object.<string,*>} object Plain object
         * @returns {fish_match.fish_match_enter_req} fish_match_enter_req
         */
        fish_match_enter_req.fromObject = function fromObject(object) {
            if (object instanceof $root.fish_match.fish_match_enter_req)
                return object;
            var message = new $root.fish_match.fish_match_enter_req();
            if (object.roomCfgId != null)
                message.roomCfgId = object.roomCfgId | 0;
            return message;
        };

        /**
         * Creates a plain object from a fish_match_enter_req message. Also converts values to other types if specified.
         * @function toObject
         * @memberof fish_match.fish_match_enter_req
         * @static
         * @param {fish_match.fish_match_enter_req} message fish_match_enter_req
         * @param {$protobuf.IConversionOptions} [options] Conversion options
         * @returns {Object.<string,*>} Plain object
         */
        fish_match_enter_req.toObject = function toObject(message, options) {
            if (!options)
                options = {};
            var object = {};
            if (options.defaults)
                object.roomCfgId = 0;
            if (message.roomCfgId != null && message.hasOwnProperty("roomCfgId"))
                object.roomCfgId = message.roomCfgId;
            return object;
        };

        /**
         * Converts this fish_match_enter_req to JSON.
         * @function toJSON
         * @memberof fish_match.fish_match_enter_req
         * @instance
         * @returns {Object.<string,*>} JSON object
         */
        fish_match_enter_req.prototype.toJSON = function toJSON() {
            return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
        };

        return fish_match_enter_req;
    })();

    fish_match.fish_match_leave_room_req = (function() {

        /**
         * Properties of a fish_match_leave_room_req.
         * @memberof fish_match
         * @interface Ifish_match_leave_room_req
         */

        /**
         * Constructs a new fish_match_leave_room_req.
         * @memberof fish_match
         * @classdesc Represents a fish_match_leave_room_req.
         * @implements Ifish_match_leave_room_req
         * @constructor
         * @param {fish_match.Ifish_match_leave_room_req=} [properties] Properties to set
         */
        function fish_match_leave_room_req(properties) {
            if (properties)
                for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                    if (properties[keys[i]] != null)
                        this[keys[i]] = properties[keys[i]];
        }

        /**
         * Creates a new fish_match_leave_room_req instance using the specified properties.
         * @function create
         * @memberof fish_match.fish_match_leave_room_req
         * @static
         * @param {fish_match.Ifish_match_leave_room_req=} [properties] Properties to set
         * @returns {fish_match.fish_match_leave_room_req} fish_match_leave_room_req instance
         */
        fish_match_leave_room_req.create = function create(properties) {
            return new fish_match_leave_room_req(properties);
        };

        /**
         * Encodes the specified fish_match_leave_room_req message. Does not implicitly {@link fish_match.fish_match_leave_room_req.verify|verify} messages.
         * @function encode
         * @memberof fish_match.fish_match_leave_room_req
         * @static
         * @param {fish_match.Ifish_match_leave_room_req} message fish_match_leave_room_req message or plain object to encode
         * @param {$protobuf.Writer} [writer] Writer to encode to
         * @returns {$protobuf.Writer} Writer
         */
        fish_match_leave_room_req.encode = function encode(message, writer) {
            if (!writer)
                writer = $Writer.create();
            return writer;
        };

        /**
         * Encodes the specified fish_match_leave_room_req message, length delimited. Does not implicitly {@link fish_match.fish_match_leave_room_req.verify|verify} messages.
         * @function encodeDelimited
         * @memberof fish_match.fish_match_leave_room_req
         * @static
         * @param {fish_match.Ifish_match_leave_room_req} message fish_match_leave_room_req message or plain object to encode
         * @param {$protobuf.Writer} [writer] Writer to encode to
         * @returns {$protobuf.Writer} Writer
         */
        fish_match_leave_room_req.encodeDelimited = function encodeDelimited(message, writer) {
            return this.encode(message, writer).ldelim();
        };

        /**
         * Decodes a fish_match_leave_room_req message from the specified reader or buffer.
         * @function decode
         * @memberof fish_match.fish_match_leave_room_req
         * @static
         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
         * @param {number} [length] Message length if known beforehand
         * @returns {fish_match.fish_match_leave_room_req} fish_match_leave_room_req
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        fish_match_leave_room_req.decode = function decode(reader, length) {
            if (!(reader instanceof $Reader))
                reader = $Reader.create(reader);
            var end = length === undefined ? reader.len : reader.pos + length, message = new $root.fish_match.fish_match_leave_room_req();
            while (reader.pos < end) {
                var tag = reader.uint32();
                switch (tag >>> 3) {
                default:
                    reader.skipType(tag & 7);
                    break;
                }
            }
            return message;
        };

        /**
         * Decodes a fish_match_leave_room_req message from the specified reader or buffer, length delimited.
         * @function decodeDelimited
         * @memberof fish_match.fish_match_leave_room_req
         * @static
         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
         * @returns {fish_match.fish_match_leave_room_req} fish_match_leave_room_req
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        fish_match_leave_room_req.decodeDelimited = function decodeDelimited(reader) {
            if (!(reader instanceof $Reader))
                reader = new $Reader(reader);
            return this.decode(reader, reader.uint32());
        };

        /**
         * Verifies a fish_match_leave_room_req message.
         * @function verify
         * @memberof fish_match.fish_match_leave_room_req
         * @static
         * @param {Object.<string,*>} message Plain object to verify
         * @returns {string|null} `null` if valid, otherwise the reason why it is not
         */
        fish_match_leave_room_req.verify = function verify(message) {
            if (typeof message !== "object" || message === null)
                return "object expected";
            return null;
        };

        /**
         * Creates a fish_match_leave_room_req message from a plain object. Also converts values to their respective internal types.
         * @function fromObject
         * @memberof fish_match.fish_match_leave_room_req
         * @static
         * @param {Object.<string,*>} object Plain object
         * @returns {fish_match.fish_match_leave_room_req} fish_match_leave_room_req
         */
        fish_match_leave_room_req.fromObject = function fromObject(object) {
            if (object instanceof $root.fish_match.fish_match_leave_room_req)
                return object;
            return new $root.fish_match.fish_match_leave_room_req();
        };

        /**
         * Creates a plain object from a fish_match_leave_room_req message. Also converts values to other types if specified.
         * @function toObject
         * @memberof fish_match.fish_match_leave_room_req
         * @static
         * @param {fish_match.fish_match_leave_room_req} message fish_match_leave_room_req
         * @param {$protobuf.IConversionOptions} [options] Conversion options
         * @returns {Object.<string,*>} Plain object
         */
        fish_match_leave_room_req.toObject = function toObject() {
            return {};
        };

        /**
         * Converts this fish_match_leave_room_req to JSON.
         * @function toJSON
         * @memberof fish_match.fish_match_leave_room_req
         * @instance
         * @returns {Object.<string,*>} JSON object
         */
        fish_match_leave_room_req.prototype.toJSON = function toJSON() {
            return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
        };

        return fish_match_leave_room_req;
    })();

    fish_match.fish_match_enter_rsp = (function() {

        /**
         * Properties of a fish_match_enter_rsp.
         * @memberof fish_match
         * @interface Ifish_match_enter_rsp
         * @property {number|null} [nStatus] fish_match_enter_rsp nStatus
         */

        /**
         * Constructs a new fish_match_enter_rsp.
         * @memberof fish_match
         * @classdesc Represents a fish_match_enter_rsp.
         * @implements Ifish_match_enter_rsp
         * @constructor
         * @param {fish_match.Ifish_match_enter_rsp=} [properties] Properties to set
         */
        function fish_match_enter_rsp(properties) {
            if (properties)
                for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                    if (properties[keys[i]] != null)
                        this[keys[i]] = properties[keys[i]];
        }

        /**
         * fish_match_enter_rsp nStatus.
         * @member {number} nStatus
         * @memberof fish_match.fish_match_enter_rsp
         * @instance
         */
        fish_match_enter_rsp.prototype.nStatus = 0;

        /**
         * Creates a new fish_match_enter_rsp instance using the specified properties.
         * @function create
         * @memberof fish_match.fish_match_enter_rsp
         * @static
         * @param {fish_match.Ifish_match_enter_rsp=} [properties] Properties to set
         * @returns {fish_match.fish_match_enter_rsp} fish_match_enter_rsp instance
         */
        fish_match_enter_rsp.create = function create(properties) {
            return new fish_match_enter_rsp(properties);
        };

        /**
         * Encodes the specified fish_match_enter_rsp message. Does not implicitly {@link fish_match.fish_match_enter_rsp.verify|verify} messages.
         * @function encode
         * @memberof fish_match.fish_match_enter_rsp
         * @static
         * @param {fish_match.Ifish_match_enter_rsp} message fish_match_enter_rsp message or plain object to encode
         * @param {$protobuf.Writer} [writer] Writer to encode to
         * @returns {$protobuf.Writer} Writer
         */
        fish_match_enter_rsp.encode = function encode(message, writer) {
            if (!writer)
                writer = $Writer.create();
            if (message.nStatus != null && message.hasOwnProperty("nStatus"))
                writer.uint32(/* id 1, wireType 0 =*/8).int32(message.nStatus);
            return writer;
        };

        /**
         * Encodes the specified fish_match_enter_rsp message, length delimited. Does not implicitly {@link fish_match.fish_match_enter_rsp.verify|verify} messages.
         * @function encodeDelimited
         * @memberof fish_match.fish_match_enter_rsp
         * @static
         * @param {fish_match.Ifish_match_enter_rsp} message fish_match_enter_rsp message or plain object to encode
         * @param {$protobuf.Writer} [writer] Writer to encode to
         * @returns {$protobuf.Writer} Writer
         */
        fish_match_enter_rsp.encodeDelimited = function encodeDelimited(message, writer) {
            return this.encode(message, writer).ldelim();
        };

        /**
         * Decodes a fish_match_enter_rsp message from the specified reader or buffer.
         * @function decode
         * @memberof fish_match.fish_match_enter_rsp
         * @static
         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
         * @param {number} [length] Message length if known beforehand
         * @returns {fish_match.fish_match_enter_rsp} fish_match_enter_rsp
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        fish_match_enter_rsp.decode = function decode(reader, length) {
            if (!(reader instanceof $Reader))
                reader = $Reader.create(reader);
            var end = length === undefined ? reader.len : reader.pos + length, message = new $root.fish_match.fish_match_enter_rsp();
            while (reader.pos < end) {
                var tag = reader.uint32();
                switch (tag >>> 3) {
                case 1:
                    message.nStatus = reader.int32();
                    break;
                default:
                    reader.skipType(tag & 7);
                    break;
                }
            }
            return message;
        };

        /**
         * Decodes a fish_match_enter_rsp message from the specified reader or buffer, length delimited.
         * @function decodeDelimited
         * @memberof fish_match.fish_match_enter_rsp
         * @static
         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
         * @returns {fish_match.fish_match_enter_rsp} fish_match_enter_rsp
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        fish_match_enter_rsp.decodeDelimited = function decodeDelimited(reader) {
            if (!(reader instanceof $Reader))
                reader = new $Reader(reader);
            return this.decode(reader, reader.uint32());
        };

        /**
         * Verifies a fish_match_enter_rsp message.
         * @function verify
         * @memberof fish_match.fish_match_enter_rsp
         * @static
         * @param {Object.<string,*>} message Plain object to verify
         * @returns {string|null} `null` if valid, otherwise the reason why it is not
         */
        fish_match_enter_rsp.verify = function verify(message) {
            if (typeof message !== "object" || message === null)
                return "object expected";
            if (message.nStatus != null && message.hasOwnProperty("nStatus"))
                if (!$util.isInteger(message.nStatus))
                    return "nStatus: integer expected";
            return null;
        };

        /**
         * Creates a fish_match_enter_rsp message from a plain object. Also converts values to their respective internal types.
         * @function fromObject
         * @memberof fish_match.fish_match_enter_rsp
         * @static
         * @param {Object.<string,*>} object Plain object
         * @returns {fish_match.fish_match_enter_rsp} fish_match_enter_rsp
         */
        fish_match_enter_rsp.fromObject = function fromObject(object) {
            if (object instanceof $root.fish_match.fish_match_enter_rsp)
                return object;
            var message = new $root.fish_match.fish_match_enter_rsp();
            if (object.nStatus != null)
                message.nStatus = object.nStatus | 0;
            return message;
        };

        /**
         * Creates a plain object from a fish_match_enter_rsp message. Also converts values to other types if specified.
         * @function toObject
         * @memberof fish_match.fish_match_enter_rsp
         * @static
         * @param {fish_match.fish_match_enter_rsp} message fish_match_enter_rsp
         * @param {$protobuf.IConversionOptions} [options] Conversion options
         * @returns {Object.<string,*>} Plain object
         */
        fish_match_enter_rsp.toObject = function toObject(message, options) {
            if (!options)
                options = {};
            var object = {};
            if (options.defaults)
                object.nStatus = 0;
            if (message.nStatus != null && message.hasOwnProperty("nStatus"))
                object.nStatus = message.nStatus;
            return object;
        };

        /**
         * Converts this fish_match_enter_rsp to JSON.
         * @function toJSON
         * @memberof fish_match.fish_match_enter_rsp
         * @instance
         * @returns {Object.<string,*>} JSON object
         */
        fish_match_enter_rsp.prototype.toJSON = function toJSON() {
            return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
        };

        return fish_match_enter_rsp;
    })();

    return fish_match;
})();

$root.form = (function() {

    /**
     * Namespace form.
     * @exports form
     * @namespace
     */
    var form = {};

    form.submit_form = (function() {

        /**
         * Properties of a submit_form.
         * @memberof form
         * @interface Isubmit_form
         * @property {string} sendStr submit_form sendStr
         */

        /**
         * Constructs a new submit_form.
         * @memberof form
         * @classdesc Represents a submit_form.
         * @implements Isubmit_form
         * @constructor
         * @param {form.Isubmit_form=} [properties] Properties to set
         */
        function submit_form(properties) {
            if (properties)
                for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                    if (properties[keys[i]] != null)
                        this[keys[i]] = properties[keys[i]];
        }

        /**
         * submit_form sendStr.
         * @member {string} sendStr
         * @memberof form.submit_form
         * @instance
         */
        submit_form.prototype.sendStr = "";

        /**
         * Creates a new submit_form instance using the specified properties.
         * @function create
         * @memberof form.submit_form
         * @static
         * @param {form.Isubmit_form=} [properties] Properties to set
         * @returns {form.submit_form} submit_form instance
         */
        submit_form.create = function create(properties) {
            return new submit_form(properties);
        };

        /**
         * Encodes the specified submit_form message. Does not implicitly {@link form.submit_form.verify|verify} messages.
         * @function encode
         * @memberof form.submit_form
         * @static
         * @param {form.Isubmit_form} message submit_form message or plain object to encode
         * @param {$protobuf.Writer} [writer] Writer to encode to
         * @returns {$protobuf.Writer} Writer
         */
        submit_form.encode = function encode(message, writer) {
            if (!writer)
                writer = $Writer.create();
            writer.uint32(/* id 1, wireType 2 =*/10).string(message.sendStr);
            return writer;
        };

        /**
         * Encodes the specified submit_form message, length delimited. Does not implicitly {@link form.submit_form.verify|verify} messages.
         * @function encodeDelimited
         * @memberof form.submit_form
         * @static
         * @param {form.Isubmit_form} message submit_form message or plain object to encode
         * @param {$protobuf.Writer} [writer] Writer to encode to
         * @returns {$protobuf.Writer} Writer
         */
        submit_form.encodeDelimited = function encodeDelimited(message, writer) {
            return this.encode(message, writer).ldelim();
        };

        /**
         * Decodes a submit_form message from the specified reader or buffer.
         * @function decode
         * @memberof form.submit_form
         * @static
         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
         * @param {number} [length] Message length if known beforehand
         * @returns {form.submit_form} submit_form
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        submit_form.decode = function decode(reader, length) {
            if (!(reader instanceof $Reader))
                reader = $Reader.create(reader);
            var end = length === undefined ? reader.len : reader.pos + length, message = new $root.form.submit_form();
            while (reader.pos < end) {
                var tag = reader.uint32();
                switch (tag >>> 3) {
                case 1:
                    message.sendStr = reader.string();
                    break;
                default:
                    reader.skipType(tag & 7);
                    break;
                }
            }
            if (!message.hasOwnProperty("sendStr"))
                throw $util.ProtocolError("missing required 'sendStr'", { instance: message });
            return message;
        };

        /**
         * Decodes a submit_form message from the specified reader or buffer, length delimited.
         * @function decodeDelimited
         * @memberof form.submit_form
         * @static
         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
         * @returns {form.submit_form} submit_form
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        submit_form.decodeDelimited = function decodeDelimited(reader) {
            if (!(reader instanceof $Reader))
                reader = new $Reader(reader);
            return this.decode(reader, reader.uint32());
        };

        /**
         * Verifies a submit_form message.
         * @function verify
         * @memberof form.submit_form
         * @static
         * @param {Object.<string,*>} message Plain object to verify
         * @returns {string|null} `null` if valid, otherwise the reason why it is not
         */
        submit_form.verify = function verify(message) {
            if (typeof message !== "object" || message === null)
                return "object expected";
            if (!$util.isString(message.sendStr))
                return "sendStr: string expected";
            return null;
        };

        /**
         * Creates a submit_form message from a plain object. Also converts values to their respective internal types.
         * @function fromObject
         * @memberof form.submit_form
         * @static
         * @param {Object.<string,*>} object Plain object
         * @returns {form.submit_form} submit_form
         */
        submit_form.fromObject = function fromObject(object) {
            if (object instanceof $root.form.submit_form)
                return object;
            var message = new $root.form.submit_form();
            if (object.sendStr != null)
                message.sendStr = String(object.sendStr);
            return message;
        };

        /**
         * Creates a plain object from a submit_form message. Also converts values to other types if specified.
         * @function toObject
         * @memberof form.submit_form
         * @static
         * @param {form.submit_form} message submit_form
         * @param {$protobuf.IConversionOptions} [options] Conversion options
         * @returns {Object.<string,*>} Plain object
         */
        submit_form.toObject = function toObject(message, options) {
            if (!options)
                options = {};
            var object = {};
            if (options.defaults)
                object.sendStr = "";
            if (message.sendStr != null && message.hasOwnProperty("sendStr"))
                object.sendStr = message.sendStr;
            return object;
        };

        /**
         * Converts this submit_form to JSON.
         * @function toJSON
         * @memberof form.submit_form
         * @instance
         * @returns {Object.<string,*>} JSON object
         */
        submit_form.prototype.toJSON = function toJSON() {
            return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
        };

        return submit_form;
    })();

    form.submit_form_resp = (function() {

        /**
         * Properties of a submit_form_resp.
         * @memberof form
         * @interface Isubmit_form_resp
         * @property {number} status submit_form_resp status
         * @property {number|null} [code] submit_form_resp code
         * @property {string|null} [msg] submit_form_resp msg
         * @property {string} objStr submit_form_resp objStr
         */

        /**
         * Constructs a new submit_form_resp.
         * @memberof form
         * @classdesc Represents a submit_form_resp.
         * @implements Isubmit_form_resp
         * @constructor
         * @param {form.Isubmit_form_resp=} [properties] Properties to set
         */
        function submit_form_resp(properties) {
            if (properties)
                for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                    if (properties[keys[i]] != null)
                        this[keys[i]] = properties[keys[i]];
        }

        /**
         * submit_form_resp status.
         * @member {number} status
         * @memberof form.submit_form_resp
         * @instance
         */
        submit_form_resp.prototype.status = 0;

        /**
         * submit_form_resp code.
         * @member {number} code
         * @memberof form.submit_form_resp
         * @instance
         */
        submit_form_resp.prototype.code = 0;

        /**
         * submit_form_resp msg.
         * @member {string} msg
         * @memberof form.submit_form_resp
         * @instance
         */
        submit_form_resp.prototype.msg = "";

        /**
         * submit_form_resp objStr.
         * @member {string} objStr
         * @memberof form.submit_form_resp
         * @instance
         */
        submit_form_resp.prototype.objStr = "";

        /**
         * Creates a new submit_form_resp instance using the specified properties.
         * @function create
         * @memberof form.submit_form_resp
         * @static
         * @param {form.Isubmit_form_resp=} [properties] Properties to set
         * @returns {form.submit_form_resp} submit_form_resp instance
         */
        submit_form_resp.create = function create(properties) {
            return new submit_form_resp(properties);
        };

        /**
         * Encodes the specified submit_form_resp message. Does not implicitly {@link form.submit_form_resp.verify|verify} messages.
         * @function encode
         * @memberof form.submit_form_resp
         * @static
         * @param {form.Isubmit_form_resp} message submit_form_resp message or plain object to encode
         * @param {$protobuf.Writer} [writer] Writer to encode to
         * @returns {$protobuf.Writer} Writer
         */
        submit_form_resp.encode = function encode(message, writer) {
            if (!writer)
                writer = $Writer.create();
            writer.uint32(/* id 1, wireType 0 =*/8).int32(message.status);
            if (message.code != null && message.hasOwnProperty("code"))
                writer.uint32(/* id 2, wireType 0 =*/16).int32(message.code);
            if (message.msg != null && message.hasOwnProperty("msg"))
                writer.uint32(/* id 3, wireType 2 =*/26).string(message.msg);
            writer.uint32(/* id 4, wireType 2 =*/34).string(message.objStr);
            return writer;
        };

        /**
         * Encodes the specified submit_form_resp message, length delimited. Does not implicitly {@link form.submit_form_resp.verify|verify} messages.
         * @function encodeDelimited
         * @memberof form.submit_form_resp
         * @static
         * @param {form.Isubmit_form_resp} message submit_form_resp message or plain object to encode
         * @param {$protobuf.Writer} [writer] Writer to encode to
         * @returns {$protobuf.Writer} Writer
         */
        submit_form_resp.encodeDelimited = function encodeDelimited(message, writer) {
            return this.encode(message, writer).ldelim();
        };

        /**
         * Decodes a submit_form_resp message from the specified reader or buffer.
         * @function decode
         * @memberof form.submit_form_resp
         * @static
         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
         * @param {number} [length] Message length if known beforehand
         * @returns {form.submit_form_resp} submit_form_resp
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        submit_form_resp.decode = function decode(reader, length) {
            if (!(reader instanceof $Reader))
                reader = $Reader.create(reader);
            var end = length === undefined ? reader.len : reader.pos + length, message = new $root.form.submit_form_resp();
            while (reader.pos < end) {
                var tag = reader.uint32();
                switch (tag >>> 3) {
                case 1:
                    message.status = reader.int32();
                    break;
                case 2:
                    message.code = reader.int32();
                    break;
                case 3:
                    message.msg = reader.string();
                    break;
                case 4:
                    message.objStr = reader.string();
                    break;
                default:
                    reader.skipType(tag & 7);
                    break;
                }
            }
            if (!message.hasOwnProperty("status"))
                throw $util.ProtocolError("missing required 'status'", { instance: message });
            if (!message.hasOwnProperty("objStr"))
                throw $util.ProtocolError("missing required 'objStr'", { instance: message });
            return message;
        };

        /**
         * Decodes a submit_form_resp message from the specified reader or buffer, length delimited.
         * @function decodeDelimited
         * @memberof form.submit_form_resp
         * @static
         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
         * @returns {form.submit_form_resp} submit_form_resp
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        submit_form_resp.decodeDelimited = function decodeDelimited(reader) {
            if (!(reader instanceof $Reader))
                reader = new $Reader(reader);
            return this.decode(reader, reader.uint32());
        };

        /**
         * Verifies a submit_form_resp message.
         * @function verify
         * @memberof form.submit_form_resp
         * @static
         * @param {Object.<string,*>} message Plain object to verify
         * @returns {string|null} `null` if valid, otherwise the reason why it is not
         */
        submit_form_resp.verify = function verify(message) {
            if (typeof message !== "object" || message === null)
                return "object expected";
            if (!$util.isInteger(message.status))
                return "status: integer expected";
            if (message.code != null && message.hasOwnProperty("code"))
                if (!$util.isInteger(message.code))
                    return "code: integer expected";
            if (message.msg != null && message.hasOwnProperty("msg"))
                if (!$util.isString(message.msg))
                    return "msg: string expected";
            if (!$util.isString(message.objStr))
                return "objStr: string expected";
            return null;
        };

        /**
         * Creates a submit_form_resp message from a plain object. Also converts values to their respective internal types.
         * @function fromObject
         * @memberof form.submit_form_resp
         * @static
         * @param {Object.<string,*>} object Plain object
         * @returns {form.submit_form_resp} submit_form_resp
         */
        submit_form_resp.fromObject = function fromObject(object) {
            if (object instanceof $root.form.submit_form_resp)
                return object;
            var message = new $root.form.submit_form_resp();
            if (object.status != null)
                message.status = object.status | 0;
            if (object.code != null)
                message.code = object.code | 0;
            if (object.msg != null)
                message.msg = String(object.msg);
            if (object.objStr != null)
                message.objStr = String(object.objStr);
            return message;
        };

        /**
         * Creates a plain object from a submit_form_resp message. Also converts values to other types if specified.
         * @function toObject
         * @memberof form.submit_form_resp
         * @static
         * @param {form.submit_form_resp} message submit_form_resp
         * @param {$protobuf.IConversionOptions} [options] Conversion options
         * @returns {Object.<string,*>} Plain object
         */
        submit_form_resp.toObject = function toObject(message, options) {
            if (!options)
                options = {};
            var object = {};
            if (options.defaults) {
                object.status = 0;
                object.code = 0;
                object.msg = "";
                object.objStr = "";
            }
            if (message.status != null && message.hasOwnProperty("status"))
                object.status = message.status;
            if (message.code != null && message.hasOwnProperty("code"))
                object.code = message.code;
            if (message.msg != null && message.hasOwnProperty("msg"))
                object.msg = message.msg;
            if (message.objStr != null && message.hasOwnProperty("objStr"))
                object.objStr = message.objStr;
            return object;
        };

        /**
         * Converts this submit_form_resp to JSON.
         * @function toJSON
         * @memberof form.submit_form_resp
         * @instance
         * @returns {Object.<string,*>} JSON object
         */
        submit_form_resp.prototype.toJSON = function toJSON() {
            return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
        };

        return submit_form_resp;
    })();

    form.form_notify = (function() {

        /**
         * Properties of a form_notify.
         * @memberof form
         * @interface Iform_notify
         * @property {string} objStr form_notify objStr
         */

        /**
         * Constructs a new form_notify.
         * @memberof form
         * @classdesc Represents a form_notify.
         * @implements Iform_notify
         * @constructor
         * @param {form.Iform_notify=} [properties] Properties to set
         */
        function form_notify(properties) {
            if (properties)
                for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                    if (properties[keys[i]] != null)
                        this[keys[i]] = properties[keys[i]];
        }

        /**
         * form_notify objStr.
         * @member {string} objStr
         * @memberof form.form_notify
         * @instance
         */
        form_notify.prototype.objStr = "";

        /**
         * Creates a new form_notify instance using the specified properties.
         * @function create
         * @memberof form.form_notify
         * @static
         * @param {form.Iform_notify=} [properties] Properties to set
         * @returns {form.form_notify} form_notify instance
         */
        form_notify.create = function create(properties) {
            return new form_notify(properties);
        };

        /**
         * Encodes the specified form_notify message. Does not implicitly {@link form.form_notify.verify|verify} messages.
         * @function encode
         * @memberof form.form_notify
         * @static
         * @param {form.Iform_notify} message form_notify message or plain object to encode
         * @param {$protobuf.Writer} [writer] Writer to encode to
         * @returns {$protobuf.Writer} Writer
         */
        form_notify.encode = function encode(message, writer) {
            if (!writer)
                writer = $Writer.create();
            writer.uint32(/* id 1, wireType 2 =*/10).string(message.objStr);
            return writer;
        };

        /**
         * Encodes the specified form_notify message, length delimited. Does not implicitly {@link form.form_notify.verify|verify} messages.
         * @function encodeDelimited
         * @memberof form.form_notify
         * @static
         * @param {form.Iform_notify} message form_notify message or plain object to encode
         * @param {$protobuf.Writer} [writer] Writer to encode to
         * @returns {$protobuf.Writer} Writer
         */
        form_notify.encodeDelimited = function encodeDelimited(message, writer) {
            return this.encode(message, writer).ldelim();
        };

        /**
         * Decodes a form_notify message from the specified reader or buffer.
         * @function decode
         * @memberof form.form_notify
         * @static
         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
         * @param {number} [length] Message length if known beforehand
         * @returns {form.form_notify} form_notify
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        form_notify.decode = function decode(reader, length) {
            if (!(reader instanceof $Reader))
                reader = $Reader.create(reader);
            var end = length === undefined ? reader.len : reader.pos + length, message = new $root.form.form_notify();
            while (reader.pos < end) {
                var tag = reader.uint32();
                switch (tag >>> 3) {
                case 1:
                    message.objStr = reader.string();
                    break;
                default:
                    reader.skipType(tag & 7);
                    break;
                }
            }
            if (!message.hasOwnProperty("objStr"))
                throw $util.ProtocolError("missing required 'objStr'", { instance: message });
            return message;
        };

        /**
         * Decodes a form_notify message from the specified reader or buffer, length delimited.
         * @function decodeDelimited
         * @memberof form.form_notify
         * @static
         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
         * @returns {form.form_notify} form_notify
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        form_notify.decodeDelimited = function decodeDelimited(reader) {
            if (!(reader instanceof $Reader))
                reader = new $Reader(reader);
            return this.decode(reader, reader.uint32());
        };

        /**
         * Verifies a form_notify message.
         * @function verify
         * @memberof form.form_notify
         * @static
         * @param {Object.<string,*>} message Plain object to verify
         * @returns {string|null} `null` if valid, otherwise the reason why it is not
         */
        form_notify.verify = function verify(message) {
            if (typeof message !== "object" || message === null)
                return "object expected";
            if (!$util.isString(message.objStr))
                return "objStr: string expected";
            return null;
        };

        /**
         * Creates a form_notify message from a plain object. Also converts values to their respective internal types.
         * @function fromObject
         * @memberof form.form_notify
         * @static
         * @param {Object.<string,*>} object Plain object
         * @returns {form.form_notify} form_notify
         */
        form_notify.fromObject = function fromObject(object) {
            if (object instanceof $root.form.form_notify)
                return object;
            var message = new $root.form.form_notify();
            if (object.objStr != null)
                message.objStr = String(object.objStr);
            return message;
        };

        /**
         * Creates a plain object from a form_notify message. Also converts values to other types if specified.
         * @function toObject
         * @memberof form.form_notify
         * @static
         * @param {form.form_notify} message form_notify
         * @param {$protobuf.IConversionOptions} [options] Conversion options
         * @returns {Object.<string,*>} Plain object
         */
        form_notify.toObject = function toObject(message, options) {
            if (!options)
                options = {};
            var object = {};
            if (options.defaults)
                object.objStr = "";
            if (message.objStr != null && message.hasOwnProperty("objStr"))
                object.objStr = message.objStr;
            return object;
        };

        /**
         * Converts this form_notify to JSON.
         * @function toJSON
         * @memberof form.form_notify
         * @instance
         * @returns {Object.<string,*>} JSON object
         */
        form_notify.prototype.toJSON = function toJSON() {
            return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
        };

        return form_notify;
    })();

    return form;
})();

module.exports = $root;
