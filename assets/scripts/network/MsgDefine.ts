
//////////////////////////////////////////////////protobuf， 消息号到消息的映射

export const proto_id_map = {
	// "1000" : ["test","TestReq"],
	"1001" : ["form","submit_form"],									//通用表单
	"1002" : ["form","submit_form_resp"],								//表单回复
	"1003" : ["form","form_notify"],									//表单通知

	/////////////////////////////////////////////// 战斗中的消息 定义
	/////////////////////////////////匹配消息
	"110" : ["fish_match", "fish_match_enter_req"],						// 匹配进入的请求
	"111" : ["fish_match", "fish_match_enter_rsp"],						// 匹配进入的响应

	"202" : ["fish_match", "fish_match_leave_room_req"],				// 匹配离开的请求

	// 战斗消息
	"500" : ["fish_battle", "fish_battle_sync_roominfo_req"],			// 同步房间信息的请求
	"501" : ["fish_battle", "fish_battle_sync_roominfo_rsp"],			// 同步房间信息的响应
	"502" : ["fish_battle", "fish_notify"],								// 放鱼的通知
	"503" : ["fish_battle", "fire_bullet_req"],							// 子弹发射的请求
	"504" : ["fish_battle", "fire_bullet_notify"],						// 子弹发送的广播
	"505" : ["fish_battle", "bullet_and_fish_collision_req"],			// 鱼和子弹碰撞的请求
	"506" : ["fish_battle", "bullet_and_fish_collision_notify"],		// 鱼和子弹碰撞的广播
	"507" : ["fish_battle", "bullet_and_fish_collision_buff_notify"],	// 鱼和子弹碰撞的buff通知

	/////////////////////////////////////////////// 登陆的消息 定义
	"101" : ["fish_login", "fish_login_req"],							//登录请求
	"102" : ["fish_login", "fish_login_rsp"],							//登录回调
	"103" : ["fish_login", "heartbeat"],								//心跳请求
	"104" : ["fish_login", "heartbeat_ack"], 							//心跳回调
}	

//////////////////////////////////////////////////protobuf， 消息到消息号的映射
function getMsgCodesToMsg(){
	let newMap: object = {};
	for (let key in proto_id_map) {
		let dataString = proto_id_map[key][0] + "." + proto_id_map[key][1]
		newMap[dataString] = key;
	}
	return newMap;
}

export const proto_key_map = getMsgCodesToMsg()