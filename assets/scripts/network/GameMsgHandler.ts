// 处理战斗过程中的服务端消息派发操作
import {G} from "../common/Global";
import {NOTIFY_EVENTS} from "../common/EventDefine";
import {MSG_STRUCT_LIST} from "../common/ProtoMsgDefine";

export class GameMsgHandler {

	/**
	 * [syncRoomInfoResponse 同步房间信息的回调]
	 * @param {[type]} params [服务端传递过的参数]
	 */
	public static syncRoomInfoResponse(params) {
		G.app.logMgr.info("syncRoomInfoResponse", params);
		// 1. 同步桌子信息，数据项
		G.app.dataMgr.syncRoomInfoForDatas(params);
		// 2. 派发 消息进行UI层的刷新
		G.app.eventMgr.dispatchEvent(NOTIFY_EVENTS.GAME_NOTIFY_SYNC_ROOM_INFO);
	}

	/**
	 * [fishMatchEnterRsp 鱼的匹配返回]
	 * @param {[type]} params [服务端传递过来的参数]
	 */
	public static fishMatchEnterRsp(params) {
		G.app.eventMgr.dispatchEvent(NOTIFY_EVENTS.GAME_NOTIFY_MATCH_ENTER_RSP);
	}

	/**
	 * [pushFishNotify 放鱼通知]
	 * @param {[type]} params [鱼的参数]
	 */
	public static pushFishNotify(params) {
		G.app.logMgr.info("pushFishNotify", params);
		G.app.dataMgr.fishDataMgr.addFishDatas(params[MSG_STRUCT_LIST.FISH_NOTIFY.veFishList]);
	}

	/**
	 * [fireBulletNotify 开火通知]
	 * @param {[type]} params [开火通知参数]
	 */
	public static fireBulletNotify(params) {
		G.app.logMgr.info("fireBulletNotify", params);
		G.app.eventMgr.dispatchEvent(NOTIFY_EVENTS.GAME_NOTIFY_FIRE_BULLET_FOR_ALL, params);
	}

	/**
	 * [bulletAndFishCollisionDieNotify 鱼和子弹碰撞的通知]
	 * @param {[type]} params [通知参数]
	 */
	public static bulletAndFishCollisionNotify(params) {
		G.app.logMgr.info("bullet and fish collision notify", params);
		G.app.eventMgr.dispatchEvent(NOTIFY_EVENTS.GAME_NOTIFY_BULLET_AND_FISH_COLLISION, params);
	}

	/**
	 * [bulletAndFishCollisiontBuffNotify 鱼和子弹碰撞buff的通知]
	 * @param {[type]} params [buff通知的参数]
	 */
	public static bulletAndFishCollisiontBuffNotify(params) {
		G.app.logMgr.info("bullet and fish collision buff notify", params);
		G.app.eventMgr.dispatchEvent(NOTIFY_EVENTS.GAME_NOTIFY_BULLET_AND_FISH_COLLISION_BUFF, params);
	}
}
