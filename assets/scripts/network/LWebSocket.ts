// import { proto_msg_to_msgcodes } from './MsgDefine';
import {NOTIFY_EVENTS} from "../common/EventDefine";
import {handler} from "../tools/Utils";
import {G} from "../common/Global";
import * as Util from "../tools/Utils";
import * as Pb from  "./proto";
import {proto_key_map} from "./MsgDefine";
import {proto_id_map} from "./MsgDefine";
var $protobuf = protobuf;
enum SocketState 
{
    UnInit,
    Linking,
    Linked,
    Close,
}

class LWebSocket{
    // 变量定义
    private _ip: string;
    private _port: number;
    private _reader: any;
    private _socket: any;
    private _state : SocketState;
    private _msgdispatch: any;
    private _queue: any;
    private _working : boolean;

    constructor(){
        this._ip            = "";
        this._port          = 0;
        this._reader        = null;
        this._socket        = null;
        this._state         = SocketState.UnInit;
        this._msgdispatch   = null;
        this._queue         = null;
        this._working       = false;
    }

    init(){
        console.log("webSocket init");
        let self = this;
        this._reader = new FileReader();
        this._reader.addEventListener( "loadend", function(){
            let data = this._result;
            // let buffer = new ByteBuffer();
            // buffer.append( data )
            // buffer.flip();
            // let len = buffer.readShort();
            // let head = buffer.readString( len );
            // console.log("response text msg: " + head);
            // self._msgdispatch.dispatch( head, buffer.toBuffer() )
            self.domessage();
        });

        this._reader.addEventListener( "onerror", function(){
            // self.working = false;
            self.domessage();
        });

        this._queue = new Array();
        // this._msgdispatch = require("msgdispatch");
    }

    domessage() {
        if( this._reader.readyState != 1 ){
            if ( this._queue.length > 0 ){
                let data = this._queue.shift();
                this._reader.readAsArrayBuffer(data);
            }
        }
    }

    changeNumToFF(num_p:number){
        let num = (num_p >> 7) & 0x7f;
        return num
    }

    //执行回调 param是服务端原参数
    doCallBack(param){
        // let msgId = "1001"//param.slice(2,6)
        // cc.log("msgid===",msgId)
        // let param =Util.stringToUint8Array(param_rsp) //param.slice(6) string 解码方式弃用
        // cc.log("back data",param)
        // let tempArray = new ArrayBuffer( param.byteLength );
		// for (var i = 0; i < param.byteLength; i++) {
		// 	tempArray[i] = param[i]
		// }
		// cc.log("tempArray",tempArray)
		let recData = new DataView(param); 
        // cc.log("recData",recData)　
        // for (var i = 0; i < param.length; i++) {
		// 	recData.setUint8(i,param[i]);
		// }
        // let msglenTemp1:number = recData.getUint8(0);//解析包长 2byte,转化成7进制数
        // let msglenTemp2:number = recData.getUint8(1);//解析包长 2byte
        let msglen =  recData.getUint16(0)//msglenTemp1 *256 + msglenTemp2;
		// cc.log(msglen)

        // let msgIdtemp1:number = recData.getUint8(2);//解析协议号2byte,转化成7进制数
        // let msgIdtemp2:number = recData.getUint8(3);//解析协议号2byte
        let msgId = recData.getUint16(2)//msgIdtemp1 *256 + msgIdtemp2;
       
        // cc.log(msgId)

        let uint8ArrayNew = new Uint8Array( msglen );
        if (msglen !== param.byteLength-2){ 
            G.app.logMgr.info("recv error data and can not decode,len expect==" + msglen ,param)
            return
        }
		for (var i = 0; i < msglen-2; i++) {
			uint8ArrayNew[i] = recData.getUint8(4+i)//param[i+4]
		}
		// cc.log("decode uint8ArrayNew",uint8ArrayNew)
        let keyMap = proto_id_map[String(msgId)];
        if (!keyMap ){ 
            G.app.logMgr.info("recv error data do not have correct msgsid",msgId)
            return
        }
		let protokey = keyMap[0];
        let msgkey = keyMap[1];
		let decodeForm =  Pb[protokey][msgkey].decode(uint8ArrayNew);
        // console.log("decodeForm", decodeForm);

        //消息统一接口发出
        let newKey:string = protokey + "." + msgkey; // combine to lua type key xx.xx
        G.app.eventMgr.dispatchEvent(NOTIFY_EVENTS.NET_CALLBACK_RESP,newKey,decodeForm);
    }

    connect(ip: string, port: number, fnConnect: handler, fnError: handler) {
        let self = this;
        this._ip = ip;
        this._port = port;
        let ws = new WebSocket('ws://' + this._ip + ':' + this._port + '/wss');
        ws.binaryType = "arraybuffer";
        ws.onopen = function (event) {
            G.app.logMgr.info("Send Text WS was opened.");
            self._state = SocketState.Linked;
            if( fnConnect ){
                fnConnect.exec()
            }
        };
        ws.onmessage = function (event) {
            // G.app.logMgr.info("event.log", event.data);
            // console.log("response text msg: " + event.data);
            // self._queue.push( event.data );
            // if(self._queue.length > 0){
            //     // self.domessage();
            // }
            self.doCallBack(event.data);
        };
        ws.onerror = function (event) {
            G.app.logMgr.info("Send Text fired an error");
            self._state = SocketState.Close;
        };
        ws.onclose = function (event) {
            G.app.logMgr.info("WebSocket instance closed.");
            self._state = SocketState.Close;
            if(fnError){
                fnError.exec()
            }
        };
        this._socket = ws;
    }

    send(param: any){
        // let TestReq = G.app.pb.root.lookup("test.TestReq");

        // let encodeMsg = TestReq.encode(TestReq.create(buffer)).finish();
        // console.log("encodeMsg", encodeMsg);
        // let decodeMsg = TestReq.decode(encodeMsg);
        // console.log("decodeMsg", decodeMsg);
        this._socket.send(param);
    }

    /**
     * [sendTest description]
     */

    sendTest(buffer: any){
        cc.log(buffer)
       
        // cc.log(form)
        // let test:protoAll = new protoAll();
        // let TestReq = protoAll.form.FormTest.create(buffer);
        // let form = protoAll.form;
        // let formT = new protoAll.form();
        // let formT:formP;
        // cc.log(form)
        // let formT = new form.FormTest();
        // form.FormTest

        // cc.log(formT)
        // let testForm = form.FormTest.create(buffer);
        // console.log("testForm", testForm);
        // let encodeForm = form.FormTest.encode(testForm).finish();
        // console.log("encodeForm", encodeForm);
        // let decodeForm = form.FormTest.decode(encodeForm);
        // console.log("decodeForm", decodeForm);
        // // this._socket.send(decodeForm);
        // let testForm2 = form2.FormTest2.create(buffer);
        // console.log("testForm", testForm2);
        // let encodeForm2 = form2.FormTest2.encode(testForm).finish();
        // console.log("encodeForm", encodeForm2);
        // let decodeForm2 = form2.FormTest2.decode(encodeForm);
        // console.log("decodeForm", decodeForm2)
        // this._socket.send(buffer);
    }

    /**
     * [sendToServer 将消息发送给服务端]
     * @param {any}    msgData          [需要发送的表结构]
     * @param {string} msgStructureName [proto通信的表结构，默认为xxx]
     */
    // sendToServer(msgData: any, msgStructureName: string = "test.TestReq"){
    //     let msgCode = proto_msg_to_msgcodes[msgStructureName];
    //     if(!msgCode){
    //         console.log("msgCode not find, please confirm for the msgStructureName", msgStructureName);
    //         return;
    //     }
    //     if(this._socket && this._state == SocketState.Linked){
    //         let encodeMsg = this._encodeMsg(msgData, msgStructureName);
    //         this._socket.send(encodeMsg);
    //     }else{
    //         console.log("please confirm the network");
    //     }
    // }

    /**
     * [_encodeMsg 消息编码函数]
     * @param {any}    msgData          [需要发送的消息]
     * @param {string} msgStructureName [消息结构体名字]
     */
    private _encodeMsg(msgData: any, msgStructureName: string){
        // 这里需要改一下，通过通用接口来找到相应的接口
        let msgObject = G.app.pb.root.lookup(msgStructureName);
        let encodeMsg = msgObject.encode(msgObject.create(msgData)).finish();

        return encodeMsg;
    }
    //     local msgdata = protobuf.encode(msgname, data)
    // local size = #msgdata + 2
    // local package = string.pack(">h", size) .. string.pack(">h", msgid)  .. msgdata
    // return package

  
}

export default LWebSocket;