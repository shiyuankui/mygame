import {GameMsgHandler} from "./GameMsgHandler";
import {HallMsgHandler} from "./HallMsgHandler";

//key 　文件名 value 方法名
export const FuncMap = {
    //////////////////////// 大厅消息注册
    "system_data":                                         HallMsgHandler.setUserInfoData,             //用户数据通知设置                           
    "items_data":                                          HallMsgHandler.setUserBagData,              //背包数据通知设置
    "basic_custom_data":                                   HallMsgHandler.setUserCustomData,           //其他数据通知设置

    /////////////////////// 战斗消息注册
    "fish_battle.fish_battle_sync_roominfo_rsp":           GameMsgHandler.syncRoomInfoResponse,        // 同步房间信息的消息回调
    "fish_match.fish_match_enter_rsp":                     GameMsgHandler.fishMatchEnterRsp,           // 鱼的房间匹配返回
    "fish_battle.fish_notify":                             GameMsgHandler.pushFishNotify,              // 放鱼通知
    "fish_battle.fire_bullet_notify":                      GameMsgHandler.fireBulletNotify,            // 发射子弹的通知
    "fish_battle.bullet_and_fish_collision_notify":        GameMsgHandler.bulletAndFishCollisionNotify,    // 鱼和子弹碰撞死亡的通知
    "fish_battle.bullet_and_fish_collision_buff_notify":   GameMsgHandler.bulletAndFishCollisiontBuffNotify, // 鱼和子弹碰撞的buff通知
}



export class MsgDispatch{
    //dispatch head = FuncMap key, buffer = 传参
    dispatch(head, buffer){
        let func = FuncMap[head];
        if (func !== undefined) {// null
            func( buffer );
        }
    }
}

