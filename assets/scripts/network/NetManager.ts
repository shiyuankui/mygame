import { handler } from './../tools/Utils';
import {G} from "../common/Global";
import NetMgr from './LWebSocket';
import * as Util from "../tools/Utils";
import * as Pb from  "./proto";
import {proto_key_map} from "./MsgDefine";
import {proto_id_map} from "./MsgDefine";
import {NOTIFY_EVENTS} from "../common/EventDefine";
import {MsgDispatch} from "./MsgDispatch";

// *********************SAMPLE ********************************************

//提交通用表单 submitForm
// let func:string = "test";
// let param:any = {};
// param.key4 = "111";
// param.key2 = "333";
// G.app.netMgr.submitForm(func,param,gen_handler(this.testCallBack,this));

//注册监听 registerCallBack
// G.app.netMgr.registerCallBack("test",gen_handler(this.testCallBack,this))
// let func:string = "test";
// let param:any = {};
// param.key4 = "111";
// param.key2 = "333";
// G.app.netMgr.submitForm(func,param);

//提交pb内容 sendProtoMsg
// let func:string = "form2.FormTest2";
// let param:any = {};
// param.func = "111";
// param.param = "333";
// G.app.netMgr.sendProtoMsg(func,param,gen_handler(this.testCallBack,this));


enum LOG_LINK_STATUS{
	LINK_FAILED,
	LINK_SUCCESS,
	LINK_UNLINK
}

export class NetManager {
	constructor() {
    }
	public static readonly Instance: NetManager = new NetManager();
	private _status: LOG_LINK_STATUS = LOG_LINK_STATUS.LINK_UNLINK;
	private _host: string = "192.168.5.60";//loginManager 进行set操作，通过服务器获取
	private _port: number = 8001;//loginManager 进行set操作，通过服务器获取
	netMgr: any;
	dispatcher:any;
	serverCallBacks:object = {}; //Call back save here
	initConnectCb:any = null;
	get host(): string {
		return this._host;
	}

	set host(newHost : string) {
		this._host = newHost;
	}
	connect
	get port(): number {
		return this._port;
	}

	set port(newPort : number) {
		this._port = newPort;
	}
	
	init(callback?:handler){
		this.netMgr = new NetMgr();
		this.dispatcher = new MsgDispatch();
		this.netMgr.init();
		if (callback){
			this.initConnectCb = callback
		}

		this.netMgr.connect(this._host, this._port, Util.gen_handler(this.loadSuccess, this),Util.gen_handler(this.loadFaild, this), this);
		// EventMgr.addEventListener(NOTIFY_EVENTS.NET_CALLBACK_NOTIFY,Util.gen_handler(this.formResp, this))
		// G.app.eventMgr.addEventListener(NOTIFY_EVENTS.FORM_CALLBACK_RESP,Util.gen_handler(this.formResp, this))
		G.app.eventMgr.addEventListener(NOTIFY_EVENTS.NET_CALLBACK_RESP,Util.gen_handler(this.netResp, this))
	}
	
	sendInfo(param){
		this.netMgr.send(param)
		// this.netMgr.sendTest(param)
	}
	
    loadSuccess(){
		G.app.logMgr.info("connect success");
		if (this.initConnectCb)
		{
			this.initConnectCb.exec()
		}
    }

    loadFaild(){
		G.app.logMgr.info("connect failed");
		// 3s后尝试重新连接,弹出框后重连

		// setTimeout(function () {
		// 	net.netMgr.connect(net._host, net._port, Util.gen_handler(net.loadSuccess, net),Util.gen_handler(net.loadFaild, net), net);
		// }, 3000);
	}
	
	encodeForm(msgname:string,data:any){

	}

	//用proto发送信息 msg 消息名 data 内容 回调可省略
	sendProtoMsg(msg:string,data:any,callBack?:handler){
		//存在回调参数的时候，进行存储回调函数
		if(callBack){
			if (msg !== "form.submit_form"){
				this.serverCallBacks[msg] = callBack;
			}
		}
		let Str = this.encodeProtoStr(msg,data);
		this.sendInfo(Str);
	}
	//加码信息
	encodeProtoStr(msg:string,data:any){
		let Str = "";
		let msgId = proto_key_map[msg];
		// cc.log("jsondata",data)
		// cc.log("msg id ===========" + msg,msgId)
		if (!msgId){ G.app.logMgr.info("this msg do not have id ===",msg)}
		// cc.log(Pb)
		let keyMap = proto_id_map[msgId];
		let protokey = keyMap[0];
		let msgkey = keyMap[1];
		let pbForm = Pb[protokey][msgkey].create(data);
        let encodeForm = Pb[protokey][msgkey].encode(pbForm).finish();
		// console.log("encodeForm", encodeForm);
		// let strForm = Util.Uint8ArrayToString(encodeForm); sting编码方式，已弃用,占用流量太大,采用二进制的方式进行数据传输
		// cc.log("str====", encodeForm.length);
		let protoLen = encodeForm.length
		// let uint8Array = new Uint8Array( encodeForm.length + 6 ); 原来准备用Uint8a格式去发，后来发现可以用ArrayBuffer
		let sendArray = new ArrayBuffer( encodeForm.length + 4 );
		let dataView = new DataView(sendArray, 0, protoLen+4);//dataView进行数据发送格式控制，统一采用大端方式发送
		dataView.setUint16(0,protoLen+2,false)//2个字节 表示 包长
		dataView.setUint16(2,msgId,false)	//2个字节表示协议号
		for (var i = 0; i < encodeForm.length; i++) {
			dataView.setUint8(4+i,encodeForm[i]);
		}
		//下面是手动编码方式,手动编码可能会有格式标准化的问题,所以采用dataView进行格式标准化
		// uint8Array
		// cc.log("dataView",dataView)
		// for (var i = 0; i < 2; i++) {
		// 	uint8Array[i] = encodeForm.length >> (8 - i * 8);
		// }
		// for (var i = 0; i < 4; i++) {
		// 	uint8Array[i+2] = msgId >> (8 - i * 8);
		// }
		// return encodeForm

		// let tdata:any = {};
		// tdata.objStr = "{\"func\":\"system_data\",\"params\":{\"bet\":1,\"exp\":0,\"money\":0,\"telphone\":\"\",\"last_time\":1545036709,\"register_time\":1545036709,\"version\":\"versionCode\",\"bind_name\":\"123456\",\"id\":\"565021533784051714\",\"name\":\"getNickName\",\"month_card\":0,\"vip\":0}}"
		// tdata.objStr = "{\"fun\""

		// let pbForm2 = Pb["form"] ["form_notify"].create(tdata);
		// let encodeForm2 = Pb["form"] ["form_notify"].encode(pbForm2).finish();
		// cc.log("encodeForm2",encodeForm2)
		// cc.log("sendArray",sendArray)
		return sendArray
	}

	//手动注册回调
	registerCallBack(func: string,callBack:handler){
		this.serverCallBacks[func] = callBack;
	}
	//手动注销回调
	unregisterCallBack(func: string){
		this.serverCallBacks[func] = null;
	}
	//提交通用表单 func 函数名 param 参数
	submitForm(func: string, param: any, callBack?:handler ) {		
        let data:any = {};
        data.func = func;
		data.params = param;
		let jsonStr:string = JSON.stringify(data);
		let dataSend:any = {};
		dataSend.sendStr = jsonStr;
		if(callBack){
			this.serverCallBacks[data.func] = callBack;
		}
		this.sendProtoMsg("form.submit_form",dataSend);
    }
  
	//网络回调回复 msgName 表单名 resp 回复内容
	netResp(msgName:string,resp:any){
		G.app.logMgr.info("msgName===" + msgName,resp)
		//当返回proto为通用表单时进行解析,返回通用函数名和参数
		if (msgName== "form.submit_form_resp" || msgName== "form.form_notify"){
			// let respStr:string = resp
			// cc.log("netResp=====",resp.objStr)
			let jsStr = resp.objStr
			if (resp.status !== 0 && msgName== "form.submit_form_resp"){ 
				G.app.logMgr.info("net error status ==1")
				return
			}
			let isJson = Util.isJsonString(jsStr) //MAKE SURE THIS IS JSON STRING AND PARSE IT
			if (isJson)
			{
				let data = JSON.parse(jsStr)  // parse json to form
				let funcName:string = data.func;
				let params:string = data.params;
				if(this.serverCallBacks[funcName]){ //if type is form then excute callback use funcName as key
					this.serverCallBacks[funcName].exec(params);
				}else{
					this.dispatcher.dispatch(funcName,params);
				}
			}else{
				G.app.logMgr.info("ERROR:objStr is not json ",jsStr)
			}
		}else{
			if(this.serverCallBacks[msgName]){
				this.serverCallBacks[msgName].exec(resp)
			}
			else{
				this.dispatcher.dispatch(msgName,resp);
			}
		}	
	}
  	//通用表单回复
    // formResp(resp:any) {
    //     //decode
	// }
    // formNotify(notify:any){
    //     let data = notify // todo
	// 	// cc.log("notify",notify)
    //     //do CMD
    //     // CMD[data.func][data.param]

    //     //close waiting?
	// }
}

export const net = NetManager.Instance;