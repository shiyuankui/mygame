// 大厅模块消息派发
import {G} from "../common/Global";
import {NOTIFY_EVENTS} from "../common/EventDefine";
export class HallMsgHandler {



    public static setUserInfoData(params) {
			G.app.dataMgr.playerData.initPlayerInfo(params);
			// 2. 派发 消息进行UI层的刷新
			// G.app.eventMgr.dispatchEvent(NOTIFY_EVENTS.PLAYER_INFO_UPDATE_NOTIFY);
		}
    
    public static setUserBagData(params) {
			G.app.dataMgr.bagDataMgr.addItems(params);
			// 2. 派发 消息进行UI层的刷新
			// G.app.eventMgr.dispatchEvent(NOTIFY_EVENTS.PLAYER_BAG_UPDATE_NOTIFY);
		}

		public static setUserCustomData(params) {
				cc.log("custom",params)
			// G.app.dataMgr.bagData.addItems(params);
			// // 2. 派发 消息进行UI层的刷新
			// G.app.eventMgr.dispatchEvent(NOTIFY_EVENTS.PLAYER_BAG_UPDATE_NOTIFY);
		}
}
