import { G } from "../common/Global";

const {ccclass, property} = cc._decorator;
@ccclass
export default class EventComponent extends cc.Component {
    _handlers = {}
    onLoad () {}

    addEventListener(msg,callBack){
        G.app.eventMgr.addEventListener(msg, callBack);
        this._handlers[msg] = callBack
    }

    removeEventListener(msg,callBack?){
        if(callBack){
            G.app.eventMgr.removeEventListener(msg, callBack);
        }else{
            for (let i in this._handlers){
                if(msg == i){
                    G.app.eventMgr.removeEventListener(i, this._handlers[i]);
                }
            }
        } 
    }

    onDestroy(){
        for (let i in this._handlers){
            G.app.eventMgr.removeEventListener(i, this._handlers[i]);
        }
    }
}
