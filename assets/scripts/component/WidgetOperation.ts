const {ccclass, property} = cc._decorator;

@ccclass
export class WidgetOperation extends cc.Component {


	/**
	 * [onExit 关闭动画结束回调，销毁当前层]
	 */
	onExit() {
		G.app.destroyLayer();
	}

}
