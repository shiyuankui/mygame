import { NOTIFY_EVENTS } from "../common/EventDefine";


const {ccclass, property} = cc._decorator;

@ccclass
export default class testEntity extends cc.Component {

    onLoad () {
        this.init();
    }

    init(){
        this.node.addComponent("Enemy")
        this.node.addComponent("Property")
        this.node.addComponent("Move")

    }

    start () {

    }

    // update (dt) {}
}
