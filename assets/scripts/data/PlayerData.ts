import { CFG_SQL_TBL__USERINFO } from './../common/MysqlStructDefine';
// 记录当前玩家的数据，在玩家登陆 (断线重连)的时候进行初始化操作
import HHelpTools from "../tools/HHelpTool";

class PlayerData {
	public static readonly Instance = new PlayerData();

	_userID: any				= -1;	// 玩家当前的userID
	_curBatteryID: number 		= -1;	// 玩家炮台ID
	_curLevel: number			= -1;	// 玩家当前等级
	_curExp: number				= -1;	// 玩家当前经验
	_curDeposit: number			= -1;	// 当前银子数
	_curDiamond: number			= -1;	// 当前钻石数
	_curMoney:	number			= -1;	// 当前鱼币
	_curName: string			= "";	// 玩家名字

	// 初始化当前玩家的数据
	constructor() {
		this.initVarProperty();
	}

	initVarProperty() {
		HHelpTools.propertyFunc(PlayerData, "_userID", {readonly: true});
		HHelpTools.propertyFunc(PlayerData, "_curLevel");
		HHelpTools.propertyFunc(PlayerData, "_curExp");
	}

	initPlayerInfo(playerData) {
		this._userID 		= playerData[CFG_SQL_TBL__USERINFO.id];
		this._curMoney		= playerData[CFG_SQL_TBL__USERINFO.money];
		this._curExp		= playerData[CFG_SQL_TBL__USERINFO.exp];
		this._curName		= playerData[CFG_SQL_TBL__USERINFO.name];
	}

}

export const PlayerDT = PlayerData.Instance;
