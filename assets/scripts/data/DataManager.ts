import {PlayerDT} from "./PlayerData";
import HHelpTools from "../tools/HHelpTool";

class DataManager {
	public static readonly Instance = new DataManager();

	playerData: any			= null;


	constructor(){
		this.playerData	 = PlayerDT;

	}

	// 重置客户端数据，主要是在断线重连的时候进行数据重置
	reset() {

	}

	/**
	 * [syncRoomInfoForDatas 同步房间数据]
	 * @param {[type]} syncInfos [同步过来的数据，fish_battle.fish_battle_sync_roominfo_rsp结构]
	 */
	syncRoomInfoForDatas(syncInfos) {

	}


}

export const DataMgr = DataManager.Instance;
