// 事件管理器，负责事件的分发

import {NOTIFY_EVENTS} from "../common/EventDefine";
import {gen_handler} from "../tools/Utils"

class EventManager {
	public static readonly Instance = new EventManager();

	// 存储了事件到事件回调函数的映射
	public _eventsMap: any;

	constructor(){
		this._eventsMap = {};
	}

	/**
	 * [addEventListener 注册监听事件]
	 * @param {NOTIFY_EVENTS} aEventName [事件名，必须在NOTIFY_EVENTS中定义]
	 * @param {gen_handler}   aCallFunc  [事件回调函数]
	 */
	addEventListener(aEventName: NOTIFY_EVENTS, aCallFunc: gen_handler) {
		let callfuncSets = this._eventsMap[aEventName];
		if (!callfuncSets) {
			this._eventsMap[aEventName] = callfuncSets = [];
		}
		callfuncSets.push(aCallFunc);
	}

	/**
	 * [removeEventListener 移除监听事件]
	 * @param {NOTIFY_EVENTS} rEventName [事件名，必须在NOTIFY_EVENTS中定义]
	 * @param {gen_handler}   rCallFunc  [事件回调函数]
	 */
	removeEventListener(rEventName: NOTIFY_EVENTS, rCallFunc: gen_handler) {
		let callfuncSets = this._eventsMap[rEventName];
		if (!callfuncSets) {
			return;
		}
		let index = callfuncSets.indexof(rCallFunc);
		if (index < 0) {
			return;
		}
		callfuncSets.splice(index, 1);
	}

	/**
	 * [dispachEvent 事件派发]
	 * @param {NOTIFY_EVENTS} dEventName     [事件名，必须在NOTIFY_EVENTS中定义]
	 * @param {any[]}         ...dParams	 [事件回调传入的参数，参数可选]
	 */
	dispatchEvent(dEventName: NOTIFY_EVENTS, ...dParams: any[]) {
		let callfuncSets = this._eventsMap[dEventName];
		if (!callfuncSets) {
			return;
		}

		for (let index = 0; index < callfuncSets.length; ++ index) {
			let callFunc = callfuncSets[index];
			if (callFunc) {
				callFunc.exec(...dParams);
			}
		}
	}

}

export const EventMgr = EventManager.Instance;
