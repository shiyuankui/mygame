// 日志管理器，负责进行日志的打印

enum LOG_LINK_STATUS{
	LINK_FAILED,
	LINK_SUCCESS,
	LINK_UNLINK
}

class LogManager {
	public static readonly Instance: LogManager = new LogManager();

	private _status: LOG_LINK_STATUS = LOG_LINK_STATUS.LINK_UNLINK;

	constructor(){
		// 连接http服务器
		this._status = this._linkHttpServer();
	}

	/**
	 * [_linkHttpServer 连接日志服务器]
	 */
	private _linkHttpServer(){
		let curStatus = LOG_LINK_STATUS.LINK_UNLINK;

		return curStatus;
	}

	/**
	 * [info 打印正常信息]
	 * @param {any} iMsg   [消息格式]
	 * @param {any} iSubst [格式化参数]
	 */
	info(iMsg: any, iSubst?: any){
		cc.log(iMsg, iSubst);
	}

	/**
	 * [warn 打印警告信息]
	 * @param {any} iMsg   [消息格式]
	 * @param {any} iSubst [格式化参数]
	 */
	warn(iMsg: any, iSubst?: any){
		cc.warn(iMsg, iSubst);
	}

	/**
	 * [error 打印错误信息]
	 * @param {any} iMsg   [消息格式]
	 * @param {any} iSubst [格式化参数]
	 */
	error(iMsg: any, iSubst?: any){
		cc.error(iMsg, iSubst);
	}
}

export const LogMgr = LogManager.Instance;