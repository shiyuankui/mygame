
import {G} from "../common/Global";
import {NOTIFY_EVENTS} from "../common/EventDefine";
import HHelpTool from "../tools/HHelpTool";
const {ccclass, property} = cc._decorator;

@ccclass
class GameObjectManager extends cc.Component {
	public static readonly Instance = new GameObjectManager();

	// 存储数据，以eid为key，GameObject为value进行存储
	private _gameObjects: any = [];
	onLoad(){

	}

	addObject(gameObject) {
		let obj = this.createObject(gameObject)
		this._gameObjects.push(obj)
	}

	deleteObject(index) {
		this._gameObjects.slice(index,1)
	}


	createObject(data){
		let prefabLayer = HHelpTool.requirePrefabFile("game/NodeEntity");
		let cardScript =  prefabLayer.getComponent("GameEntity");
		cardScript.initData(data)
		this.node.addChild(prefabLayer);
		// cardScript.setCardPos((i-0.5)*150,50)
		return prefabLayer
	}

	/**
	 * [getItemByID 通过物品ID查找物品]
	 * @param {[type]} eid [ID]
	 */
	getObjectByID(eid) {
		return this._gameObjects[eid];
	}


	doAiActions(){
		for( let i in this._gameObjects){
			let obj = this._gameObjects[i]
			this.doAiAction(obj);
		}
	}

	doAiAction(obj){
		let enemy = this.searchEnemy(obj);



	}

	countDistance(obj1,obj2){
		let property1 = obj1.getComponent("GameEntity")
		let property2 = obj2.getComponent("GameEntity")
		let dx = Math.abs(property1.px - property2.px) 
		let dy = Math.abs(property1.py - property2.py) 
		return dx + dy
	}


	searchEnemy(obj){
		let minDis = 9999;
		let owner = obj.getComponent("Property").ownership
		let objs = this.node.getComponentsInChildren("Property")
		let enemy = null
		for (let i in objs) {
			if (objs[i].ownership != owner){
				let dis = this.countDistance(obj,objs[i])
				if(minDis > dis){
					minDis = dis
					enemy = objs[i]
				}
			}
		}
		return enemy
	}



}

export const GameObjMgr = GameObjectManager.Instance;