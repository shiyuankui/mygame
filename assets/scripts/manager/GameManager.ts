import { GameObjMgr } from './GameObjectManager';
import {NOTIFY_EVENTS} from "../common/EventDefine";
import {gen_handler} from "../tools/Utils"
import { G } from "../common/Global";
import CardManager from './CardManager';
const {ccclass, property} = cc._decorator;

@ccclass
export class GameManager extends cc.Component {
	public static readonly Instance = new GameManager();

	private _turn;
	// 存储数据
	private _gameObjects: any = [];
	private _timeline:any = [];
	private _interval = 0.5;
	private _timeCount = 0;

	cardMgr = null;
	objectMgr = null;
	gameWorld = null;

	public get turn() {
        return this._turn;
    }

    public set turn(num: number) {
        this._turn = num;
	}
	
	public get timeline() {
        return this._timeline;
    }

    public set timeline(data:[]) {
        this._timeline = data;
	}
	onLoad(){
		this.initGame();
		this.registerListener();

	}

	registerListener(){
		G.app.eventMgr.addEventListener("HandToGraveyard",gen_handler(this.pushStep,this))
		G.app.eventMgr.addEventListener("HandToExiled",gen_handler(this.pushStep,this))
		G.app.eventMgr.addEventListener("HandToBattlefield",gen_handler(this.pushStep,this))
	}

	initGame(){
		this._turn = 1;
		G.app.setGameMgr(this);

		let CardLayer =  this.node.getChildByName("CardLayer");
        let CardMgr = CardLayer.getComponent("CardManager");
        this.cardMgr = CardMgr;

        let EntityLayer =  this.node.getChildByName("EntityLayer");
        let EntityMgr = EntityLayer.getComponent("GameObjectManager");
		this.objectMgr = EntityMgr;
		
		this.gameWorld = this.node.getComponent("GameScene")

	}


	onTurnEnd(){
		this._turn = this._turn + 1;
		G.app.eventMgr.dispatchEvent(NOTIFY_EVENTS.ON_TURN_END)

		this.createEnemy();
		this.doAiTurn();
		// this.node.dispatchEvent(new cc.Event.EventCustom(NOTIFY_EVENTS.ON_TURN_END, true))
		this.doTurnEnd();
	}

	doAiTurn(){
		this.objectMgr.doAiActions();
		
	}

	createEnemy(){
		let data = {px : 3 , py : 3,id:9}
		this.objectMgr.addObject(data)
	}


	
	pushStep(step){
		this._timeline.push(step)
	}

	clearGameData(){
		this._turn = 1;
	}

	// addGameEntity(id,data){
	// 	let param = {
	// 		eid : id,
	// 		data : data
	// 	}
	// 	GameObjMgr.addObject(param)
	// 	// this._gameObjects[id] = data
	// }

	// getGameEntityById(id){
	// 	return GameObjMgr.getObjectByID(id)
	// }

	doTurnEnd(){
		for(let i in this._gameObjects){
			let obj = this._gameObjects[i]
			// obj.getComponent("GameEntity").onTurnEnd();
			obj.onTurnEnd();
		}
	}

	update(dt){
		this._timeCount = this._timeCount + dt;
		if(this._timeline.length > 0 && this._timeCount > this._interval){
			this._timeCount = 0;
			let step = this._timeline.shift();
			this.dealStep(step)

		}
		
		

	}

	dealStep(step){
		cc.log("now step====",step)
		let FuncKey = step.msg
		this.gameWorld[FuncKey](step)

	}

}

export const GameMgr = GameManager.Instance;
