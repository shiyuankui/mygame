
import {G} from "../common/Global";
import {NOTIFY_EVENTS} from "../common/EventDefine";
import HHelpTool from "../tools/HHelpTool";
const {ccclass, property} = cc._decorator;

@ccclass
class EntityManager extends cc.Component {
	public static readonly Instance = new EntityManager();

	// 存储数据，以eid为key，GameObject为value进行存储
	private _entities: any = [];
	onLoad(){

	}

	addEntity(data) {
		let obj = this.createEntity(data)
		this._entities.push(obj)
	}

	deleteEntity(index) {
		this._entities.slice(index,1)
	}


	createEntity(data){
		// let prefabLayer = HHelpTool.requirePrefabFile("game/NodeEntity");
		// let cardScript =  prefabLayer.getComponent("GameEntity");
		// cardScript.initData(data)
		// this.node.addChild(prefabLayer);
		// cardScript.setCardPos((i-0.5)*150,50)
		let node = new cc.Node();
		this.node.addChild(node);
		return node
	}

	getEntity(eid) {
		return this._entities[eid];
	}


	doAiActions(){
		for( let i in this._entities){
			let obj = this._entities[i]
			this.doAiAction(obj);
		}
	}

	doAiAction(obj){
		let enemy = this.searchEnemy(obj);



	}

	countDistance(obj1,obj2){
		let property1 = obj1.getComponent("GameEntity")
		let property2 = obj2.getComponent("GameEntity")
		let dx = Math.abs(property1.px - property2.px) 
		let dy = Math.abs(property1.py - property2.py) 
		return dx + dy
	}


	searchEnemy(obj){
		let minDis = 9999;
		let owner = obj.getComponent("Property").ownership
		let objs = this.node.getComponentsInChildren("Property")
		let enemy = null
		for (let i in objs) {
			if (objs[i].ownership != owner){
				let dis = this.countDistance(obj,objs[i])
				if(minDis > dis){
					minDis = dis
					enemy = objs[i]
				}
			}
		}
		return enemy
	}



}

export const EntityMgr = EntityManager.Instance;