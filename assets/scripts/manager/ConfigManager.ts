import { ConfigCard } from './../configs/ConfigCard';
import { ConfigObject } from './../configs/ConfigObject';
// 配置表管理器，负责配置表的读取和管理
import {G} from "../common/Global";

class ConfigManager {
    public static readonly Instance: ConfigManager = new ConfigManager();

    CONFIG_TBL = {
        "ConfigObject" : ConfigObject,
        "ConfigCard" : ConfigCard,
    }

    constructor(){
    }

    getTable(configTblName: string){
        if (!this.CONFIG_TBL[configTblName]) {
            G.app.logMgr.err("not find the config table", configTblName);
            return null;
        }

        return this.CONFIG_TBL[configTblName];
    }

    /**/
    getConfigRecord(configTblName: string, key: any){
        let config = this.CONFIG_TBL[configTblName];
        if (!config) {
            G.app.logMgr.err("not find the config and key", configTblName, key);
        }
        return config[key];
    }

    getConfigField(configTblName: string, key: any, field: any){
        let record = this.getConfigRecord(configTblName, key);
        let value = null;
        if(record)
            value = record[field];
        return value;
    }

    getConfigsByField(configTblName: string, field: any,fieldValue:any){
        // let record = this.getConfigRecord(configTblName, key);
        let tbl = this.getTable(configTblName);
        let config : any[] =[];
        let i = 0;
        for(let vla in tbl){
            let are = tbl[vla];
            if(are[field] === fieldValue){
                config[i] = are;
                i++;
            }
        }
        // val: 当前值
        // idx：当前index
        // array: Array
        // tbl.forEach((val, idx, array) => {
        //     if (val[field] === fieldValue)
        //     {
        //         confing[idx] = val; 
        //     }
        // });
        return config;
        // let value = null;
        // if(record)
        //     value = record[field];
        // return value;
    }

}

export const ConfigMgr = ConfigManager.Instance;