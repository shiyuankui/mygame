import { LoginMgr } from './LoginManager';
const {ccclass, property} = cc._decorator;

import {net} from '../network/NetManager';
import {gen_handler} from "../tools/Utils";
import {EventMgr} from "./EventManager";
import {LogMgr} from "./LogManager";
import {ConfigMgr} from "./ConfigManager";
import {DataMgr} from "../data/DataManager";
import HHelpTools from "../tools/HHelpTool";

@ccclass
export class AppManager extends cc.Component {
	
	netMgr: any    = null;
	pb: any 	   = null;
	eventMgr:any   = null;
	dataMgr:any    = null;
	logMgr:any     = null;
	configMgr:any  = null;
	loginMgr:any   = null;
	gameMgr:any	   = null;


	_layerManager: any	= [];												// 弹窗层级管理器

	onLoad(){
		console.log("AppManager onLoad");
	
		// 初始化网络模块
			
		this.netMgr = net;

		// 初始化事件管理器
		this.eventMgr = EventMgr;

		// 日志管理器
		this.logMgr = LogMgr;

		// 配置表管理器
		this.configMgr = ConfigMgr;

		// 数据管理器
		this.dataMgr = DataMgr;
		// this.netMgr.init();

		this.loginMgr = LoginMgr;

		// this.gameMgr = GameMgr;
		
	}

	setGameMgr(gameMgr){
		this.gameMgr = gameMgr;
	}

	startLogin(){
		cc.director.loadScene("game");
	}

    loadSuccess(){
        console.log("connect success");
        // this.netMgr.send({tmpMsg: "hello world"});
        this.netMgr.sendTest("hello world");
        cc.loader.loadResDir("prefab");
        cc.director.loadScene("main");
    }

    loadFaild(){
        console.log("connect failed");

    }

    /**
     * [showMainScene 显示主场景，大厅界面]
     */
    showMainScene(){
    	// cc.director.loadScene("main");
    }

    /**
     * [showGameScene 显示游戏场景，战斗界面]
     */
    showGameScene(){
    	cc.director.loadScene("game");
    }

    /**
     * [showLoadingScene 显示loading场景]
     */
    showLoadingScene() {
    	cc.director.loadScene("loading");
    }

    /**
	 * [onStartGame 同步到桌子数据后返回]
	 * @param {[type]} params [战斗中所需的参数]
	 */
	onStartGame(params) {
		this.logMgr.info("on start game");

	}


	//////////////////////////////// 层级管理之用
	/**
	 * [pushLayer 加入层]
	 * @param {string} prefabFileName [prefab文件名]
	 * @param {string} actionNodeName [执行动作的节点名]
	 * @param {any}    nodeAction     [节点需要执行的动作]
	 */
	pushLayer(prefabFileName: string, actionNodeName: string = "SpriteBg", nodeAction?: any) {
		let prefabLayer = HHelpTools.requirePrefabFile(prefabFileName);
		this.node.addChild(prefabLayer);

		this._layerManager.push(prefabLayer);

		let actionNode = prefabLayer.getChildByName(actionNodeName);
		if (nodeAction) {
			actionNode.runAction(nodeAction);
		}else {
			HHelpTools.playPopinEffect(actionNode);
		}
	}


	/**
	 * [popLayer 弹出层]
	 */
	popLayer() {
		let curLayer = this._layerManager.pop();

		if (curLayer) {		
			this.node.removeChild(curLayer);
			// HHelpTools.playPopoutEffect(curLayer);
		}
	}

	/**
	 * [replaceLayer 替换层]
	 * @param {string}    prefabFileName [prefab文件名]
	 * @param {string =              "SpriteBg"}  actionNodeName [执行动作的节点名]
	 * @param {any}       nodeAction     [执行的动作]
	 */
	replaceLayer(prefabFileName: string, actionNodeName: string = "SpriteBg", nodeAction?: any) {
		this.popLayer();
		this.pushLayer(prefabFileName, actionNodeName, nodeAction);
	}

	popAllLayer() {
		if (this._layerManager) {
			while (this._layerManager.length > 0) {
				this.popLayer();
			}
		}
		this._layerManager = [];
	}
}
