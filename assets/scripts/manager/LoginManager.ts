import {G} from "../common/Global";
import { gen_handler } from "../tools/Utils";
import HHelpTool from "../tools/HHelpTool";

enum LOG_LINK_STATUS{
	LINK_FAILED,
	LINK_SUCCESS,
	LINK_UNLINK
};

const enum Define {
	HEART_DICONNECT_TIME = 600000,
	HEART_INTERVAL = 3000,
};

class LoginManager {
	public static readonly Instance: LoginManager = new LoginManager();
	private _status: LOG_LINK_STATUS = LOG_LINK_STATUS.LINK_UNLINK;
	private heart_interval:number = 3;
	private count:number = 0;
	private send_time:number = new Date().getTime();
	timer:any;
	reLoginRoom:number = 0;
	onLoad(){
		// cc.loader.loadResDir("prefab");
		cc.director.loadScene("main");
		// G.app.netMgr.connect(this._host, this._port, this.loadSuccess, this.loadFaild, this);
	}

	startLogin(){
		this.loginPlatformCB(0);
	}

	loginPlatformCB(code:number){
		//todo 同城游登录成功回调
		if (code === 0){
			this.getServerAddress();
		}
		if (code === 1){
			//fail
		}
	}

	getServerAddress(){
		//获取服务器ip port地址
	
		G.app.netMgr.host = "192.168.5.60";
		G.app.netMgr.port = 8002;
		// G.app.netMgr.host = "192.168.5.60";
		// G.app.netMgr.port = 8001;

		
		G.app.netMgr.init(gen_handler(this.loadSuccess,this));
		// G.app.netMgr.connect(this._host, this._port, this.loadSuccess, this.loadFaild, this);
	}

	loginServer(){

		G.app.netMgr.registerCallBack("fish_login.fish_login_rsp",gen_handler(this.loginServerResp,this))
		
	// self.loginCB = callback

		let pid = 1; //ios 2 other 1
		let info:any = {}; //obj send to server
		info.strTcyUserId = G.tempUserId; //"123456";//gUserPlugin:getUserID(),--绑定账号
		info.nChannelId   = 1;//SDKFunc.account:getPlatformChannel(),--//渠道ID，同城游为1 
		info.nOsType = pid;//系统类型，1表示安卓，2表示IOS
		info.strDeviceId = "getMacAddress";// gDeviceUtils:getMacAddress(),--//设备标识ID
		info.strNickName = "getNickName";//gUserPlugin:getNickName(),--//昵称
		info.nSex = 1;// gUserPlugin:getUserSex(),--//性别
		info.strVersion = "versionCode";//SDKFunc.versionCode,--//客户端当前版本号
		info.strImei = "getIMEI";//gDeviceUtils:getIMEI(),
		info.strImsi = "getIMSI";//gDeviceUtils:getIMSI(),
        // accounttype = -1,--//账号类型，-1表示未绑定，0表示qq,1表示微信
		// fromid = 6,-- //单包安卓6,ios 8,zip包安卓66,ios 88
	// if DEBUG_FLAG then
	// 	info.bindaccount = os.time()
	// end	
	// send_info("login.auth", info)

		let func:string = "fish_login.fish_login_req";
		G.app.netMgr.sendProtoMsg(func,info);
		// G.app.netMgr.connect(this._host, this._port, this.loadSuccess, this.loadFaild, this);
	}
	
	
	
    loadSuccess(){
        G.app.logMgr.info("login mgr connect success");
        // this.netMgr.send({tmpMsg: "hello world"});
        // G.app.netMgr.sendTest("hello world");
        // cc.loader.loadResDir("prefab");
		// cc.director.loadScene("main");
		this.loginServer();
    }

    loadFaild(){
        G.app.logMgr.info("connect failed");

        // this.netMgr.sendToServer({msg: "hello world"});
        // cc.loader.loadResDir("prefab");
        // this.showMainScene();
    }
	//fish_login_req 回调
	loginServerResp(param){
		// cc.log("loginServerResp roomm",param.nRoomId);
		if (param.nStatus === 0 ){
			this.sendHeart();
			this.reLoginRoom = param.nRoomId;
			cc.loader.loadResDir("prefab");
			cc.director.loadScene("main");
		}
	}
	//判断是否是断线重连进来的
	reLoginToBattle(){
		if (this.reLoginRoom !== 0){
			this.reLoginRoom = 0;
			return true;
		}
		else
		{
			return false;
		}
	}


 
	//心跳回调
	sendHeartResp(param){
		this.count = 0;
		//  = cc.director.getScheduler().schedule(huidiao, this, time)
	}
	//发送心跳
	sendHeart(){
		G.app.logMgr.info("sendHeart",this.send_time)
		let d_time = 0;
		if (this.send_time){
			d_time = new Date().getTime() - this.send_time;
		}

		if (d_time < Define.HEART_DICONNECT_TIME) 
		{
			this.startHeartScheduler();
			let func:string = "fish_login.heartbeat";
			let param:any = {};
			G.app.netMgr.sendProtoMsg(func,param);
		}
		else
		{
			this.send_time = null;
			G.app.logMgr.info("onHeartTime Reconnect",this.timer)
			// cc.director.getScheduler().unschedule(this.closeTimer, this);
			clearTimeout(this.timer)
			this.popReEnter()
		}
	}
	//打开发送心跳计时器
	startHeartScheduler(){
		// cc.director.getScheduler().schedule(this.onHeartTime, this, Define.HEART_INTERVAL);
		this.count = 0;
		this.send_time = new Date().getTime();
		G.app.netMgr.registerCallBack("fish_login.heartbeat_ack",gen_handler(this.sendHeartResp,this))
		this.countDown();
	}

	countDown() {
		this.timer = setTimeout(function () {
			LoginMgr.countDown();
			LoginMgr.onHeartTime();
		}, Define.HEART_INTERVAL);
		// G.app.logMgr.info("countDown",this.timer)
	};
	//心跳计时器执行内容
	onHeartTime(){
		// G.app.logMgr.info("onHeartTime",this.send_time)
		this.count = this.count + 1;
		if (this.count > 1) {
			G.app.logMgr.info("onHeartTime Reconnect",this.timer)
			// cc.director.getScheduler().unschedule(this.closeTimer, this);
			clearTimeout(this.timer)
			this.popReEnter()
		} else{
			let func:string = "fish_login.heartbeat";
			let param:any = {};
			G.app.netMgr.sendProtoMsg(func,param);
		}
		
	}
	//打开重连界面
	popReEnter(){
		G.app.logMgr.info("popReEnter",this.count)
		this.count = 0
		//跳转到重新登录
		// cc.director.loadScene("loading");
		// HHelpTool.showPopTips(1, gen_handler(this.onPressConfirm, this), gen_handler(this.onPressCancel, this));
	
		// cc.director.loadScene("loading");
	}

	onPressConfirm(){
		G.app.logMgr.info("press confirm");
		G.app.dataMgr.reset();
		cc.director.loadScene("loading");
    }

    onPressCancel(){
        G.app.logMgr.info("press cancel");
    }

}

export const LoginMgr = LoginManager.Instance;