
import HHelpTool from "../tools/HHelpTool";

const {ccclass, property} = cc._decorator;

@ccclass
export default class CardManager extends cc.Component {
	public static readonly Instance = new CardManager();

	cardData:any = [];

	onLoad(){
		this.initCards();
	}

	reset() {
		this.cardData = [];
	}
	
	initCards(){
		for(let i = 0; i < 5;i++ ){
			let card = this.createCard({id:(i+1),index:i})
			this.addCard(card)
		}
	}

	createCard(data){
		let prefabLayer = HHelpTool.requirePrefabFile("game/NodeSprite");
		// CardMgr.addCard(i,prefabLayer)
		// this.node.getChildByName("CardLayer").addChild(prefabLayer);
		let cardScript =  prefabLayer.getComponent("Card");
		cardScript.initData(data)
		this.node.addChild(prefabLayer);
		// cardScript.setCardPos((i-0.5)*150,50)
		return prefabLayer
	}

	updateCards(){

	}

	getCardCount(){
		return this.cardData.length;
	}

	addCard(card){
		this.cardData.push(card) ;
	}

	getCard(index){
		return this.cardData[index];
	}

	playCard(index){
		let card = this.getCard(index)
		this.cardData.splice(index, 1);
		card.parent = null;
		

		cc.log("this.cardData=",this.cardData)
	}

}

