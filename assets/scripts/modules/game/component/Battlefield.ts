import { G } from "../../../common/Global";

const {ccclass, property} = cc._decorator;

@ccclass
export default class Battlefield extends cc.Component {

    private _Change: string;

    public get Change() {
        return this._Change;
    }

    public set Change(name: string) {
        this._Change = name;
    }

    onLoad () {
        this._Change = "Graveyard"
    }

    start () {

    }

    onDestroy(){
       G.app.eventMgr.dispatchEvent("BattlefieldTo"+this._Change) 
    }


    // update (dt) {}
}