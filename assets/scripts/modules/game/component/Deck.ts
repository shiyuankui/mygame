import { G } from "../../../common/Global";

const {ccclass, property} = cc._decorator;

@ccclass
export default class Deck extends cc.Component {

    private _Change: string;

    public get Change() {
        return this._Change;
    }

    public set Change(name: string) {
        this._Change = name;
    }

    onLoad () {
        this._Change = "Hand"
    }

    start () {

    }

    onDestroy(){
       G.app.eventMgr.dispatchEvent("DeckTo"+this._Change) 
    }


    // update (dt) {}
}