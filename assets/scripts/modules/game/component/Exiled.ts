import { G } from "../../../common/Global";

const {ccclass, property} = cc._decorator;

@ccclass
export default class Exiled extends cc.Component {

    private _Change: string;

    public get Change() {
        return this._Change;
    }

    public set Change(name: string) {
        this._Change = name;
    }

    onLoad () {
        this._Change = "Exiled"
    }

    start () {

    }

    onDestroy(){
       G.app.eventMgr.dispatchEvent("ExiledTo"+this._Change) 
    }


    // update (dt) {}
}