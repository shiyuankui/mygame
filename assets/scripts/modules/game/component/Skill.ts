const {ccclass, property} = cc._decorator;

@ccclass
export default class Enemy extends cc.Component {

    onLoad () {
        
    }

    start () {

    }

    cast(){
        if(this.node.getComponent("Property")){
            let hp = this.node.getComponent("Property")
            hp.updateHp(-1)
            let move =  this.node.getComponent("Move")
            // G.app.eventMgr.dispatchEvent(NOTIFY_EVENTS.PLAYER_CANNON_DATA_NOTIFY);
            move.moveRight()
            
        }
        
    }


    // update (dt) {}
}