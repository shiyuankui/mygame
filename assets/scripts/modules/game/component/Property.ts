import { G } from "../../../common/Global";

const {ccclass, property} = cc._decorator;

@ccclass
export default class Property extends cc.Component {

    curHp   = 0;
    id      = 1;
    hpBase  = 0;
    atkBase = 0;
    hpMax   = 0;
    atk     = 0;
    ownership = 1;
    onLoad () {
        
    }

    start () {

    }

    initData(id){
        this.id = id
        this.hpBase =  G.app.configMgr.getConfigField("ConfigObject",this.id,"hp");
        this.atkBase =  G.app.configMgr.getConfigField("ConfigObject",this.id,"atk"); 
        this.ownership = G.app.configMgr.getConfigField("ConfigObject",this.id,"ownership"); 
        this.updateHpMax(this.hpBase);
        this.updateAtk(this.atkBase);
        this.curHp = this.hpBase;


        this.updateStatus();
    }

    updateStatus(){
        let card = this.node.getChildByName("SpriteIcon")
        let labAtk = card.getChildByName("LabelAttck")  
        labAtk.getComponent(cc.Label).string = String(this.atk);

        let labHp = card.getChildByName("LabelHp")  
        labHp.getComponent(cc.Label).string = String(this.curHp);
    }

    updateAtk(num){
        this.atk = num;
    }


    updateHpMax(num){
        this.hpMax = num;
    }

    updateHp(num){
        this.curHp = this.curHp + num
        
    }

    setHp(num){
        this.curHp = num;
    }


    // update (dt) {}
}