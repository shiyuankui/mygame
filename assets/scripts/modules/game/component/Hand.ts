import { G } from "../../../common/Global";

const {ccclass, property} = cc._decorator;

@ccclass
export default class Hand extends cc.Component {

    private _Change: string;

    public get Change() {
        return this._Change;
    }

    public set Change(name: string) {
        this._Change = name;
    }

    onLoad () {
        this._Change = "Graveyard"
    }

    start () {

    }

    onDestroy(){
        let msgDis = "HandTo"+this._Change
        let param = {
            node : this.node,
            msg : msgDis
        }
       G.app.eventMgr.dispatchEvent(msgDis,param) 
    }


    // update (dt) {}
}