import { GameMgr } from '../../manager/GameManager';
import HHelpTool from "../../tools/HHelpTool";
import {G} from "../../common/Global";
import CardManager from '../../manager/CardManager';


// let layer = map.getLayer('baseLayer');
// //获取左上角瓦片坐标为（0,0）的图块的像素坐标
// var pos = layer.getPositionAt(0,0);
// //获得当前该图块的id，也就是gid（注意，这里的id是从1开始的，与TiledMap Editor中显示的不同，如果返回值为0，则为空）
// var gid = layer.getTileGIDAt(0,0);
// for (let i = 1;i < 5;i++){
//     layer.setTileGIDAt(1, i, 1, 1)
// }

const {ccclass, property} = cc._decorator;

@ccclass
export class GameScene extends cc.Component {
	// @property(cc.Vec2)
	// physicWorldSize: cc.Vec2 = cc.v2(0, 0);

    // @property(cc.Node)
    // layerPlayerManager: cc.Node = null;

    tmxMap = null;
    selectCard = null;
    cardMgr = null;
    objectMgr = null;
    onLoad(){
        let tiledmap =  this.node.getChildByName("tiledmap")
        let map = tiledmap.getComponent("cc.TiledMap")
        this.tmxMap = tiledmap;

        let EntityLayer =  this.node.getChildByName("EntityLayer")
        let EntityMgr = EntityLayer.getComponent("GameObjectManager")
        this.objectMgr = EntityMgr
        this.registerTouchListener(EntityLayer)
        // GameMgr.initGame();
        // this.updateCard()

        // this.node.addComponent("Hand")
        // this.node.getComponent("Hand").Handtype = 1
    }    

    registerTouchListener(node) {

		node.on(cc.Node.EventType.TOUCH_START, function(event) {
            let touches = event.getTouches();
            let touchLoc = touches[0].getLocation();
            this.onTouchBegan(touchLoc);
		}.bind(this), this);

		node.on(cc.Node.EventType.TOUCH_MOVE, function(event) {
			let touches = event.getTouches();
			let touchLoc = touches[0].getLocation();
            this.onTouchMove(touchLoc);
		}.bind(this), this);

        node.on(cc.Node.EventType.TOUCH_END, function(event) {
            let touches = event.getTouches();
			let touchLoc = touches[0].getLocation();
			this.onTouchEnd(touchLoc);
		}.bind(this), this);
	}

	onTouchBegan(touchLoc) {
        this.selectCard = null;
        let cardNum =  this.cardMgr.getCardCount();
        for( let i = 0;i < cardNum;i++)
        {
            let card =  this.cardMgr.getCard(i)
            if(card){
                let cardBox =  new cc.Rect(card.x-card.width/2, card.y-card.height/2, card.width, card.height);
                if (cardBox.contains(touchLoc)) {
                    this.selectCard = i
                }
            }
        }
    }
    
    getCardByIndex(index){
        let card =  HHelpTool.getChildByDirectory("CardLayer.NodeSprite"+index, this.node)
        return card
    }

	onTouchMove(touchLoc) {
        if ( this.selectCard != null ){
            let card = this.cardMgr.getCard(this.selectCard)
            let cardScript = card.getComponent("Card")
            cardScript.setMoveType();
            card.x = touchLoc.x
            card.y = touchLoc.y
        }
	}

	onTouchEnd(touchLoc) {
        this.convertPosToGrid(this.tmxMap,touchLoc.x,touchLoc.y)
        this.selectCard = null;
    }
    
    convertPosToGrid(tiledmap,x,y){
        let dx = tiledmap.x
        let dy = tiledmap.y + this.node.height/2 - tiledmap.height/2
        let tmxMap = tiledmap.getComponent("cc.TiledMap")
        let cellXCount = tmxMap.getMapSize().width;
        let cellYCount = tmxMap.getMapSize().height;
        let cellWidth = tmxMap.getTileSize().width;
        let cellHeight = tmxMap.getTileSize().height;
        let mapPixWidth = cellWidth * cellXCount;
        let mapPixHeight = cellHeight * cellYCount;
        let rx = x - dx
        let ry = y - dy
        let px = Math.floor((x-dx)/cellWidth)//Math.round(0-((rx - mapPixWidth/2) / (  cellWidth/2) + (ry - mapPixHeight) /(   cellHeight/2) )/2)
        let py = Math.floor((y-dy)/cellHeight)//Math.round(0-((ry - mapPixHeight) / (  cellHeight/2) - (rx - mapPixWidth/2) /(  cellWidth/2)  )/2)
        // let pos = tmxMap.getLayer('baseLayer').getPositionAt(px,py);
        cc.log("x===" +(px ) + ";" + (py) +";"+x+";"+y)
        if(px <= cellXCount &&px >=0&&py>=0&&py<=cellYCount){
           
            let layer = tmxMap.getLayer('layer')
            if (this.selectCard != null){
                let card = this.cardMgr.getCard(this.selectCard)
                let cardId =  card.getComponent("Card").id;
                let summonId =  G.app.configMgr.getConfigField("ConfigCard",cardId,"summon");
                // let pos = layer.getPositionAt(px,py);
                let posx = Math.floor(px /2);
                let posy = Math.floor(py /2);
                let data = {px : posx , py : posy,cellWidth: cellWidth, cellHeight : cellHeight,id:summonId}
                this.playCard(this.selectCard,data);
            }
            // layer.setTileGIDAt(1, px, cellYCount- py, 1)
        }
    }

    touchPosChangeToGridPos(){

    }


    playCard(index,data){
        this.cardMgr.playCard(index);
        this.objectMgr.addObject(data)
    }

    update(dt){

    }

    
}