const {ccclass, property} = cc._decorator;

@ccclass
export default class Card extends cc.Component {

    @property(cc.Label)
    // label: cc.Label = null;

    id = null;
    index = null;
    _recordX = null;
    _recordy = null;

    onLoad () {
        // let sprite = this.node.getChildByName("SpriteCardBg")

        // this.registerTouchListener(this.node)

        this._recordX = this.node.x
        this._recordy = this.node.y
    
    }

    initData(data){
        let index = data.index
        this.id = data.id
        this.index = index;
        this.node.name = "NodeSprite" + index;
        this.updateCardPos();
    }
    
    updateCardPos(){
        this.node.x = (this.index+0.5)*150;
        this.node.y = 60;
    }
    


    setMoveType(){
        this.node.getChildByName("SpriteCardBg").active = false
    }

    setMoveEnd(){
        this.node.x = this._recordX
        this.node.y = this._recordX
        this.node.getChildByName("SpriteCardBg").active = true
    }


    start () {

    }

    // update (dt) {}
}