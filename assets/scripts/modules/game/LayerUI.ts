import HHelpTool from "../../tools/HHelpTool";
import { G } from "../../common/Global";
import { NOTIFY_EVENTS } from "../../common/EventDefine";
import { gen_handler } from "../../tools/Utils";

const {ccclass, property} = cc._decorator;

@ccclass
export default class LayerUI extends cc.Component {

    onLoad () {
        this.registerButtonListener();
    }

    start () {

    }

	registerButtonListener() {
		let btn = this.node.getChildByName("BtnTurnEnd");
        HHelpTool.addClickEvent(this.node, btn, "LayerUI", "onPressBtn", "123456");
        
        // let labelCount = this.node.getChildByName("LabelTurnCount")
        // labelCount.on(NOTIFY_EVENTS.ON_TURN_END,function(){
          
        // }.bind(this),this)
        G.app.eventMgr.addEventListener(NOTIFY_EVENTS.ON_TURN_END,gen_handler(this.onTurnEnd,this))
    }
    
    onTurnEnd(){
        let labelCount = this.node.getChildByName("LabelTurnCount")
        let turn = G.app.gameMgr.getTurnNum();
        labelCount.getComponent(cc.Label).string = turn
    }

    onDestroy(){
        G.app.eventMgr.removeEventListener(NOTIFY_EVENTS.ON_TURN_END,gen_handler(this.onTurnEnd,this))
    }

	onPressBtn(event, custonData) {
        G.app.gameMgr.onTurnEnd();
	}
}
