import { G } from "../../common/Global";

const {ccclass, property} = cc._decorator;

@ccclass
export default class GameEntity extends cc.Component {
    cellWidth = 40;
    cellHeight = 40;
    canMove = true;

    @property(Number)
    px = 0;

    @property(Number)
    py = 0;

    @property(Number)
    id = 1;


    onLoad(){

    }


 
    setPos(pos){
        this.node.x = pos.x;
        this.node.y = pos.y;
    }

    start () {

    }

    initData(data){
        for(let i in data){
            this[i] = data[i]
        }

        if(data.id){
            let property = this.node.getComponent("Property")
            cc.log(this.node)
            cc.log(property)
            property.initData(data.id)
        }
     
        // this.px = data.px
        // this.py = data.py
        // this.cellWidth = data.cellWidth 
        // this.cellHeight = data.cellHeight
        
                
        

        this.updateStatus();
    }

    

    update (dt) {
        // this.updatePos();


    }

   
    onTurnEnd(){
        cc.log("onTurnEnd")
    }

    onTurnBegin(){
        cc.log("onTurnBegin")
    }

    updateStatus(){
        this.updatePos();

    }

    updatePos(){
        this.node.x = (this.px*2+1) * this.cellWidth
        this.node.y = (this.py*2+1) * this.cellHeight
    }

  

    moveUp(){
        if(this.canMove){
            this.py++;
            this.updatePos();
        }
        
    }

    moveDown(){
        if(this.canMove){
            this.py--;
            this.updatePos();
        }
    }

    moveLeft(){
        if(this.canMove){
            this.px--;
            this.updatePos();
        }
    }

    moveRight(){
        if(this.canMove){
            this.px++;
            this.updatePos();
        }
    }
}