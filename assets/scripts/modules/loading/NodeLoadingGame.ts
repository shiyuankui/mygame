const {ccclass, property} = cc._decorator;

@ccclass
export class NodeLoadingGame extends cc.Component {
	@property(cc.Node)
	sprLoading: cc.Node = null;

	onLoad() {
		this.rotateSprLoading();
	}

	/**
	 * [rotateSprLoading 加载图片进行旋转操作]
	 */
	rotateSprLoading() {

		let actRotate = cc.rotateBy(1, 180)
    	let actRepeatFor = cc.repeatForever(actRotate)
    	this.sprLoading.runAction(actRepeatFor)
	}

}
