import {G} from "../../common/Global"
import {AppManager} from "../../manager/AppManager"

const {ccclass, property} = cc._decorator;

@ccclass
export class LoadingScene extends cc.Component {
    @property(cc.Node)
    private app: cc.Node = null;

    onLoad(){
        cc.game.addPersistRootNode(this.app);
        this.initGlobal();

    }

    initGlobal(){
        if(!G.app)
        {
            G.app = this.app.getComponent(AppManager);
        }    
    }

    start() {
        console.log("loading start");
        cc.loader.loadResDir("prefab");
        G.app.startLogin();
        
    }
}