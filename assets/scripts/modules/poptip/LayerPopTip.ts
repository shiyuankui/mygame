import HHelpTools from "../../tools/HHelpTool";

const {ccclass, property} = cc._decorator;

@ccclass
export class LayerPopTip extends cc.Component {

	confirmFunc: any = null;
	cancelFunc: any = null;

    onLoad(){
        let sprBgNode = this.node.getChildByName("sprBg");

		let btnClose = sprBgNode.getChildByName("btnClose");
        HHelpTools.addClickEvent(this.node, btnClose, "LayerPopTip", "onPressClose");
        // HHelpTool.playPopinEffect(this.node);
    }

    registerCallbackFunc(rConfirmFunc: any, rCancelFunc: any){
    	console.log("register callback func");
    	this.confirmFunc = rConfirmFunc;
    	this.cancelFunc = rCancelFunc;
    }

    showTip(tipID: number){
    	console.log("show tips");
    }

    onPressConfirm(){
    	if (this.confirmFunc) {
    		this.confirmFunc.exec();
    	}
    }

	onPressClose(){
		if (this.cancelFunc) {
			this.cancelFunc.exec();
		}
    	// this.node.destroy();
        HHelpTools.playPopoutEffect(this.node);
    }

}
