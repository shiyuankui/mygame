import HHelpTools from "../../tools/HHelpTool";
import {G} from "../../common/Global";
import {gen_handler} from "../../tools/Utils";
import {NOTIFY_EVENTS} from "../../common/EventDefine";
import * as ProtoMsgDefine from "../../common/ProtoMsgDefine";

const {ccclass, property} = cc._decorator;

@ccclass
export class LayerMainPageUI extends cc.Component {
 	roomType: number            = 101;            // 记录房间类型
	onLoad() {
		this.registerButtonListener();
	}

	/**
	 * [registerButtonListener 注册按钮点击事件]
	 */
	registerButtonListener() {
		let btnBattery = this.node.getChildByName("BtnBattery");
		HHelpTools.addClickEvent(this.node, btnBattery, "LayerMainPageUI", "onPressBtnBattery");

		let btnQuickStart = this.node.getChildByName("BtnQuickStart");
        HHelpTools.addClickEvent(this.node, btnQuickStart, "LayerMainPageUI", "onPressQuickStart");
	}

	/**
	 * [onPressBtnBattery 点击了炮台按钮]
	 */
	onPressBtnBattery() {
		G.app.pushLayer("cannon/LayerCannon", "PanelBg");
	}
	
	onPressQuickStart(){
        G.app.logMgr.info("on press room");
        // 1. 判定玩家是否可以进入该房间
        // 2. 开始进入房间，向服务器发送注册消息
        let param:any = ProtoMsgDefine.getMsgStructData(ProtoMsgDefine.MSG_DATA_STRUCT_LIST.FISH_MATCH_ENTER_REQ);
        param.roomCfgId = this.roomType;
        G.app.netMgr.sendProtoMsg("fish_match.fish_match_enter_req", param);
    }
}
