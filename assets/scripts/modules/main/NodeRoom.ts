import HHelpTools from "../../tools/HHelpTool";
import {G} from "../../common/Global";
import * as ProtoMsgDefine from "../../common/ProtoMsgDefine";
import {gen_handler} from "../../tools/Utils";
import {NOTIFY_EVENTS} from "../../common/EventDefine";
const {ccclass, property} = cc._decorator;

@ccclass
export class NodeRoom extends cc.Component {
	@property(cc.Integer)
	roomType: number			= -1;			// 记录房间类型


	onLoad() {
		this.registerBtnListener();
	}

	registerBtnListener() {
		let btnRoom = this.node.getChildByName("RoomBg");
        HHelpTools.addClickEvent(this.node, btnRoom, "NodeRoom", "onPressRoom");
	}


	init(roomId){
		this.roomType = roomId;
	}
	/**
	 * [onPressRoom 点击了房间按钮]
	 */
	onPressRoom() {
		G.app.logMgr.info("on press room");
		// G.app.showGameScene();
		// 1. 判定玩家是否可以进入该房间
		// 2. 开始进入房间，向服务器发送注册消息
		let param:any = ProtoMsgDefine.getMsgStructData(ProtoMsgDefine.MSG_DATA_STRUCT_LIST.FISH_MATCH_ENTER_REQ);
		param.roomCfgId = this.roomType;
		G.app.netMgr.sendProtoMsg("fish_match.fish_match_enter_req", param);
	}

}
