import HHelpTool from "../../tools/HHelpTool";

const {ccclass, property} = cc._decorator;

@ccclass
export default class LayerMainMenu extends cc.Component {

    onLoad () {
        this.registerBtnListener();
    }
    /**
	 * [registerBtnListener 注册按钮点击事件]
	 */
    registerBtnListener(){
        let btnClassic = this.node.getChildByName("BtnClassic");
        HHelpTool.addClickEvent(this.node, btnClassic, "LayerMainMenu", "onPressBtnClassic");
    }

    /**
     * [onPressBtnClassic 按下经典模式按钮]
     */
    onPressBtnClassic(){
        //添加预制场景
        // let LayerClassicMenu = HHelpTool.requirePrefabFile("main/LayerClassicMenu");
        // this.node.parent.addChild(LayerClassicMenu);
        let layerClassicMenu = this.node.parent.getChildByName("LayerClassicMenu");
        this.node.active = false;
        // this.node.destroy();
        layerClassicMenu.active = true;
    }
    // start () {

    // }

    // update (dt) {}
}
