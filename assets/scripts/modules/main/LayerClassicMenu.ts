
import HHelpTools from "../../tools/HHelpTool";
import {G} from "../../common/Global";
import {gen_handler} from "../../tools/Utils";
import {NOTIFY_EVENTS} from "../../common/EventDefine";

const {ccclass, property} = cc._decorator;

@ccclass
export default class LayerClassicMenu extends cc.Component {

    _pageViewClassic :cc.Node = null;
    _curPage : number = 1;
    _curTotal : number = 3;
    _page : cc.Node[] = [];
    _btnExit : cc.Node = null;
    onLoad () {
        this._btnExit = this.node.getChildByName("BtnExit");
        this._pageViewClassic = this.node.getChildByName("PageViewClassic");
        this.initPage();
        this.registerBtnListener();
        this.registerEventListener();
    }
    
    start () {
        this._pageViewClassic.getComponent(cc.PageView).scrollToPage(this._curPage,0);
        this.updatePageStatus();
        console.log(this._pageViewClassic.getComponent(cc.PageView).getCurrentPageIndex());
    }
    registerBtnListener(){
        HHelpTools.addClickEvent(this.node, this._btnExit, "LayerClassicMenu", "onPressExit");
    }

    registerEventListener() {
		G.app.eventMgr.addEventListener(NOTIFY_EVENTS.GAME_NOTIFY_MATCH_ENTER_RSP, gen_handler(this.onEnterRoomRsp, this));
    }
    
    initPage(){
        let roomData = G.app.configMgr.getConfigsByField("RoomType","room_type",1);
        let k : number = 0;
        for (let i in roomData){
            //取预制
            let prePage = HHelpTools.requirePrefabFile(`main/NodeRoomItem${k+1}`);
            this._pageViewClassic.getComponent(cc.PageView).addPage(prePage);
            this._page[i] = prePage;
            k++;
        }
        this._curTotal = k;
        let pageViewEventHandler = new cc.Component.EventHandler();
        pageViewEventHandler.target = this.node; // 这个是你的事件处理代码组件所属的节点
        pageViewEventHandler.component = "LayerClassicMenu";
        pageViewEventHandler.handler = "onSlide";
        
        this._pageViewClassic.getComponent(cc.PageView).pageEvents.push(pageViewEventHandler);1

    }

    updatePageStatus(){
        for(let i = 0;i<this._curTotal;i++){
            if(i != this._curPage){
                this._page[i].getChildByName("RoomBg").setScale(0.7);
            }
            else{
                this._page[i].getChildByName("RoomBg").setScale(1.2);
            }
        }
    }

    onSlide(sender, eventType){
        console.log(this._pageViewClassic.getComponent(cc.PageView).getCurrentPageIndex());
        if (eventType !== cc.PageView.EventType.PAGE_TURNING) {
            return;
        }
        this._curPage = this._pageViewClassic.getComponent(cc.PageView).getCurrentPageIndex();

        this.updatePageStatus();
        
    }

    onPressExit(){
        this.node.parent.getChildByName("LayerMainMenu").active = true;
        this.node.active = false;
    }
    /**
	 * [onEnterRoomRsp ?????????]
	 * @param {[type]} params [?????]
	 */
	onEnterRoomRsp(params) {
		G.app.logMgr.info("on enter room rsp");

		G.app.showGameScene();
	}
    // update (dt) {}
}
