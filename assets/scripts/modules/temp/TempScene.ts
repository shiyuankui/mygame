import HHelpTools from "../../tools/HHelpTool";
import {G} from "../../common/Global";

const {ccclass, property} = cc._decorator;

@ccclass
export class TempScene extends cc.Component {
	onLoad() {
		this.registerButtonListener();
	}

	registerButtonListener() {
		let btn1 = this.node.getChildByName("Button1");
		let btn2 = this.node.getChildByName("Button2");
		let btn3 = this.node.getChildByName("Button3");
		let btn4 = this.node.getChildByName("Button4");

		HHelpTools.addClickEvent(this.node, btn1, "TempScene", "onPressBtn", "123456");
		HHelpTools.addClickEvent(this.node, btn2, "TempScene", "onPressBtn", "234567");
		HHelpTools.addClickEvent(this.node, btn3, "TempScene", "onPressBtn", "345678");
		HHelpTools.addClickEvent(this.node, btn4, "TempScene", "onPressBtn", "456789");
	}

	onPressBtn(event, custonData) {
		G.tempUserId = custonData;
		cc.director.loadScene("loading");
	}

}
