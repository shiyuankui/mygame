import {G} from "../common/Global";

// 通用工具管理类
class HHelpTools {
	/**
	 * [requirePrefabFile 请求prefab文件]
	 * @param {string} rFileName [prefab文件名]
	 */
	requirePrefabFile(rFileName: string){
		rFileName = "prefab/" + rFileName;

		if(!cc.loader.getRes(rFileName)){
			cc.loader.loadRes(rFileName, function(errorMessage,loadedResource){
				//检查资源加载
				if( errorMessage ) { cc.log( '载入预制资源失败, 原因:' + errorMessage ); return; }
				if( !( loadedResource instanceof cc.Prefab ) ) { cc.log( '你载入的不是预制资源!' ); return; } 
				//开始实例化预制资源
				let prefabNode = cc.instantiate(loadedResource);  
				return prefabNode; 
			});
		}else{
			let prefabNode = cc.instantiate(cc.loader.getRes(rFileName));  
			return prefabNode; 
		}
	}

	/**
	 * [showWithPopEffect 对当前节点执行显示动作]
	 * @param {cc.Node} sNode [当前节点]
	 */
	showWithPopEffect(sNode: cc.Node){
		if (!sNode) {
			return;
		}
		sNode.opacity = 0;
    	sNode.active = true;

    	let seq1 = cc.sequence(cc.scaleTo(.2, 1.05), cc.scaleTo(.2, 1.0));
    	let seq2 = cc.sequence(cc.fadeTo(.1, 128), cc.fadeTo(.1, 255));

    	sNode.runAction(cc.sequence(seq1, seq2));
	}

	/**
	 * [hideWithPopEffect 对当前节点执行隐藏动作]
	 * @param {cc.Node} sNode [当前节点]
	 */
	hideWithPopEffect(hNode: cc.Node){
		if (!hNode) {
        	return;
    	}

    	let seq1 = cc.sequence(cc.scaleTo(.2, 1.05), cc.scaleTo(.2, 1.0));
    	let seq2 = cc.sequence(cc.fadeTo(.1, 128), cc.fadeTo(.1, 0));
    	hNode.runAction(cc.sequence(seq1, seq2));

    	setTimeout(function() {
        	hNode.active = false;
    	}, 200);
	}

	/**
	 * [playPopinEffect 窗体执行弹出动作]
	 * @param {cc.Node} pNode       [当前节点]
	 * @param {number}  pActionTime [执行事件，默认是0.5s]
	 */
	playPopinEffect(pNode: cc.Node, pActionTime: number = 0.5){
		if (!pNode) {
			return;
		}
		pNode.scale = 0.5;
		let popinAction = cc.scaleTo(pActionTime, 1.0).easing(cc.easeBackOut());
		pNode.runAction(popinAction);
	}

	playPopoutEffect(pNode: cc.Node, pActionTime: number = 0.2) {
		if (!pNode) {
			return;
		}
		let popoutAction = cc.scaleTo(pActionTime, 0.0);
		let callbackAction = cc.callFunc(function(){
			pNode.destroy();
		})
		pNode.runAction(cc.sequence(popoutAction, callbackAction));
	}

	/**
	 * [showPopTips description]
	 * @param {number}  sTipID       [提示的编号]
	 * @param {any} 	sConfirmFunc [确定的回调函数]
	 * @param {any} 	sCancelFunc  [取消的回调函数]
	 */
	showPopTips(sTipID: number, sConfirmFunc?: any, sCancelFunc?: any){
		let popTip = this.requirePrefabFile("poptip/LayerPopTip");
		let curScene = cc.director.getScene();
        curScene.addChild(popTip);
        this.playPopinEffect(popTip.getChildByName("sprBg"));
        let popTipScript = popTip.getComponent("LayerPopTip");
        popTipScript.registerCallbackFunc(sConfirmFunc, sCancelFunc);
        popTipScript.showTip(sTipID);
	}

	pushLayer(prefabFileName: string, actionNodeFile: string, showAction?: any) {
		let prefabNode = this.requirePrefabFile(prefabFileName);
		let curScene = cc.director.getScene();
		curScene.addChild(prefabNode);

		let actionNode = prefabNode.getChildByName(actionNodeFile);
		if (showAction) {
			actionNode.runAction(showAction);
		}else {
			this.playPopinEffect(actionNode);
		}
	}

	/**
	 * [loadUrlTexture 加载远程URL资源]
	 * @param {cc.Node} lNode   [当前节点]
	 * @param {string}  lImgUrl [远程资源的URL]
	 * @param {number}  lWidth  [资源的宽度]
	 * @param {number}  lHeight [资源的高度]
	 */
	loadUrlTexture(lNode: cc.Node, lImgUrl: string, lWidth: number, lHeight: number){
		if (!lImgUrl || lImgUrl.length === 0) {
        	return
    	}

    	let sprImage = lNode.getComponent(cc.Sprite)
    	if (!sprImage) {
        	console.log("Error in loadURLTexture, the node is empty ")
        	return
    	}

    	// 默认80X80的大小
    	let imageWidth = (lWidth === null ? 80 : lWidth)
	    let imageHeight = (lHeight === null ? 80 : lHeight)

    	// load head texture
    	cc.loader.load({
        	url: lImgUrl,
        	type: "png"
    	}, function(err, tex) {
        	let spriteFrameTex = new cc.SpriteFrame(tex, new cc.Rect(0, 0, lWidth, lHeight));
        	sprImage.spriteFrame = spriteFrameTex
    	});
	}

	/**
	 * [loadDragonBones 加载骨骼动画]
	 * @param {[type]} resHeadName 			[资源头]
	 * @param {[type]} resID       			[资源ID]
	 * @param {[type]} parentNode       	[父节点]
	 */
	loadDragonBones(resHeadName, resID, parentNode) {
		let armatureDisplay = parentNode.addComponent(dragonBones.ArmatureDisplay);

		let assetName = "armature/" + resHeadName + resID + "_ske";
		let atlasAsset = "armature/" + resHeadName + resID + "_tex";

        cc.loader.loadRes(assetName, dragonBones.DragonBonesAsset, (err, res) => {
            if (err) cc.error(err);
            armatureDisplay.dragonAsset = res;
            cc.loader.loadRes(atlasAsset, dragonBones.DragonBonesAtlasAsset, (err, res) => {
	            if (err) cc.error(err);
    	        armatureDisplay.dragonAtlasAsset = res;
    	    });
        });

		return armatureDisplay;
	}

	testFunc() {

	}

	/**
	 * [propertyFunc 属性设置函数]
	 * @param {[type]} pClass    [类名]
	 * @param {[type]} pVarName  [类变量]
	 * @param {[type]} pOptional [可选参数]
	 */
	propertyFunc(pClass, pVarName: string, pOptional?) {
		var initToUpperCase = function(iString) {
			return iString.substring(0, 1).toUpperCase().concat(iString.substring(1));
		}

		var removeUnderLine = function(rString) {
			return (rString.substring(0, 1) === "_") ? rString.substring(1) : rString;
		}

		let funcName = null
		// 如果没有参数的话
		if (!pOptional) {
			funcName = removeUnderLine(pVarName);
			funcName = "set" + initToUpperCase(funcName);
			eval("pClass.prototype." + funcName + " = function(pVar) { this." + pVarName + " = pVar;};");
			// pClass.prototype.funcName = function(pVar) {pClass[pVarName] = pVar;};


			funcName = removeUnderLine(pVarName);
			funcName = "get" + initToUpperCase(funcName);
			eval("pClass.prototype." + funcName + " = function() { return this." + pVarName + "; };");
			// pClass.prototype.funcName = function() {return pClass[pVarName];};
			return;
		}

		if (pOptional.readonly !== true) {
			if (typeof(pOptional.setter) === "string" ) {
				eval("pClass.prototype." + pOptional.setter + " = function(pVar) { this." + pVarName + " = pVar;};");
			}else {
				funcName = removeUnderLine(pVarName);
				funcName = "set" + initToUpperCase(funcName);
				eval("pClass.prototype." + funcName + " = function(pVar) { this." + pVarName + " = pVar;};");
			}
		}

		if (typeof(pOptional.getter) === "string" ) {
			eval("pClass.prototype." + pOptional.getter + " = function() { return this." + pVarName + "; };");
		}else {
			funcName = removeUnderLine(pVarName);
			funcName = "get" + initToUpperCase(funcName);
			eval("pClass.prototype." + funcName + " = function() { return this." + pVarName + "; };");
		}
	}




	/**
	 * [getVectorAngle 计算任意两点与水平线的夹角]
	 * @param {[type]} gBeginPoint [起点]
	 * @param {[type]} gEndPoint   [终点]
	 */
	getVectorAngle(gBeginPoint, gEndPoint) {
		// let pZeroPoint = cc.v2(0, 0);
	    let pSubPoint = gEndPoint.sub(gBeginPoint);
	 //    let angleDegree = cc.misc.radiansToDegrees(Math.atan2(pSubPoint.y, pSubPoint.x));
        // console.log(angleToDegree);

        // let angle = pSubPoint.angle(pSubPoint);
        // let angleDegree = cc.misc.radiansToDegrees(angle);

        let angle = pSubPoint.signAngle(cc.v2(0, 1));
        let angleDegree = cc.misc.radiansToDegrees(angle);

	    return angleDegree;
	}
    
    /**
     * [changePosFromDesignToViewSize 将坐标点从设计分辨率改为屏幕分辨率]
     * @param {[type]} viewPos [description]
     */
	changePosFromDesignToViewSize(viewPos) {
		let newPos = cc.v2(0, 0);

		newPos.x = G.visibleSize.width / G.visibleSize.height * viewPos.x;
		newPos.y = G.visibleSize.height / G.visibleSize.height * viewPos.y;
		return newPos;
	}


	/**
	 * [addClickEvent description]
	 * @param {cc.Node} aTarget         [处理这个事件所属的节点]
	 * @param {cc.Node} aNode           [监听当前事件的节点]
	 * @param {string}  aComponent      [组件所属的文件名]
	 * @param {string}  aHandler        [处理函数]
	 * @param {string}  aCustomEvent 	[自定义参数]
	 */
	addClickEvent(aTarget: cc.Node, aNode: cc.Node, aComponent: string, aHandler: string, aCustomEvent?: any){
		var clickEventHandler = new cc.Component.EventHandler();
    	clickEventHandler.target = aTarget; 
    	clickEventHandler.component = aComponent; // 这个是代码文件名
    	clickEventHandler.handler = aHandler; // 回调函数
    	clickEventHandler.customEventData = aCustomEvent; // 回掉中的自定义参数

    	var clickEvents = aNode.getComponent(cc.Button).clickEvents;
    	clickEvents.push(clickEventHandler);
	}


	/**
	 * [addToggleEvent 注册单选按钮事件]
	 * @param {cc.Node} aTarget      [处理这个事件所属的节点]
	 * @param {cc.Node} aNode        [监听当前事件的节点]
	 * @param {string}  aComponent   [组件所属的文件名]
	 * @param {string}  aHandler     [处理函数]
	 * @param {string}  aCustomEvent [自定义参数]
	 */
	addToggleEvent(aTarget: cc.Node, aNode: cc.Node, aComponent: string, aHandler: string, aCustomEvent?: any){
		var toggleEventHandler = new cc.Component.EventHandler();
    	toggleEventHandler.target = aTarget; 
    	toggleEventHandler.component = aComponent; // 这个是代码文件名
    	toggleEventHandler.handler = aHandler; // 回调函数
    	toggleEventHandler.customEventData = aCustomEvent; // 回掉中的自定义参数

    	var clickEvents = aNode.getComponent(cc.Toggle).clickEvents;
    	clickEvents.push(toggleEventHandler);
	}
	
	getChildByDirectory(str,parent){
		let actionArray = str.split(".")
		let retNode = parent;
		for(let i = 0 ; i<actionArray.length;i++ ){
			retNode = retNode.getChildByName(actionArray[i])
			if (!retNode){
				cc.log("error",actionArray)
			}
		}
		return retNode
	}
}

export default new HHelpTools();
