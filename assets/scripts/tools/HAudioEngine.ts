// 音乐音效管理
class HAudioEngine {
	// 音乐相关
	private _isMusicOpen: boolean;
	private _musicID: number;
	private _musicVolume: number;

	// 音效相关
	private _isEffectOpen: boolean;
	private _effectIDs: number[];
	private _effectVolume: number;


	constructor(){
		this._isMusicOpen 	= true;
    	this._musicID 		= -1;
    	this._musicVolume 	= 0.5;

    	this._isEffectOpen 	= true;
    	this._effectIDs 	= [];
    	this._effectVolume 	= 0.5;
    
    	// 根据本地存储来进行修改
    	let mo = cc.sys.localStorage.getItem("musicOpen");
    	if (mo === "false") {
        	this._isMusicOpen = false;
        	this._musicVolume = 0.5;
    	} else {
        	this._isMusicOpen = true;
        	this._musicVolume = cc.sys.localStorage.getItem("musicVolume");
    	}

    	var ao = cc.sys.localStorage.getItem("audioOpen");
    	if (ao === "false") {
        	this._isEffectOpen = false;
        	this._effectVolume = 0.5;
    	} else {
        	this._isEffectOpen = true;
        	this._effectVolume = cc.sys.localStorage.getItem("audioVolume");
    	}

    	cc.audioEngine.setMaxAudioInstance(50);
	}

	////////////////////////////////////////////////音乐相关

	/**
	 * [playMusic 音乐播放]
	 * @param {string}  pMusicFileName [文件名]
	 * @param {boolean} pIsLoop        [是否循环]
	 */
	playMusic(pMusicFileName: string, pIsLoop: boolean){

	}

	/**
	 * [pauseMusic 暂停音乐，因为音乐只有一个，所以不需要传递musicid]
	 */
	pauseMusic(){

	}

	/**
	 * [stopMusic 停止音乐]
	 */
	stopMusic(){

	}

	/**
	 * [resumeMusic 恢复音乐]
	 */
	resumeMusic(){

	}


	////////////////////////////////////////////////////音效相关
	/**
	 * [playEffect 播放音效]
	 * @param {string}     pEffectFileName [音效文件]
	 * @param {boolean =               false}       pIsLoop [是否循环，默认是不循环的]
	 */
	playEffect(pEffectFileName: string, pIsLoop: boolean = false){

	}

	/**
	 * [pauseEffect 暂停音效]
	 * @param {string} pEffectID [音效ID]
	 */
	pauseEffect(pEffectID: number){

	}

	/**
	 * [stopEffect 停止音效]
	 * @param {number} sEffectID [音效ID]
	 */
	stopEffect(sEffectID: number){

	}

	/**
	 * [pauseAllEffect 暂停所有的音效]
	 */
	pauseAllEffect(){

	}

	/**
	 * [stopAllEffect 停止所有的音效]
	 */
	stopAllEffect(){

	}

	////////////////////////////////////////////////////////////一些私有接口
}

export default new HAudioEngine();